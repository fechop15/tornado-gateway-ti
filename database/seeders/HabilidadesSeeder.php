<?php

namespace Database\Seeders;

use App\Models\Habilidades;
use App\Models\NivelHabilidad;
use Illuminate\Database\Seeder;

class HabilidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=HabilidadesSeeder
        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en  Java',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en Python',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en Php',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en Go',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en Angular',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en JavaScript',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en React',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación en Node.js',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos Oracle',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos MySql',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos SqlServer',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos Postgres',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos Oracle',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Programación bases de datos MongoDB',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Desarrollo Front-end',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Desarrollo Back-end',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Ciencia de datos',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Big Data',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Machine Learning',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Inteligencia Artificial',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Robótica',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

        $habilidad=Habilidades::create([
            'descripcion'=>'Minería de datos (Data Mining)',
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Alto',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Medio',
            'habilidade_id'=>$habilidad->id,
        ]);
        NivelHabilidad::create([
            'descripcion'=>'Bajo',
            'habilidade_id'=>$habilidad->id,
        ]);

    }
}
