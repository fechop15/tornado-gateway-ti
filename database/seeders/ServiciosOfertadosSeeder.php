<?php

namespace Database\Seeders;

use App\Models\Servicio;
use Illuminate\Database\Seeder;

class ServiciosOfertadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Servicio
        //php artisan db:seed --class=ServiciosOfertadosSeeder
        //Desarrollo a la Medida;
        // Comercialización de producto TI estandar;
        // Infraestructura tecnologica;
        // Seguridad Informática;
        // Investigación;
        // Redes y comunicaciones;
        // Diseño y desarrollo Web;
        // Diseño y desarrollo de Apps.
        Servicio::create([
            'descripcion'=>'Desarrollo a la Medida',
        ]);
        Servicio::create([
            'descripcion'=>'Comercialización de producto TI estandar',
        ]);
        Servicio::create([
            'descripcion'=>'Infraestructura tecnologica',
        ]);
        Servicio::create([
            'descripcion'=>'Seguridad Informática',
        ]);
        Servicio::create([
            'descripcion'=>'Investigación',
        ]);
        Servicio::create([
            'descripcion'=>'Redes y comunicaciones',
        ]);
        Servicio::create([
            'descripcion'=>'Diseño y desarrollo Web',
        ]);
        Servicio::create([
            'descripcion'=>'Diseño y desarrollo de Apps',
        ]);
    }
}
