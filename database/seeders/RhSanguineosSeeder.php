<?php

namespace Database\Seeders;

use App\Models\RhSanguineo;
use Illuminate\Database\Seeder;

class RhSanguineosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=RhSanguineosSeeder
        RhSanguineo::create([
            'descripcion'=>'A+',
        ]);
        RhSanguineo::create([
            'descripcion'=>'A-',
        ]);
        RhSanguineo::create([
            'descripcion'=>'B+',
        ]);
        RhSanguineo::create([
            'descripcion'=>'B-',
        ]);
        RhSanguineo::create([
            'descripcion'=>'O+',
        ]);
        RhSanguineo::create([
            'descripcion'=>'O-',
        ]);
        RhSanguineo::create([
            'descripcion'=>'AB+',
        ]);
        RhSanguineo::create([
            'descripcion'=>'AB-',
        ]);
    }
}
