<?php

namespace Database\Seeders;

use App\Models\NivelIngles;
use Illuminate\Database\Seeder;

class NivelInglesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=NivelInglesSeeder
        NivelIngles::create([
            'descripcion' => 'A1',
        ]);
        NivelIngles::create([
            'descripcion' => 'A2',
        ]);

        NivelIngles::create([
            'descripcion' => 'B1',
        ]);
        NivelIngles::create([
            'descripcion' => 'B2',
        ]);

        NivelIngles::create([
            'descripcion' => 'C1',
        ]);
        NivelIngles::create([
            'descripcion' => 'C2',
        ]);
    }
}
