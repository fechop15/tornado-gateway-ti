<?php

namespace Database\Seeders;

use App\Models\Sector;
use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=SectorSeeder
        Sector::create([
            'id'=>'1',
            'descripcion'=>'BPO',
        ]);
        Sector::create([
            'id'=>'2',
            'descripcion'=>'KPO',
        ]);Sector::create([
            'id'=>'3',
            'descripcion'=>'ITO',
        ]);Sector::create([
            'id'=>'4',
            'descripcion'=>'Help Desk 2,3,4',
        ]);Sector::create([
            'id'=>'5',
            'descripcion'=>'Shared Service Centers',
        ]);Sector::create([
            'id'=>'6',
            'descripcion'=>'Audiovisual',
        ]);Sector::create([
            'id'=>'7',
            'descripcion'=>'Fintech',
        ]);Sector::create([
            'id'=>'8',
            'descripcion'=>'Data Center',
        ]);Sector::create([
            'id'=>'9',
            'descripcion'=>'IT service',
        ]);Sector::create([
            'id'=>'10',
            'descripcion'=>'Mobile Apps',
        ]);Sector::create([
            'id'=>'11',
            'descripcion'=>'Agritech',
        ]);Sector::create([
            'id'=>'12',
            'descripcion'=>'E-health',
        ]);Sector::create([
            'id'=>'13',
            'descripcion'=>'R&D Centers',
        ]);Sector::create([
            'id'=>'14',
            'descripcion'=>'Animation',
        ]);Sector::create([
            'id'=>'15',
            'descripcion'=>'Videogames',
        ]);Sector::create([
            'id'=>'16',
            'descripcion'=>'Software',
        ]);Sector::create([
            'id'=>'17',
            'descripcion'=>'E-commerce',
        ]);
    }
}
