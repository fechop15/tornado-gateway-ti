<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TipoOperacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //php artisan db:seed --class=TipoOperacionesSeeder
        \App\Models\Tipo_operacion::insert([
            [
                'descripcion' => 'Nacional',
            ]
        ]);
        \App\Models\Tipo_operacion::insert([
            [
                'descripcion' => 'Internacional',
            ]
        ]);
        \App\Models\Tipo_operacion::insert([
            [
                'descripcion' => 'Ambas',
            ]
        ]);
    }
}
