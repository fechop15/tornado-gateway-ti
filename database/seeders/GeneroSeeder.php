<?php

namespace Database\Seeders;

use App\Models\Genero;
use Illuminate\Database\Seeder;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=GeneroSeeder

        Genero::create([
            'descripcion'=>'Masculino',
        ]);
        Genero::create([
            'descripcion'=>'Femenino',
        ]);
        Genero::create([
            'descripcion'=>'Otro',
        ]);
    }
}
