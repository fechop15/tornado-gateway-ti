<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class Tipo_usuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=Tipo_usuarioSeeder
        \App\Models\Tipo_usuario::insert([
            [
                'id'=>0,
                'descripcion' => 'Administrador',
            ]
        ]);
        \App\Models\Tipo_usuario::insert([
            [
                'id'=>1,
                'descripcion' => 'Persona',
            ]
        ]);
        //
        \App\Models\Tipo_usuario::insert([
            [
                'id'=>2,
                'descripcion' => 'Empresa',
            ]
        ]);
    }
}
