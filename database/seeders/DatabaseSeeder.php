<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(Tipo_usuarioSeeder::class);
        //$this->call(UserSeeder::class);


        $this->truncateTables([

            'role_has_permissions', 'model_has_roles', 'model_has_permissions',
            'model_has_roles', 'roles', 'permissions', 'users', 'countries', "states",
            "cities"

        ]);

        $this->call(CountriesSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(RolsSeeder::class);
        $this->call(Tipo_usuarioSeeder::class);
        //$this->call(UserSeeder::class);

    }

    public function truncateTables(array $tables)
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {

            DB::table($table)->truncate();

        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');


    }
}
