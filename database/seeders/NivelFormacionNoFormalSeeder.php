<?php

namespace Database\Seeders;

use App\Models\NivelFormacionNoFormal;
use Illuminate\Database\Seeder;

class NivelFormacionNoFormalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=NivelFormacionNoFormalSeeder
        //Curso/diplomado/seminario/taller
        NivelFormacionNoFormal::create([
            'id'=>'1',
            'descripcion'=>'Curso',
        ]);
        NivelFormacionNoFormal::create([
            'id'=>'2',
            'descripcion'=>'Diplomado',
        ]);
        NivelFormacionNoFormal::create([
            'id'=>'3',
            'descripcion'=>'Seminario',
        ]);
        NivelFormacionNoFormal::create([
            'id'=>'4',
            'descripcion'=>'Taller',
        ]);
        NivelFormacionNoFormal::create([
            'id'=>'5',
            'descripcion'=>'Certificado',
        ]);
    }
}
