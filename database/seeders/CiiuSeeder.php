<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CiiuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=CiiuSeeder
        \App\Models\Ciiu::insert([
            [
                'descripcion' => 'CIIU 1',
            ]
        ]);   \App\Models\Ciiu::insert([
            [
                'descripcion' => 'CIIU 2',
            ]
        ]); \App\Models\Ciiu::insert([
            [
                'descripcion' => 'CIIU 3',
            ]
        ]);
    }
}
