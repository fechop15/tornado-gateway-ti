<?php

namespace Database\Seeders;

use App\Models\EstadoCivil;
use Illuminate\Database\Seeder;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=EstadoCivilSeeder
        EstadoCivil::create([
            'descripcion'=>'soltero',
        ]);
        EstadoCivil::create([
            'descripcion'=>'casado',
        ]);
        EstadoCivil::create([
            'descripcion'=>'Union Libre',
        ]);
        EstadoCivil::create([
            'descripcion'=>'divorciado',
        ]);
        EstadoCivil::create([
            'descripcion'=>'otro',
        ]);
    }
}
