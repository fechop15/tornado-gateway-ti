<?php

namespace Database\Seeders;

use App\Models\TipoCertificaciones;
use Illuminate\Database\Seeder;

class TipoCertificacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=TipoCertificacionesSeeder
        TipoCertificaciones::create([
            'id'=>'1',
            'descripcion'=>'Certificacion 1',
        ]);
        TipoCertificaciones::create([
            'id'=>'2',
            'descripcion'=>'Certificacion 2',
        ]);
    }
}
