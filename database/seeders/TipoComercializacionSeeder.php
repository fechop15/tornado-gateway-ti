<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TipoComercializacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=TipoComercializacionSeeder
        \App\Models\Tipo_comercionalizacion::insert([
            [
                'descripcion' => 'Directa',
            ]
        ]);
        \App\Models\Tipo_comercionalizacion::insert([
            [
                'descripcion' => 'Tercero',
            ]
        ]);
    }
}
