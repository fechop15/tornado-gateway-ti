<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SectorEmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=SectorEmpresaSeeder
        //1. El sector del transporte
        //2. El sector financiero
        //3. El sector del comercio
        //4. El sector de la construcción
        //5. El sector minero y energético
        //6. El sector de las comunicaciones
        \App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'transporte',
            ]
        ]);\App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'financiero',
            ]
        ]);\App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'comercio',
            ]
        ]);\App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'construcción',
            ]
        ]);\App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'minero y energético',
            ]
        ]);\App\Models\Sector_empresa::insert([
            [
                'descripcion' => 'comunicaciones',
            ]
        ]);
    }
}
