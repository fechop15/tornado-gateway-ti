<?php

namespace Database\Seeders;

use App\Models\Servicio;
use Illuminate\Database\Seeder;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=ServiciosSeeder

        Servicio::create([
            'id'=>'1',
            'descripcion'=>'Desarrollo a la Medida',
        ]);Servicio::create([
            'id'=>'2',
            'descripcion'=>'Comercialización de producto TI estandar',
        ]);Servicio::create([
            'id'=>'3',
            'descripcion'=>'Infraestructura tecnologica',
        ]);Servicio::create([
            'id'=>'4',
            'descripcion'=>'Seguridad Informática',
        ]);Servicio::create([
            'id'=>'5',
            'descripcion'=>'Investigación; Redes y comunicaciones',
        ]);Servicio::create([
            'id'=>'6',
            'descripcion'=>'Diseño y desarrollo Web',
        ]);Servicio::create([
            'id'=>'7',
            'descripcion'=>'Diseño y desarrollo de Apps',
        ]);
    }
}
