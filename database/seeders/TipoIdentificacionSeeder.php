<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TipoIdentificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=TipoIdentificacionSeeder
        \App\Models\Tipo_identificacion::insert([
            [
                'descripcion' => 'Cedula de ciudadania',
            ]
        ]);
        \App\Models\Tipo_identificacion::insert([
            [
                'descripcion' => 'Tarjeta de identidad',
            ]
        ]);
        \App\Models\Tipo_identificacion::insert([
            [
                'descripcion' => 'Pasaporte',
            ]
        ]);
        \App\Models\Tipo_identificacion::insert([
            [
                'descripcion' => 'Cedula extranjera',
            ]
        ]);
    }
}
