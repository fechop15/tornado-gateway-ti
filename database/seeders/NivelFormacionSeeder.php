<?php

namespace Database\Seeders;

use App\Models\NivelFormacion;
use Illuminate\Database\Seeder;

class NivelFormacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=NivelFormacionSeeder
        //Bachiller; Técnico; Tecnólogo; Pregrado; Maestría; Doctorado
        NivelFormacion::create([
            'id'=>'1',
            'descripcion'=>'Bachiller',
        ]);
        NivelFormacion::create([
            'id'=>'2',
            'descripcion'=>'Técnico',
        ]);
        NivelFormacion::create([
            'id'=>'3',
            'descripcion'=>'Tecnólogo',
        ]);
        NivelFormacion::create([
            'id'=>'4',
            'descripcion'=>'Pregrado',
        ]);
        NivelFormacion::create([
            'id'=>'5',
            'descripcion'=>'Maestría',
        ]);
        NivelFormacion::create([
            'id'=>'6',
            'descripcion'=>'Doctorado',
        ]);
    }
}
