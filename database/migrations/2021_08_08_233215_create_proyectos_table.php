<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id();
            $table->longText('nombre');
            $table->longText('descripcion')->nullable();
            $table->longText('obj_general')->nullable();
            $table->longText('obj_especifico')->nullable();
            $table->longText('area_tematica')->nullable();
            $table->longText('beneficios')->nullable();
            $table->longText('estado_desarrollo')->nullable();
            $table->longText('propiedad_intelectual')->nullable();
            $table->longText('diferencial')->nullable();

            $table->string('explicacion_PDF_PPT')->nullable();
            $table->string('url_video')->nullable();
            $table->string('presupuesto_EXEL')->nullable();

            $table->string('estado')->default("Pendiente");
            $table->unsignedBigInteger('empresa_id')->unsigned()->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
