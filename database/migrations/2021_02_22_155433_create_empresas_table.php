<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('razonsocial')->nullable();
            $table->string('nombrecomercial')->nullable();
            $table->string('rut_rutaarachivo')->nullable();
            $table->string('breve_descr_empresa')->nullable();
            $table->string('direccion_correspondencia')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
            $table->string('identi_rutaarchivo')->nullable();
            $table->string('identifica_representante_legal')->nullable();
            $table->string('nombte_representante_legal')->nullable();
            $table->string('anno_creacion_empresa')->nullable();
            $table->string('num_empleados_directos')->nullable();
            $table->string('num_empleados_indirectos')->nullable();
            $table->string('camaracomercio_rutaarachivo')->nullable();
            $table->string('prome_ventas_anuales')->nullable();
            $table->string('balance_general_rutaarachivo')->nullable();
            $table->string('total_activos')->nullable();
            $table->string('num_sedes')->nullable();
            $table->string('ciudades_otras_sedes')->nullable();
            $table->string('pagina_web')->nullable();
            $table->string('link_portafolio')->nullable();
            $table->string('logo_rutararchivo')->nullable();
            $table->string('link_facebook')->nullable();
            $table->string('link_instagram')->nullable();
            $table->string('link_linkedin')->nullable();
            $table->string('link_twitter')->nullable();
            $table->string('tiene_canales_virtuales_atencion')->nullable();;
            $table->string('realiza_ventas_linea')->nullable();;
            $table->string('certifica_iso9001')->nullable();;
            $table->string('certifica_iso14001')->nullable();;
            $table->string('otra_certifica_internacional')->nullable();;
            $table->string('uso_energias_alternativas')->nullable();;
            $table->string('num_convenios_estra_alianza_estable')->nullable();;
            $table->string('porce_presupues_info_personal')->nullable();;
            $table->string('cuenta_software_crm')->nullable();;
            $table->string('cuenta_software_erp')->nullable();;
            $table->string('num_persona_dominio_segunda_lengua')->nullable();;
            $table->string('num_mujeres_empleadas')->nullable();;
            $table->string('num_empleados_con_postgrado')->nullable();;
            $table->string('edad_promedio_empleados')->nullable();;
            $table->string('num_madres_cabeza_hogar')->nullable();;
            $table->string('ingremento_ventas')->nullable();;
            $table->string('llegar_nuevos_mercados')->nullable();;
            $table->string('lanzamiento_nuevos_productos')->nullable();;
            $table->string('mejoramiento_productividad')->nullable();;
            $table->string('incremento_capacidad_productiva')->nullable();;
            $table->string('desarrollo_nuevos_canales')->nullable();;
            $table->string('implementacion_ti')->nullable();;
            $table->string('infraestructura_fisica')->nullable();;
            $table->string('compra_maquinaria_equipos')->nullable();;
            $table->string('num_empleados_habilidades_ti')->nullable();;
            $table->string('num_empleados_region')->nullable();
            $table->longText('mercado')->nullable();
            $table->longText('recurso_humano')->nullable();

            $table->string('clave_rut')->nullable();
            $table->string('camara_comercio_rutaarachivo')->nullable();
            $table->string('celular_representante_legal')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('tipoid_represen_legal')->unsigned()->nullable();
            $table->foreign('tipoid_represen_legal')->references('id')->on('tipo_identificacions');

            $table->integer('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('sector_empresa_id')->unsigned()->nullable();
            $table->foreign('sector_empresa_id')->references('id')->on('sector_empresas');

            $table->unsignedBigInteger('ciiu_id')->unsigned()->nullable();
            $table->foreign('ciiu_id')->references('id')->on('ciius');

            $table->unsignedBigInteger('actividad_economica_1_id')->unsigned()->nullable();
            $table->foreign('actividad_economica_1_id')->references('id')->on('actividad_economicas');

            $table->unsignedBigInteger('actividad_economica_2_id')->unsigned()->nullable();
            $table->foreign('actividad_economica_2_id')->unsigned()->references('id')->on('actividad_economicas');

            $table->unsignedBigInteger('tipo_operacion_id')->unsigned()->nullable();
            $table->foreign('tipo_operacion_id')->references('id')->on('tipo_operacions');

            $table->unsignedBigInteger('tipo_comercionalizacion_id')->unsigned()->nullable();
            $table->foreign('tipo_comercionalizacion_id')->references('id')->on('tipo_comercionalizacions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
