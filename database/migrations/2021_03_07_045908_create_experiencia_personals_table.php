<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperienciaPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiencia_personals', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_empresa_empleadora');
            $table->string('actividad_economina_empleador');
            $table->string('principal_funciones_cargo');
            $table->string('fecha_inicio');
            $table->string('fecha_retiro');

            $table->unsignedBigInteger('persona_id')->unsigned()->nullable();
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencia_personals');
    }
}
