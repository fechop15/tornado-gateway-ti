<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducacionNoFormalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educacion_no_formals', function (Blueprint $table) {
            $table->id();
            $table->string('num_horas');
            $table->string('fecha_certificado');
            $table->string('tituloEstudio');

            $table->unsignedBigInteger('persona_id')->unsigned()->nullable();
            $table->foreign('persona_id')->references('id')->on('personas');

            $table->unsignedBigInteger('nivel_formacion_no_formal_id')->unsigned()->nullable();
            $table->foreign('nivel_formacion_no_formal_id')->references('id')->on('nivel_formacion_no_formals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educacion_no_formals');
    }
}
