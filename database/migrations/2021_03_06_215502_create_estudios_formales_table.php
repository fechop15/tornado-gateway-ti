<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosFormalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios_formales', function (Blueprint $table) {
            $table->id();
            $table->string('titulo_obtenido')->nullable();
            $table->boolean('finalizado')->nullable();
            $table->string('fecha_obtenido_titulo')->nullable();
            $table->string('diploma_rutaarchivo')->nullable();

            $table->unsignedBigInteger('persona_id')->unsigned()->nullable();
            $table->foreign('persona_id')->references('id')->on('personas');

            $table->unsignedBigInteger('nivel_formacion_id')->unsigned()->nullable();
            $table->foreign('nivel_formacion_id')->references('id')->on('nivel_formacions');

            $table->unsignedBigInteger('sub_area_conocimiento_id')->unsigned()->nullable();
            $table->foreign('sub_area_conocimiento_id')->references('id')->on('sub_area_conocimientos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios_formales');
    }
}
