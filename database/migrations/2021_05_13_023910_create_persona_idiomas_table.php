<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonaIdiomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona_idiomas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idioma_id')->unsigned()->nullable();
            $table->foreign('idioma_id')->references('id')->on('idiomas');

            $table->unsignedBigInteger('persona_id')->unsigned()->nullable();
            $table->foreign('persona_id')->references('id')->on('personas');

            $table->string('escrito')->default('Bajo');
            $table->string('leido')->default('Bajo');
            $table->string('hablado')->default('Bajo');
            $table->string('escuchado')->default('Bajo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona_idiomas');
    }
}
