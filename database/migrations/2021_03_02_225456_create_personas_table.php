<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->id();
            $table->string('identificacion')->nullable();
            $table->string('fecha_nacimiento')->nullable();
            $table->string('celular')->nullable();
            $table->string('correo_electro_personal')->nullable();
            $table->string('link_instagram')->nullable();
            $table->string('link_linkedin')->nullable();
            $table->string('link_twitter')->nullable();
            $table->string('link_facebook')->nullable();
            $table->string('num_personas_dependientes')->nullable();
            $table->string('numero_contacto_emergencias')->nullable();
            $table->string('nombre_contacto_emergencias')->nullable();
            $table->string('direccion_recidencia')->nullable();
            $table->string('nivel_ingles_certifica_rutaarachivo')->nullable();
            $table->string('identificacion_rurataarchivo')->nullable();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('genero_id')->unsigned()->nullable();
            $table->foreign('genero_id')->references('id')->on('generos');

            $table->unsignedBigInteger('tipoid')->unsigned()->nullable();
            $table->foreign('tipoid')->references('id')->on('tipo_identificacions');

            $table->integer('ciudad_nacimiento_id')->unsigned()->nullable();
            $table->foreign('ciudad_nacimiento_id')->references('id')->on('cities');

            $table->integer('ciudad_recidencia_id')->unsigned()->nullable();
            $table->foreign('ciudad_recidencia_id')->references('id')->on('cities');

            $table->unsignedBigInteger('nivel_ingles_id')->unsigned()->nullable();
            $table->foreign('nivel_ingles_id')->references('id')->on('nivel_ingles');

            $table->unsignedBigInteger('rh_sanguineo_id')->unsigned()->nullable();
            $table->foreign('rh_sanguineo_id')->references('id')->on('rh_sanguineos');

            $table->unsignedBigInteger('estado_civil_id')->unsigned()->nullable();
            $table->foreign('estado_civil_id')->references('id')->on('estado_civils');

            $table->unsignedBigInteger('eps_id')->unsigned()->nullable();
            $table->foreign('eps_id')->references('id')->on('entidades_seguridad_socials');

            $table->unsignedBigInteger('pensiones_id')->unsigned()->nullable();
            $table->foreign('pensiones_id')->references('id')->on('entidades_seguridad_socials');

            $table->unsignedBigInteger('cesantias_id')->unsigned()->nullable();
            $table->foreign('cesantias_id')->references('id')->on('entidades_seguridad_socials');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
