<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizacions', function (Blueprint $table) {
            $table->id();
            $table->string('nombreEmpresa');
            $table->string('cotizacion_PDF')->nullable();
            $table->string('documento_PDF')->nullable();
            $table->string('camara_comercio_PDF')->nullable();
            $table->string('rut_PDF')->nullable();
            $table->string('representante_legal_PDF')->nullable();
            $table->unsignedBigInteger('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizacions');
    }
}
