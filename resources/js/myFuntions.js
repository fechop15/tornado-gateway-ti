
(function (cash) {
    "use strict";

    function loadIframe(iframeName, url) {
        let $iframe = cash('#' + iframeName);
        if ($iframe.length) {
            $iframe.attr('src',url);
            return false;
        }
        return true;
    }

})(cash);


