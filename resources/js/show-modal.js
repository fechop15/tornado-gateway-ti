(function (cash) {
    "use strict";

    // Show modal
    cash("#programmatically-show-modal").on("click", function () {
        cash("#programmatically-modal").modal("show");
    });

    // Hide modal
    cash("#programmatically-hide-modal").on("click", function () {
        cash("#programmatically-modal").modal("hide");
    });

    // Toggle modal
    cash("#programmatically-toggle-modal").on("click", function () {
        cash("#programmatically-modal").modal("toggle");
    });

    //modal permisos
    cash("#modalGuardar").on("click", function () {
        cash("#modal").modal("show");
        cash('#permisos-form')[0].reset();
        cash('#actualizar').hide();
        cash('#eliminar').hide();
        cash('#guardar').show();
        cash("#error").hide();
        cash("#accion-modal").html('Registrar');
    });

    //modal permisos
    cash("#modalUsuario").on("click", function () {
        cash('#modal').modal('show');
        cash('#form-usuario')[0].reset();
        cash('#actualizar').hide();
        cash('#guardar').show();
        cash('#eliminar').hide();
        cash("#error").hide();
        cash("#accion-modal").html('Registrar');
        cash("#password").prop('disabled', false);
    });
})(cash);
