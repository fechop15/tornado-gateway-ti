@extends('../layout/' . $layout)

@section('head')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Registro - GATEWAYTI</title>
    {!! ReCaptcha::htmlScriptTagJsApi() !!}
@endsection

@section('content')
    <p class="fixed text-center" style="right: 20px;"><img class="m-auto" src="/dist/images/tornado_digital_blue.png" width="150" alt="logo tornado">Sistema de informacion tornado digital</p>
    <p class="fixed text-white dark:text-gray-500 text-center" style="bottom: 10px"><a href="https://gatewayti.com.co/" target="_blank"><img style="display: initial;" src="https://gatewayti.com.co/images/logo.png"  width="50" alt="logo tornado">©copyright</a></p>

    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
            <!-- BEGIN: Register Info -->
            <div class="hidden xl:flex flex-col min-h-screen">
                <a href="" class="-intro-x flex items-center pt-5">
{{--                    <img alt="Midone Tailwind HTML Admin Template" class="w-6"--}}
{{--                         src="{{ asset('dist/images/logo.svg') }}">--}}
{{--                    <span class="text-white text-lg ml-3">--}}
{{--                        Mid<span class="font-medium">One</span>--}}
{{--                    </span>--}}
                </a>
                <div class="my-auto">
                    <img alt="gateway image" class="-intro-x w-1/2 -mt-16" src="https://gatewayti.com.co/images/2019/09/06/logo.png">
                    <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">¡Somos tu puerta de enlace a la ciencia, <br>  tecnología e innovación!</div>
                    <div class="-intro-x mt-5 text-lg text-white dark:text-gray-500"></div>
                </div>
            </div>
            <!-- END: Register Info -->
            <!-- BEGIN: Register Form -->
            <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">

                <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                    <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">Registro</h2>
                    <form id="register-form" >
                    <div class="intro-x mt-2 text-gray-500 dark:text-gray-500 xl:hidden text-center">
                    </div>
                    <div class="intro-x mt-8">

                        <input type="text" id="input-nombres" name="nombre"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block"
                               placeholder="Nombre empresa">
                        <div id="error-nombres" class="register__input-error w-5/6 text-theme-6 mt-2"></div>
                        <input type="text" id="input-apellidos" name="apellidos"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 hidden"
                               placeholder="Apellidos">
                        <div id="error-apellidos" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                        <input type="number" id="input-telefono" name="telefono"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4"
                               placeholder="Telefono">
                        <div id="error-telefono" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                        <select id="input-tipo_usuario" name="tipo_usuario" class="intro-x login__input input form-select border border-gray-300 block mt-4">
{{--                            <option disabled selected value>Tipo de usuario</option>--}}
                            @foreach($tipo_usuarios as $tipo_usuario)
                                @if($tipo_usuario->id!=0 && $tipo_usuario->id!=1)
                                <option value="{{$tipo_usuario->id}}">{{$tipo_usuario->descripcion}}</option>
                                @endif
                            @endforeach
                        </select>
                        <div id="error-tipo_usuario" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                        <input type="email" id="input-email" name="email"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4"
                               placeholder="Correo">
                        <div id="error-email" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                        <input type="password" name="password" id="input-password"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4"
                               placeholder="Contraseña">
                        <div id="error-password" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                        <input type="password" name="password_confirmation" id="input-password_confirmation"
                               class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4"
                               placeholder="Confirma tu contraseña">
                        <div id="error-password-confirmation" class="register__input-error w-5/6 text-theme-6 mt-2"></div>
                        <div class="capcha w-5/6 text-theme-6 mt-2">
                            {!! htmlFormSnippet() !!}
                        </div>
                        <div id="error-recaptcha" class="register__input-error w-5/6 text-theme-6 mt-2"></div>

                    </div>
                    <div class="intro-x flex items-center text-gray-700 dark:text-gray-600 mt-4 text-xs sm:text-sm">
                        <input type="checkbox" class="input border mr-2" id="input-politicas" name="politicas">
                        <label class="cursor-pointer select-none" for="politicas">Acepto politicas de </label>
                        <a class="text-theme-1 dark:text-theme-10 ml-1" href="/politica_de_datos" target="_blank">tratamiento de datos</a>.
                    </div>
                    </form>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <button id="btn-register" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">
                            Registrarme
                        </button>
                        <button id="btn-login"
                            class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">
                            Ingresar
                        </button>
                    </div>
                </div>

            </div>
            <!-- END: Register Form -->
        </div>
    </div>
@endsection
@section('script')
    <script>
        cash(function () {
            async function register() {
                // Reset state
                cash('#register-form').find('.input').removeClass('border-theme-6')
                cash('#register-form').find('.register__input-error').html('')

                // Post form
                let nombres = cash('#input-nombres').val()
                let apellidos = cash('#input-apellidos').val()
                let telefono = cash('#input-telefono').val()
                let tipo_usuario = cash('#input-tipo_usuario').val()
                let email = cash('#input-email').val()
                let password = cash('#input-password').val()
                let password_confirmation = cash('#input-password_confirmation').val()
                let politicas = cash('#input-politicas').val()
                let capcha = cash("#register-form")[0][7].value
                // Loading state
                cash('#btn-register').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1500)

                axios.post(`register`, {
                    nombres: nombres,
                    apellidos: apellidos,
                    telefono: telefono,
                    tipo_usuario: tipo_usuario,
                    email: email,
                    password: password,
                    password_confirmation: password_confirmation,
                    politicas: politicas,
                    recaptcha: capcha,
                }).then(res => {
                    Swal.fire({
                            title: "Felicidades!",
                            text: "Te haz registrado en Tornado, te enviamos un correo de bienvenida ingresa en la plataforma y completa tu perfil",
                            type: "success",
                            allowOutsideClick: false,
                        }
                    ).then((result) => {
                        if (result.value) {
                            location.href = '{{ route('login') }}'
                        }
                    });
                }).catch(err => {
                    cash('#btn-register').html('Registrarme')
                    if (err.response.data.message != 'Wrong email or password.') {
                        for (const [key, val] of Object.entries(err.response.data.errors)) {
                            cash(`#input-${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                    } else {
                        cash(`#input-password`).addClass('border-theme-6')
                        cash(`#error-password`).html(err.response.data.message)
                    }
                })
            }

            cash('#register-form').on('keyup', function(e) {
                if (e.keyCode === 13) {
                    register()
                }
            })

            cash('#btn-register').on('click', function() {
                register()
            })

            cash('#btn-login').on('click', function() {
                location.href = '{{ route('login') }}'
            })
        })
    </script>
@endsection
