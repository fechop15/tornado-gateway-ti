@extends('../layout/' . $layout)

@section('head')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Login - GATEWAYTI</title>
@endsection

@section('content')
    <p class="fixed text-center" style="right: 20px;">
        <img class="m-auto" src="/dist/images/tornado_digital_blue.png" width="150"
             alt="logo tornado">Sistema de informacion tornado digital</p>
    <p class="fixed text-white dark:text-gray-500 text-center" style="bottom: 10px">
        <a href="https://gatewayti.com.co/" target="_blank">
            <img style="display: initial;" src="https://gatewayti.com.co/images/logo.png" width="50" alt="logo tornado">©copyright</a>
    </p>

    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
            <!-- BEGIN: Login Info -->
            <div class="hidden xl:flex flex-col min-h-screen">
                <a href="" class="-intro-x flex items-center pt-5">
                </a>
                <div class="my-auto">
                    <img alt="Gateway Imagen" class="-intro-x w-1/2 -mt-16"
                         src="https://gatewayti.com.co/images/2019/09/06/logo.png">
                    <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">¡Somos tu puerta de enlace a la ciencia, <br>  tecnología e innovación!</div>

                    <div class="-intro-x mt-5 text-lg text-white dark:text-gray-500">
                    </div>
                </div>
            </div>
            <!-- END: Login Info -->
            <!-- BEGIN: Login Form -->
            <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                <div
                    class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                    <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">Recuperar
                        contraseña</h2>
                    <div class="intro-x mt-2 text-gray-500 xl:hidden text-center">
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {!! session('status') !!}
                    <form method="POST" action="{{ route('password.update') }}">

                        <div class="intro-x mt-8">
                            @csrf
                            <input type="hidden" name="token" value="{{ $request->route('token') }}">

                            <input type="email" id="email" name="email"
                                   class="intro-x login__input form-control py-3 px-4 border-gray-300 block"
                                   placeholder="Correo" value="{{ $email ?? old('email') }}">
                            @error('email')
                            <div id="error-email"
                                 class="login__input-error w-5/6 text-theme-6 mt-2">{{ $message }}</div>
                            @enderror

                            <input type="password" id="password" name="password"
                                   class="mt-3 intro-x login__input form-control py-3 px-4 border-gray-300 block @error('password') is-invalid @enderror"
                                   placeholder="contraseña" value="{{ $email ?? old('email') }}">
                            @error('password')
                            <div id="error-email"
                                 class="login__input-error w-5/6 text-theme-6 mt-2">{{ $message }}</div>
                            @enderror

                            <input type="password" id="password-confirm" name="password_confirmation" placeholder="confirmar contraseña"
                                   class="mt-3 intro-x login__input form-control py-3 px-4 border-gray-300 block">
                        </div>

                        <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                            <button id="btn-login" type="submit"
                                    class="btn btn-primary py-3 px-4 w-full xl:w-52 xl:mr-3 align-top">Recuperar
                                contraseña
                            </button>
                            <button id="btn-register"
                                    class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">
                                Ingresar
                            </button>
                        </div>
                    </form>
                    <div class="intro-x mt-10 xl:mt-24 text-gray-700 dark:text-gray-600 text-center xl:text-left">
                        Para registrarte recuerda leer los <br> <a class="text-theme-1 dark:text-theme-10" href="/politica_de_datos">terminos y condiciones & Politica de privacidad</a>
                    </div>
                </div>
            </div>
            <!-- END: Login Form -->
        </div>
    </div>
@endsection
<script>
    cash(function () {
        cash('#btn-login').on('click', function () {
            location.href = '{{ route('login') }}'
        })
    })
</script>
