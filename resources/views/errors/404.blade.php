<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light">
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="{{ asset('dist/images/logo.svg') }}" rel="shortcut icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Error 404">
    <meta name="author" content="GatewayTI">
    <title>Error 404 - {{ ucwords(config('app.name'))}}</title>

    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" />
    <!-- END: CSS Assets-->
</head>
<!-- END: Head -->
<body class="app">
<div class="container">
    <!-- BEGIN: Error Page -->
    <div class="error-page flex flex-col lg:flex-row items-center justify-center h-screen text-center lg:text-left">
        <div class="-intro-x lg:mr-20">
            <img alt="creintec" class="h-48 lg:h-auto" src="{{ asset('dist/images/logo-tornado.png') }}">
        </div>
        <div class="text-white mt-10 lg:mt-0">
            <div class="intro-x text-6xl font-medium">404</div>
            <div class="intro-x text-xl lg:text-3xl font-medium">UPS. Esta página ha desaparecido.</div>
            <div class="intro-x text-lg mt-3">Es posible que haya escrito mal la dirección o que la página se haya movido.</div>
            <a class="intro-x btn btn-warning shadow-md mt-3" href="/">Regresar al inico</a>
        </div>
    </div>
    <!-- END: Error Page -->
</div>
</body>
</html>

