<!-- BEGIN: Top Bar -->
<div class="top-bar">
    <!-- BEGIN: Breadcrumb -->
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex">
        <a href="">Applicacion</a>
        <i data-feather="chevron-right" class="breadcrumb__icon"></i>
        <a href="" class="breadcrumb--active">{{ucwords(Route::current()->getName())}} </a>
    </div>
    <!-- END: Breadcrumb -->
    <!-- BEGIN: Search -->
    <div class="intro-x relative mr-3 sm:mr-6">
{{--        <div class="search hidden sm:block">--}}
{{--            <input type="text" class="search__input form-control border-transparent placeholder-theme-13" placeholder="Search...">--}}
{{--            <i data-feather="search" class="search__icon dark:text-gray-300"></i>--}}
{{--        </div>--}}
{{--        <a class="notification sm:hidden" href="">--}}
{{--            <i data-feather="search" class="notification__icon dark:text-gray-300"></i>--}}
{{--        </a>--}}
{{--        <div class="search-result">--}}
{{--            <div class="search-result__content">--}}
{{--                <div class="search-result__content__title">Pages</div>--}}
{{--                <div class="mb-5">--}}
{{--                    <a href="" class="flex items-center">--}}
{{--                        <div class="w-8 h-8 bg-theme-18 text-theme-9 flex items-center justify-center rounded-full">--}}
{{--                            <i class="w-4 h-4" data-feather="inbox"></i>--}}
{{--                        </div>--}}
{{--                        <div class="ml-3">Mail Settings</div>--}}
{{--                    </a>--}}
{{--                    <a href="" class="flex items-center mt-2">--}}
{{--                        <div class="w-8 h-8 bg-theme-17 text-theme-11 flex items-center justify-center rounded-full">--}}
{{--                            <i class="w-4 h-4" data-feather="users"></i>--}}
{{--                        </div>--}}
{{--                        <div class="ml-3">Users & Permissions</div>--}}
{{--                    </a>--}}
{{--                    <a href="" class="flex items-center mt-2">--}}
{{--                        <div class="w-8 h-8 bg-theme-14 text-theme-10 flex items-center justify-center rounded-full">--}}
{{--                            <i class="w-4 h-4" data-feather="credit-card"></i>--}}
{{--                        </div>--}}
{{--                        <div class="ml-3">Transactions Report</div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="search-result__content__title">Users</div>--}}
{{--                <div class="mb-5">--}}
{{--                    @foreach (array_slice($fakers, 0, 4) as $faker)--}}
{{--                        <a href="" class="flex items-center mt-2">--}}
{{--                            <div class="w-8 h-8 image-fit">--}}
{{--                                <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('dist/images/' . $faker['photos'][0]) }}">--}}
{{--                            </div>--}}
{{--                            <div class="ml-3">{{ $faker['users'][0]['name'] }}</div>--}}
{{--                            <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">{{ $faker['users'][0]['email'] }}</div>--}}
{{--                        </a>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--                <div class="search-result__content__title">Products</div>--}}
{{--                @foreach (array_slice($fakers, 0, 4) as $faker)--}}
{{--                    <a href="" class="flex items-center mt-2">--}}
{{--                        <div class="w-8 h-8 image-fit">--}}
{{--                            <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('dist/images/' . $faker['images'][0]) }}">--}}
{{--                        </div>--}}
{{--                        <div class="ml-3">{{ $faker['products'][0]['name'] }}</div>--}}
{{--                        <div class="ml-auto w-48 truncate text-gray-600 text-xs text-right">{{ $faker['products'][0]['category'] }}</div>--}}
{{--                    </a>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <!-- END: Search -->
    <!-- BEGIN: Notifications -->
    <div class="intro-x dropdown mr-auto sm:mr-6">
{{--        <div class="dropdown-toggle notification notification--bullet cursor-pointer" role="button" aria-expanded="false">--}}
{{--            <i data-feather="bell" class="notification__icon dark:text-gray-300"></i>--}}
{{--        </div>--}}
{{--        <div class="notification-content pt-2 dropdown-menu">--}}
{{--            <div class="notification-content__box dropdown-menu__content box dark:bg-dark-6">--}}
{{--                <div class="notification-content__title">Notifications</div>--}}
{{--                @foreach (array_slice($fakers, 0, 5) as $key => $faker)--}}
{{--                    <div class="cursor-pointer relative flex items-center {{ $key ? 'mt-5' : '' }}">--}}
{{--                        <div class="w-12 h-12 flex-none image-fit mr-1">--}}
{{--                            <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="{{ asset('dist/images/' . $faker['photos'][0]) }}">--}}
{{--                            <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>--}}
{{--                        </div>--}}
{{--                        <div class="ml-2 overflow-hidden">--}}
{{--                            <div class="flex items-center">--}}
{{--                                <a href="javascript:;" class="font-medium truncate mr-5">{{ $faker['users'][0]['name'] }}</a>--}}
{{--                                <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">{{ $faker['times'][0] }}</div>--}}
{{--                            </div>--}}
{{--                            <div class="w-full truncate text-gray-600 mt-0.5">{{ $faker['news'][0]['short_content'] }}</div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <!-- END: Notifications -->
    <!-- BEGIN: Account Menu -->
    <div class="intro-x dropdown w-8 h-8">
        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in" role="button" aria-expanded="false">
{{--            @ if(auth()->user()->tipo_usuario_id ==2)--}}
{{--            <img alt="Tornado" src="{{auth()->user()->empresa->logo_rutararchivo?asset(auth()->user()->empresa->logo_rutararchivo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">--}}
{{--            @ else--}}
{{--                <img alt="Tornado" src="{{auth()->user()->photo?asset(auth()->user()->photo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">--}}
{{--            @ endif--}}
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings block mx-auto"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
        </div>
        <div class="dropdown-menu w-56">
            <div class="dropdown-menu__content box bg-theme-26 dark:bg-dark-6 text-white">
                <div class="p-4 border-b border-theme-27 dark:border-dark-3">
                    <div class="font-medium">{{ auth()->user()->nombres." ".auth()->user()->apellidos }}</div>
                    <div class="text-xs text-theme-28 mt-0.5 dark:text-gray-600">{{ auth()->user()->tipo_usuario->descripcion }}</div>
                </div>
                <div class="p-2">
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Perfil
                    </a>
{{--                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md">--}}
{{--                        <i data-feather="edit" class="w-4 h-4 mr-2"></i> Add Account--}}
{{--                    </a>--}}
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md">
                        <i data-feather="lock" class="w-4 h-4 mr-2"></i> Cambiar Contraseña
                    </a>
                    <a href="{{ route('ayuda') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md">
                        <i data-feather="help-circle" class="w-4 h-4 mr-2"></i> Ayuda
                    </a>
                </div>
                <div class="p-2 border-t border-theme-27 dark:border-dark-3">
                    <a href="{{ route('logout') }}" class="flex items-center block p-2 transition duration-300 ease-in-out hover:bg-theme-1 dark:hover:bg-dark-3 rounded-md">
                        <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Salir
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Account Menu -->
</div>
<!-- END: Top Bar -->
