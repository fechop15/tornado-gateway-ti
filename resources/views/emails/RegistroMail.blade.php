<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>GATEWAYTI</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style>
        * {
            font-family: sans-serif;
        }
        td {
            color: #737B7D;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
    <tr>
        <td align="center" style="padding: 40px 0 30px 0;">
            <img src="https://tornado.gatewayti.com.co/dist/images/tornado-largo.png" alt="GATEWAYTI"
                 style="width: 100%; display: block;"/>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff">

            <table border="0" style="width: 100%;padding: 0 10px;margin-bottom:50px">
                <thead>
                <tr>
                    <th colspan="2" style="text-align: left;color: #102966;font-size: 20px;font-weight: bold">Bienvenido al programa <br>TORNADO DIGITAL ® de GATEWAY TI MONTERÍA SAS.
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="2" style="text-align: left;padding: 10px 0px;padding-bottom: 20px">
                        Para nosotros es un placer darles la Bienvenida a TORNADO DIGITAL ® una plataforma creada para generar una región creativa e innovadora, con mejores oportunidades para todos.<br>
                        <br>
                        Este es el primer paso para pertenecer a nuestro programa de transformación digital de empresas, y conectarlas a con los demás actores de ecosistema de Investigación, Desarrollo e Innovación (I+D+i). <br><br>
                        A partir de ahora, el equipo Gateway TI Montería SAS y los servicios de la plataforma Tornado Digital se encuentran a su disposición, y estaremos acompañándolos en la ruta de transformación de su organización. <br><br>
                        Lo invitamos a revisar el contenido que tenemos dispuesto en nuestra página Web <a
                            href="https://www.gatewayti.com.co/" target="_blank">https://www.gatewayti.com.co/</a>
                        <br><br>
                        Cordialmente,
                        <br><br>
                        <strong>Equipo Tornado Digital ®.</strong><br>
                        GATEWAY TI Montería SAS <br>
                        Celular: 3005708876<br>
                        Email: conacto@creinntec.co<br>
                        Dirección: Calle 65 # 6-24 oficina 326. Centro comercial Places Mall Recreo<br>
                        Montería, Córdoba<br>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#0076c0">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 15px 20px;">
                <tr>
                    <td style="color: #ffffff; text-align: center">
                        <strong>© 2021 GatewayTI</strong>
                    </td>
                    <td align="right">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
