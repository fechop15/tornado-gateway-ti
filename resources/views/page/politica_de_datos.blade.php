@extends('../layout/' . $layout)

@section('head')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <title>Politica de datos - GATEWAYTI</title>
@endsection

@section('content')
    <p class="fixed text-center" style="right: 20px;"><img class="m-auto" src="dist/images/logo-tornado.png" width="150" alt="logo tornado">Sistema de informacion tornado digital</p>
    <p class="fixed text-white dark:text-gray-500 text-center" style="bottom: 10px"><a href="https://gatewayti.com.co/" target="_blank"><img style="display: initial;" src="https://gatewayti.com.co/images/logo.png"  width="50" alt="logo tornado">©copyright</a></p>

    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
            <!-- BEGIN: Login Info -->
            <div class="hidden xl:flex flex-col min-h-screen">
                <a href="" class="-intro-x flex items-center pt-5">
{{--                    <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="http://www.creinntec.co/wp-content/uploads/2020/10/logo-crein.png">--}}
{{--                    <span class="text-white text-lg ml-3">--}}
{{--                        Mid<span class="font-medium">One</span>--}}
{{--                    </span>--}}


                </a>
                <div class="my-auto">
                    <img alt="Midone Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="https://gatewayti.com.co/images/2019/09/06/logo.png">
                    <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">¡Somos tu perta a la ciencia, <br>  tecnología e innovación!</div>
                    <div class="-intro-x mt-5 text-lg text-white dark:text-gray-500">Asociación de Industrias creativas, de investigación,<br> desarrollo e innovación tecnológica de Córdoba</div>
                </div>
            </div>
            <!-- END: Login Info -->
            <!-- BEGIN: Login Form -->
            <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                    <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">Politica de datos</h2>
                    <div class="intro-x mt-2 text-gray-500">
                        Condiciones del Servicio Adicionales de Tornado Digital y el Sistema de información Tornado Digital (R)<br><br>
                        Última modificación: 25 de abrir de 2021<br><br>
                        Al usar Tornado Digital (R), confirma que acepta las Condiciones del Servicio de Gateway TI (que se encuentran en su contrato) y las Condiciones del Servicio Adicionales de Tornado Digital (R).
                        <br><br>
                        Estas Condiciones del Servicio Adicionales de Tornado digital (R) se aplicarán a la versión de código ejecutable y web de Tornado Digital (R) . El código fuente de Tornado Digital (R) no está disponible de forma gratuita de conformidad con acuerdos de licencia de software tipo comercial.
                        <br><br>
                        El uso que usted haga del sistema de información Tornado Digital(R) también está sujeto a las condiciones y regulaciones de Software y datos de la ley Colombiana.
                    </div>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <button id="btn-login" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">Ingresar</button>
                        <button id="btn-register" class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">Registarme</button>
                    </div>
                </div>
            </div>
            <!-- END: Login Form -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {

            cash('#btn-login').on('click', function() {
                location.href = '/'
            })

            cash('#btn-register').on('click', function() {

                location.href = '{{ route('register') }}'
            })
        })
    </script>
@endsection
