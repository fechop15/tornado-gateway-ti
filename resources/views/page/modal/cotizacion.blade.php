<div class="modal fade" id="modal-cotizacion-registro" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary text-light">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Registrar</span>
                    <span class="fw-light">Cotizacion</span>
                </h5>
            </div>
            <a data-dismiss="modal" href="javascript:;">
                <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
            </a>
            <div class="modal-body">
                <p>Se recomienda ingresar los documentos de soporte en formato PDF</p>
                <form id="form-cotizacion" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="proyecto_id" name="proyecto_id">
                    <div>
                        <label>Nombre de la empresa</label>
                        <input id="input-nombreEmpresa" name="nombreEmpresa" type="text"
                               class="form-control">
                    </div>
                    <div>
                        <label>Cotizacion</label>
                        <input id="input-cotizacion_PDF" name="cotizacion_PDF" type="file"
                               class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*">
                    </div>
                    <div>
                        <label>Documento de la empresa</label>
                        <input id="input-documento_PDF" name="documento_PDF" type="file"
                               class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*">
                    </div>
                    <div>
                        <label>Camara de comercio</label>
                        <input id="input-camara_comercio_PDF" name="camara_comercio_PDF" type="file"
                               class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*">
                    </div>
                    <div>
                        <label>Rut de la empresa</label>
                        <input id="input-rut_PDF" name="rut_PDF" type="file"
                               class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*">
                    </div>
                    <div>
                        <label>Documento del representante <legal></legal></label>
                        <input id="input-representante_legal_PDF" name="representante_legal_PDF" type="file"
                               class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*">
                    </div>
                    <div class="col-sm-12 pt-3">
                        <div class="alert alert-danger text-danger" id="errorCotizacion" style="display: none">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardarCotizacion" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizarCotizacion" class="btn btn-primary">Actualizar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
