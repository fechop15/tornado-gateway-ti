<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary text-light">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nuevo</span>
                    <span class="fw-light">Usuario</span>
                </h5>
            </div>
            <a data-dismiss="modal" href="javascript:;">
                <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
            </a>
            <div class="modal-body">
                <form id="form-usuario" autocomplete="off">
                    @csrf
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombres
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control"
                                       id="nombres" name="nombres"
                                       autocomplete="nope"
                                       placeholder="Nombres" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Apellidos
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control"
                                       id="apellidos" name="apellidos"
                                       autocomplete="nope"
                                       placeholder="Apellidos" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Telefono
                                    <span class="required-label">*</span>
                                </label>
                                <input type="number" class="form-control"
                                       id="telefono" name="telefono"
                                       autocomplete="nope"
                                       required="" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Correo
                                    <span class="required-label">*</span>
                                </label>
                                <input type="email" class="form-control" id="correo" name="email"
                                       placeholder="Correo Electronico" required="" autocomplete="nope">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">Contraseña <span
                                        class="required-label">*</span></label>
                                <input type="password" class="form-control" id="password" name="password"
                                       required="" autocomplete="nope">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="select-tipo">Rol</label>
                                <select class="form-control" id="rol" name="rol">
                                    @foreach(\Spatie\Permission\Models\Role::all() as $rol)
                                        <option value="{{$rol->name}}">{{$rol->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12 pt-3">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" id="eliminar" class="btn btn-primary">Eliminar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
