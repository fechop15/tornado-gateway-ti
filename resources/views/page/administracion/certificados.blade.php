@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button class="btn btn-primary shadow-md mr-2">Informacion Certificado</button>
        </div>
    </div>
    <div class="pos intro-y p-5 mt-5">
        <div class="col-span-12 lg:col-span-12">
            <div class="intro-y pr-1">
                <div class="box p-2">
                    <div class="pos__tabs nav nav-tabs justify-center" role="tablist">
                        <a id="pendiente-tab" data-toggle="tab" data-target="#pendiente" href="javascript:;" class="flex-1 py-2 rounded-md text-center active" role="tab" aria-controls="pendientes" aria-selected="true">Pendientes</a>
                        <a id="persona-tab" data-toggle="tab" data-target="#persona" href="javascript:;" class="flex-1 py-2 rounded-md text-center" role="tab" aria-controls="personas" aria-selected="false">Personas</a>
                        <a id="empresa-tab" data-toggle="tab" data-target="#empresa" href="javascript:;" class="flex-1 py-2 rounded-md text-center" role="tab" aria-controls="empresas" aria-selected="false">Empresas</a>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="pendiente" class="tab-pane active" role="tabpanel" aria-labelledby="pendiente-tab">
                    <div class="box p-5 mt-5">
                        <table id="dt-certificados-pendiente" class="table table-bordered table-hover table-striped w-100" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Fecha Pago</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Tipo</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Fecha Pago</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="persona" class="tab-pane" role="tabpanel" aria-labelledby="persona-tab">
                    <div class="box p-5 mt-5">
                        <table id="dt-certificados-persona" class="table table-bordered table-hover table-striped w-100" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Fecha Certificado</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Fecha Certificado</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="empresa" class="tab-pane" role="tabpanel" aria-labelledby="empresa-tab">
                    <div class="box p-5 mt-5">
                        <table id="dt-certificados-empresa" class="table table-bordered table-hover table-striped w-100" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Fecha Certificado</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Fecha Certificado</th>
                                <th class="text-center">Estado</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            // initialize datatable
            let TABLA_PENDIENTES = $('#dt-certificados-pendiente').DataTable({
                responsive: true,
                ajax: {
                    url: "{{ url('recurso/certificados') }}",
                    "data": function (d) {
                        d.tipo = "Pendientes";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "dataSrc": function (data) {
                        let json = [];
                        console.log(data);
                        let nombre="";
                        let correo="";
                        let tipo="";
                        for (let item in data.certificados) {
                            if (data.certificados[item].persona_id!=null){
                                nombre=data.certificados[item].persona.user.nombres+' '+data.certificados[item].persona.user.apellidos;
                                correo=data.certificados[item].persona.correo_electro_personal;
                                tipo="Persona";
                            }else{
                                nombre=data.certificados[item].empresa.nombrecomercial;
                                correo=data.certificados[item].empresa.user.email;
                                tipo="Empresa";
                            }
                            let itemJson = {
                                id: data.certificados[item].id,
                                nombre: nombre,
                                correo: correo,
                                tipo: tipo,
                                estado: data.certificados[item].estado,
                                pago: data.certificados[item].created_at,
                                Opciones: opciones(data.certificados[item].estado)
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "correo"},
                    {data: "tipo"},
                    {data: "estado"},
                    {data: "pago"},
                    {data: "Opciones"},
                ],columnDefs: [
                    {
                        targets: 3,
                        render: function (data, type, full, meta) {
                            var badge = {
                                "1":
                                    {
                                        'title': 'Pago',
                                        'class': 'badge-success'
                                    },
                                "0":
                                    {
                                        'title': 'Pendiente',
                                        'class': 'badge-warning'
                                    }
                            };
                            if (typeof badge[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
                        },
                    },]
            });
            let TABLA_PERSONAS = $('#dt-certificados-persona').DataTable({
                responsive: true,
                columnDefs: [
                    {className: 'text-center', targets: [3, 4]},
                ],
                ajax: {
                    url: "{{ url('recurso/certificados') }}",
                    "data": function (d) {
                        d.tipo = "Personas";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "dataSrc": function (data) {
                        let json = [];
                        let btnDescarga;
                        console.log(data);
                        for (let item in data.certificados) {
                            btnDescarga = '<a  class="btn btn-primary mr-1 mb-2 descargar" target="_blank"' +
                                '           data-toggle="tooltip" data-placement="top" title="Descargar certificado" data-original-title="Edit"' +
                                '           style="margin-right: 5px;" href="/descargarCertificado/'+data.certificados[item].id+'">\n' +
                                '           <i class="fas fa-download"></i>\n' +
                                ' </a>';
                            let itemJson = {
                                id: data.certificados[item].id,
                                nombre: data.certificados[item].persona.user.nombres+' '+data.certificados[item].persona.user.apellidos,
                                correo: data.certificados[item].persona.correo_electro_personal,
                                fecha_certificado: data.certificados[item].fechaInicio+'-'+data.certificados[item].fechaFin,
                                estado: new Date(data.certificados[item].fechaFin)<new Date()?0:1,
                                Opciones: btnDescarga
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "correo"},
                    {data: "fecha_certificado"},
                    {data: "estado"},
                    {data: "Opciones"},
                ],columnDefs: [
                {
                    targets: 3,
                    render: function (data, type, full, meta) {
                        var badge = {
                            "1":
                                {
                                    'title': 'Activo',
                                    'class': 'badge-success'
                                },
                            "0":
                                {
                                    'title': 'Expirado',
                                    'class': 'badge-warning'
                                }
                        };
                        if (typeof badge[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
                    },
                },]
            });
            let TABLA_EMPRESAS = $('#dt-certificados-empresa').DataTable({
                responsive: true,
                columnDefs: [
                    {className: 'text-center', targets: [3, 4]},
                ],
                ajax: {
                    url: "{{ url('recurso/certificados') }}",
                    "data": function (d) {
                        d.tipo = "Empresas";
                        d._token = $('meta[name="csrf-token"]').attr('content');
                    },
                    "dataSrc": function (data) {
                        let json = [];
                        console.log(data);
                        let btnDescarga;
                        for (let item in data.certificados) {
                            btnDescarga = '<a  class="btn btn-primary mr-1 mb-2 descargar" target="_blank"' +
                                '           data-toggle="tooltip" data-placement="top" title="Descargar certificado" data-original-title="Edit"' +
                                '           style="margin-right: 5px;" href="/descargarCertificado/'+data.certificados[item].id+'">\n' +
                                '           <i class="fas fa-download"></i>\n' +
                                ' </a>';
                            let itemJson = {
                                id: data.certificados[item].id,
                                nombre: data.certificados[item].empresa.nombrecomercial,
                                correo: data.certificados[item].empresa.user.email,
                                fecha_certificado: data.certificados[item].fechaInicio+'-'+data.certificados[item].fechaFin,
                                estado: new Date(data.certificados[item].fechaFin)<new Date()?0:1,
                                Opciones: btnDescarga
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "nombre"},
                    {data: "correo"},
                    {data: "fecha_certificado"},
                    {data: "estado"},
                    {data: "Opciones"},
                ],columnDefs: [
                {
                    targets: 3,
                    render: function (data, type, full, meta) {
                        var badge = {
                            "1":
                                {
                                    'title': 'Activo',
                                    'class': 'badge-success'
                                },
                            "0":
                                {
                                    'title': 'Expirado',
                                    'class': 'badge-warning'
                                }
                        };
                        if (typeof badge[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
                    },
                },]
            });
            $("#pendiente-tab").on('click',function (){
                TABLA_PENDIENTES.ajax.reload()
            });
            $("#persona-tab").on('click',function (){
                TABLA_PERSONAS.ajax.reload()
            });
            $("#empresa-tab").on('click',function (){
                TABLA_EMPRESAS.ajax.reload(null,true)
            });
            function opciones() {
                var opciones = '';
                opciones += '<button  class="btn btn-primary mr-1 mb-2 ver" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Ver" data-original-title="Edit"' +
                    '           style="margin-right: 5px;">\n' +
                    '           <i class="fas fa-eye"></i>\n' +
                    ' </button>';
                //@ can('eliminar.usuarios')
                    opciones += '' +
                        '<button type="button" class="btn btn-success mr-1 mb-2 certificar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Certificar" data-original-title="Edit">' +
                        '           <i class="fas fa-check"></i>\n' +
                        ' </button>';
                //@ endcan
                return opciones;
            }

            TABLA_PENDIENTES.on('click', '.certificar', function () {
                $tr = $(this).closest('tr');
                let data = TABLA_PENDIENTES.row($tr).data();
                Swal.fire({
                    title: 'Estas seguro?',
                    text: "Vas a certificar a " + data.nombre,
                    icon: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonText: 'Si, certificar',
                    showLoaderOnConfirm: true,
                }).then((Delete) => {
                    if (Delete) {
                        Swal.fire({
                            title: 'Por favor espera !',
                            html: 'Validando certificacion..',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            onBeforeOpen: () => {
                                Swal.showLoading()
                            },
                        });
                        $.ajax({
                                url: '/recurso/certificar',
                                type: 'POST',
                                data: {
                                    id: data.id,
                                    _token: $('meta[name="csrf-token"]').attr('content')
                                },
                            }
                        ).done(function (response) {
                            console.log(response);
                            if (response.status != "success") {
                                Swal.fire("Oops", "Ha ocurrido un error al certificar, intente mas tarde", "error");
                            } else {
                                TABLA_PENDIENTES.ajax.reload();
                                Swal.fire({
                                    title: 'Ok!',
                                    text: response.message,
                                    icon: 'success',
                                    buttons: {
                                        confirm: {
                                            className: 'btn btn-success'
                                        }
                                    }
                                });
                            }
                            //return response;
                        }).fail(function (error) {
                            console.log(error);
                        });
                    } else {
                        swal.close();
                    }
                });
            });

        });
    </script>

@endsection
