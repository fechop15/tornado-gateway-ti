
@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button id="modalUsuario" class="btn btn-primary shadow-md mr-2">Agregar nuevo usuario</button>
        </div>
    </div>
    <div class="intro-y box p-5 mt-5">
        <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Rol</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Opciones</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Rol</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Opciones</th>
            </tr>
            </tfoot>
        </table>

    </div>

    @include('page.modal.user')

@endsection

@section('script')

    <!--suppress JSCheckFunctionSignatures -->
    <script type="text/javascript">

        $(document).ajaxStart(function () {
            $('.panel-container').addClass('enable-loader');

        });
        $(document).ajaxComplete(function () {
            $('.panel-container').removeClass('enable-loader');
        });

        cash(function () {

            $(document).ready(function () {

                // initialize datatable

                let TABLA = $('#dt-basic-example').DataTable({
                    responsive: true,
                    ajax: {
                        url: "{{ url('recurso/users') }}",
                        "dataSrc": function (data) {
                            let json = [];
                            console.log(data);
                            for (let item in data.msg) {
                                let itemJson = {
                                    id: data.msg[item].id,
                                    nombre: data.msg[item].nombres + ' ' + data.msg[item].apellidos,
                                    nombres: data.msg[item].nombres,
                                    apellidos: data.msg[item].apellidos,
                                    telefono: data.msg[item].telefono,
                                    correo: data.msg[item].email,
                                    rol: data.msg[item].rol,
                                    estado: data.msg[item].active,
                                    Opciones: opciones(data.msg[item].active)
                                };
                                json.push(itemJson)
                            }
                            return json;
                        }
                    },
                    columns: [
                        {data: "nombre"},
                        {data: "correo"},
                        {data: "rol[0].name"},
                        {data: "estado"},
                        {data: "Opciones"},
                    ],columnDefs: [
                    {
                        targets: 3,
                        render: function (data, type, full, meta) {
                            var badge = {
                                "1":
                                    {
                                        'title': 'Activo',
                                        'class': 'badge-success'
                                    },
                                "0":
                                    {
                                        'title': 'Inactivo',
                                        'class': 'badge-warning'
                                    }
                            };
                            if (typeof badge[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
                        },
                    },]
                });

                function opciones(estado) {
                    var opciones = '';
                    //@ can('editar.usuarios')
                    opciones += '<button  class="btn btn-primary mr-1 mb-2 editar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                        '           style="margin-right: 5px;">\n' +
                        '           <i class="fas fa-pen"></i>\n' +
                        ' </button>';
                    //@ endcan

                    //@ can('eliminar.usuarios')

                    if (estado == 1) {
                        opciones += '' +
                            '<button type="button" class="btn btn-danger mr-1 mb-2 cambiarEstado" ' +
                            '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                            '           <i class="fas fa-trash"></i>\n' +
                            ' </button>';
                    } else {
                        opciones += '' +
                            '<button type="button" class="btn btn-success mr-1 mb-2 cambiarEstado" ' +
                            '           data-toggle="tooltip" data-placement="top" title="Habilitar" data-original-title="Edit">' +
                            '           <i class="fas fa-check"></i>\n' +
                            ' </button>';
                    }
                    //@ endcan
                    return opciones;
                }

                // @ can('crear.usuarios')
                $("#modalUsuario").on('click', function () {

                    $('#modal').modal('show');
                    $('#form-usuario')[0].reset();
                    $('#actualizar').hide();
                    $('#guardar').show();
                    $('#eliminar').hide();
                    $("#error").hide();
                    $("#accion-modal").html('Registrar');
                    $("#password").prop('disabled', false);

                });
                $("#guardar").on('click', function () {
                    $("#error").hide();
                    $('#modal .modal-content').addClass("is-loading");
                    $("#guardar").prop('disabled', true);
                    $.ajax({
                        url: '{{url('recurso/users')}}',
                        type: 'POST',
                        data: $("#form-usuario").serialize(),
                    }).done(function (response) {
                        $('#form-usuario')[0].reset();
                        cash('#modal').modal('hide');
                        $("#guardar").prop('disabled', false);
                        TABLA.ajax.reload();

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Ok!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })


                        //return response;
                    }).fail(function (error) {

                        console.log(error);
                        var obj = error.responseJSON.errors;
                        $.each(obj, function (key, value) {
                            $("#error").html(value[0]);
                            $("#error").show();
                        });


                    }).always(function () {
                        $('#modal .modal-content').removeClass("is-loading");
                        $("#guardar").prop('disabled', false);
                    });

                });
                // @ endcan
                // @ can('editar.usuarios')
                TABLA.on('click', '.editar', function () {
                    $tr = $(this).closest('tr');
                    var data = TABLA.row($tr).data();
                    cash("#modal").modal('show');
                    $('#form-usuario')[0].reset();
                    $('#actualizar').show();
                    $('#eliminar').hide();
                    $('#guardar').hide();
                    $("#accion-modal").html('Editar');
                    $("#id").val(data.id);
                    $("#nombres").val(data.nombres);
                    $("#apellidos").val(data.apellidos);
                    $("#correo").val(data.correo);
                    $("#telefono").val(data.telefono);
                    $("#rol").val(data.rol[0].name);
                    $("#password").prop('disabled', true);

                });
                $("#actualizar").on('click', function () {
                    $("#error").hide();
                    $('#modal .modal-content').addClass("is-loading");
                    $("#actualizar").prop('disabled', true);
                    $.ajax({
                        url: '{{url('recurso/users')}}/' + $('#id').val(),
                        type: 'PUT',
                        data: $("#form-usuario").serialize(),
                    }).done(function (response) {
                        $('#form-usuario')[0].reset();
                        cash('#modal').modal('hide');
                        TABLA.ajax.reload();

                        Swal.fire({
                            icon: 'success',
                            title: 'Ok!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })

                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                        var obj = error.responseJSON.errors;
                        $.each(obj, function (key, value) {
                            $("#error").html(value[0]);
                            $("#error").show();
                        });


                    }).always(function () {
                        $('#modal .modal-content').removeClass("is-loading");
                        $("#actualizar").prop('disabled', false);
                    });

                });
                // @ endcan
                // @ can('eliminar.usuarios')
                TABLA.on('click', '.cambiarEstado', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA.row($tr).data();
                    let opcion = "";

                    data.estado == 1 ? opcion = "Deshabilitar" : opcion = "Habilitar";

                    Swal.fire({
                        title: 'Estas seguro?',
                        text: "Vas a " + opcion + " al usuario: " + data.nombre,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Si, ' + opcion,
                    }).then((Delete) => {
                        if (Delete) {
                            $.ajax({
                                    url: '/recurso/verificarUsuario',
                                    type: 'POST',
                                    data: {
                                        id: data.id,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    },
                                }
                            ).done(function (response) {
                                console.log(response);
                                if (response.status == "Error") {
                                    //error
                                } else {
                                    let msg = "";
                                    response.msg.estado ? msg = "habilitada" : msg = "deshabilitada";
                                    TABLA.ajax.reload();
                                    swal({
                                        title: 'Ok!',
                                        text: 'El usuario ha sido ' + msg,
                                        icon: 'success',
                                        buttons: {
                                            confirm: {
                                                className: 'btn btn-success'
                                            }
                                        }
                                    });
                                }
                                //return response;
                            }).fail(function (error) {
                                console.log(error);
                            });
                        } else {
                            swal.close();
                        }
                    });
                });
                // @ endcan

            });
        });
        function importar() {
            Swal.fire({
                title: 'Selecciona un archivo a cargar',
                html: 'Descaga aqui la <a href="/template/plantilla.xlsx">plantilla de carga</a>',
                showCancelButton: true,
                confirmButtonText: 'Cargar',
                input: 'file',
                inputAttributes: {
                    accept: ".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
                },
                onBeforeOpen: () => {
                    $(".swal2-file").change(function () {
                        var reader = new FileReader();
                        reader.readAsDataURL(this.files[0]);
                    });
                }
            }).then((file) => {
                if (file.value) {
                    let formData = new FormData();
                    let file = $('.swal2-file')[0].files[0];
                    formData.append("exel", file);
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        method: 'post',
                        url: "{{ url('recurso/import') }}",
                        data: formData,
                        contentType: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        success: function (resp) {
                            console.log(resp);
                            $("#cargarXLS").show();
                            $("#fuente-carga").val("");
                            $('#mensaje-carga').summernote('reset');
                            TABLACARGA.clear().draw();
                            for (const prop in resp.msg) {
                                console.log(resp.msg[prop]);
                                let error = 1;
                                if ((resp.msg[prop].correo == null || resp.msg[prop].correo === "") && (resp.msg[prop].telefono == null || resp.msg[prop].telefono === "")) {
                                    error = 0;
                                }
                                let rowNode = TABLACARGA.row.add([
                                    prop,
                                    resp.msg[prop].nombre,
                                    resp.msg[prop].profesion,
                                    resp.msg[prop].correo,
                                    resp.msg[prop].telefono,
                                    error,
                                ]).draw().node();
                                //$(rowNode).find('tr').addClass(error);

                            }
                            $("#verAtenciones").hide();
                            //Swal.fire('Uploaded', 'Your file have been uploaded', 'success');
                        },
                        error: function () {
                            Swal.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: 'Error al cargar el archivo!'
                            })
                        }
                    })
                }
            });
        }

    </script>

@endsection
