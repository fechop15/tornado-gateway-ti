@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button id="registrarPermiso" class="btn btn-primary shadow-md mr-2">Agregar Permiso</button>
        </div>
    </div>
    <div class="intro-y box p-5 mt-5">
        <table id="tablaEmpleados" class="table table-bordered table-hover table-striped w-100">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th class="text-right">Opciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th class="text-right">Opciones</th>
            </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>
    </div>

    @include('page.modal.permisos')

@endsection

@section('script')
    <script type="text/javascript">

        $(document).ajaxStart(function () {
            $('.panel-container').addClass('enable-loader');

        });
        $(document).ajaxComplete(function () {
            $('.panel-container').removeClass('enable-loader');
        });
        cash(function () {
            $(document).ready(function () {

                var TABLA = $('#tablaEmpleados').DataTable({
                    "ajax": {
                        "url": "{{ url('recurso/permissions') }}",
                        "type": "GET",
                        "dataSrc": function (data) {
                            var json = [];
                            console.log(data);
                            for (var item in data.msg) {
                                var itemJson = {
                                    Id: data.msg[item].id,
                                    Nombre: data.msg[item].name,
                                    Descripcion: data.msg[item].description,
                                    Tipo: data.msg[item].guard_name,
                                    Creado: data.msg[item].created_at,
                                    Actualizado: data.msg[item].updated_at,
                                    Opciones: opciones(data.msg[item].id)
                                };
                                json.push(itemJson)
                            }
                            return json;
                        }
                    },
                    columns: [
                        {data: "Nombre"},
                        {data: "Tipo"},
                        {data: "Creado"},
                        {data: "Actualizado"},
                        {data: "Opciones"},
                    ],
                });

                function opciones(id) {
                    var opciones = '';

                    opciones += '' +
                        '<button type="button" class="btn btn-primary btn-xs editar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                        '           style="margin-right: 5px;">\n' +
                        '           <i class="fas fa-pen"></i>\n' +
                        ' </button>';

                    opciones += '' +
                        '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                        '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                        '           <i class="fas fa-trash"></i>\n' +
                        ' </button>';

                    return opciones;
                }

                TABLA.on('click', '.editar', function () {
                    $tr = $(this).closest('tr');
                    var data = TABLA.row($tr).data();
                    Swal.fire({
                        title: 'Nombre del permiso',
                        input: 'text',
                        inputValue: data.Nombre,
                        inputAttributes: {
                            autocapitalize: 'off',
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Actualizar',
                        showLoaderOnConfirm: true,
                        preConfirm: (login) => {
                            return $.ajax({
                                url: "{{ url('recurso/permissions/') }}/" + data.Id,
                                type: 'PUT',
                                data: {
                                    name: login,
                                    _token: $('meta[name="csrf-token"]').attr('content')
                                },
                            }).done(function (response) {

                                TABLA.ajax.reload();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Ok!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }).fail(function (error) {
                                console.log(error)
                                Swal.showValidationMessage(
                                    `error: ${error.errors.message}`
                                );
                            })
                            // return fetch(`//api.github.com/users/${login}`)
                            //     .then(response => {
                            //         if (!response.ok) {
                            //             throw new Error(response.statusText)
                            //         }
                            //         return response.json()
                            //     })
                            //     .catch(error => {
                            //         Swal.showValidationMessage(
                            //             `Request failed: ${error}`
                            //         )
                            //     })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {

                        }
                    });
                });

                TABLA.on('click', '.eliminar', function () {
                    $tr = $(this).closest('tr');
                    var data = TABLA.row($tr).data();
                    Swal.fire({
                        title: 'Estas seguro?',
                        text: "Vas a eliminar el permiso " + data.Nombre,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Si, eliminar',
                        showLoaderOnConfirm: true,
                    }).then((Delete) => {
                        if (Delete) {
                            Swal.fire({
                                title: 'Por favor espera !',
                                html: 'Validando operacion..',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                },
                            });
                            $.ajax({
                                    url: "{{ url('recurso/permissions/') }}/" + data.Id,
                                    type: 'DELETE',
                                    data: {
                                        id: data.Id,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    },
                                }
                            ).done(function (response) {
                                    TABLA.ajax.reload();
                                    Swal.fire({
                                        title: 'Ok!',
                                        text: response.message,
                                        icon: 'success',
                                        buttons: {
                                            confirm: {
                                                className: 'btn btn-success'
                                            }
                                        }
                                    });
                                //return response;
                            }).fail(function (error) {
                                console.log(error);
                            });
                        } else {
                            swal.close();
                        }
                    });
                });

                $("#registrarPermiso").on('click', function () {
                    Swal.fire({
                        title: 'Nombre del permiso',
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Guardar',
                        showLoaderOnConfirm: true,
                        preConfirm: (login) => {
                            return $.ajax({
                                url: "{{ url('recurso/permissions') }}",
                                type: 'POST',
                                data: {
                                    name: login,
                                    _token: $('meta[name="csrf-token"]').attr('content')
                                },
                            }).done(function (response) {

                                TABLA.ajax.reload();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Ok!',
                                    text: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }).fail(function (error) {
                                console.log(error)
                                Swal.showValidationMessage(
                                    `error: ${error.errors.message}`
                                );
                            })
                            // return fetch(`//api.github.com/users/${login}`)
                            //     .then(response => {
                            //         if (!response.ok) {
                            //             throw new Error(response.statusText)
                            //         }
                            //         return response.json()
                            //     })
                            //     .catch(error => {
                            //         Swal.showValidationMessage(
                            //             `Request failed: ${error}`
                            //         )
                            //     })
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {

                        }
                    });
                });
            });
        })
    </script>
@endsection
