@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            {{--            @ can('crear.roles')--}}
            <button class="btn btn-primary shadow-md mr-2" id="btnPermisos">Agregar nuevo rol</button>
            <button class="btn btn-danger shadow-md mr-2 hidden" id="btnBack">Regresar</button>
            {{--            @ endcan--}}
        </div>
    </div>
    <div id="roles-container" class="intro-y box p-5 mt-5">
        <table id="tablaRoles" class="table table-bordered table-hover table-striped w-100">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th class="text-right">Opciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Creado</th>
                <th>Actualizado</th>
                <th class="text-right">Opciones</th>
            </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>
    </div>

    <div id="permisos-container" class="hidden intro-y box p-5 mt-5">
        <form id="form-rol" autocomplete="off">
            @csrf
            <input type="hidden" id="id">

            <div class="form-group">
                <label for="name" class="text-right">
                    Nombre
                    <span class="required-label">*</span>
                </label>
                <input type="text" class="form-control"
                       id="nombre" name="name"
                       autocomplete="nope"
                       placeholder="Nombre del Rol" required="">
            </div>

            <div class="col-sm-12 pt-3">
                <div class="alert alert-danger text-danger" id="error"
                     style="display: none">
                </div>
            </div>

            <label for="permisos" class="text-right">
                Permisos
                <span class="required-label">*</span>
            </label>
            <hr>
            <!-- datatable start -->
            <div class="table-responsive">
                <table id="tablaPermisos"
                       class="table table-bordered table-hover table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th class="text-right">Opciones</th>
                    </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
                <!-- datatable end -->
            </div>

        </form>
        <div class="text-center">
            <button type="button" id="guardar-permiso" class="btn btn-primary">Guardar</button>
            <button type="button" id="actualizar-permiso" class="btn btn-primary">Actualizar
            </button>
            <button type="button" class="btn btn-danger btnBack" data-dismiss="modal">
                Cancelar
            </button>
        </div>
    </div>

    {{--    @ include('pages.modal.alert')--}}


@endsection

@section('script')

    <!--suppress JSCheckFunctionSignatures -->
    <script type="text/javascript">

        $(document).ajaxStart(function () {
            $('.panel-container').addClass('enable-loader');

        });
        $(document).ajaxComplete(function () {
            $('.panel-container').removeClass('enable-loader');
        });

        $(document).ready(function () {

            // initialize datatable

            let TABLA = $('#tablaRoles').DataTable({
                responsive: true,
                columnDefs: [
                    {className: 'text-center', targets: [4]},
                ],
                "ajax": {
                    "url": "{{ url('recurso/roles') }}",
                    "type": "GET",
                    "dataSrc": function (data) {
                        let json = [];
                        console.log(data);
                        for (let item in data.msg) {
                            let itemJson = {
                                Id: data.msg[item].id,
                                Nombre: data.msg[item].name,
                                Tipo: data.msg[item].guard_name,
                                Creado: data.msg[item].created_at,
                                Actualizado: data.msg[item].updated_at,
                                Permisos: data.msg[item].permisos,
                                Opciones: opciones()
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Nombre"},
                    {data: "Tipo"},
                    {data: "Creado"},
                    {data: "Actualizado"},
                    {data: "Opciones"},
                ],
            });

            let TABLA_PERMISOS = $('#tablaPermisos').DataTable({
                fixedHeader: true,
                responsive: true,
                "paging": false,
                columnDefs: [
                    {className: 'text-center', targets: [2]},
                ],


                "ajax": {
                    "url": "{{ url('recurso/permissions') }}",
                    "type": "GET",
                    "dataSrc": function (data) {
                        let json = [];
                        console.log(data);
                        for (let item in data.msg) {
                            let itemJson = {
                                Id: data.msg[item].id,
                                Nombre: data.msg[item].name,
                                Guard_Name: data.msg[item].guard_name,
                                Opcion: opcion(data.msg[item].id),
                            };
                            json.push(itemJson)
                        }
                        return json;
                    }
                },
                columns: [
                    {data: "Nombre"},
                    {data: "Guard_Name"},
                    {data: "Opcion"},
                ],
            });


            function opciones() {
                let opciones = '';
                //@ can('editar.roles')
                    opciones = '<button type="button" class="btn btn-primary btn-xs editar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                    '           style="margin-right: 5px;">' +
                    '           <i class="fas fa-pen"></i>' +
                    ' </button>';
                //@ endcan
                //    @c an('eliminar.roles')
                    opciones += '' +
                    '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                    '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                    '           <i class="fas fa-trash"></i>' +
                    ' </button>';
                //@ endcan
                    return opciones;
            }

            function opcion(id) {
                let opciones = '';
                //@ can('editar.roles')
                    opciones = '' +
                    '<div class="custom-control custom-switch">' +
                    '   <input type="checkbox" name="permissions[]" class="custom-control-input" id="permiso' + id + '" value="' + id + '">' +
                    '   <label class="custom-control-label" for="permiso' + id + '"></label>' +
                    '</div>';
                //@ endcan
                    return opciones;
            }

            @can('eliminar.roles')
            TABLA.on('click', '.eliminar', function () {

                $tr = $(this).closest('tr');
                let data = TABLA.row($tr).data();
                $('#form-rol')[0].reset();
                $("#id").val(data.Id);

                Swal.fire({
                    title: 'Estas Seguro?',
                    text: "Eliminaras el Rol: " + data.name,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Eliminar',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.value) {


                        $.ajax({
                            url: '{{url('recurso/roles')}}/' + $('#id').val(),
                            type: 'DELETE',
                            data: $("#form-rol").serialize(),

                        }).done(function (response) {
                            $('#form-rol')[0].reset();
                            $('#modal').modal('hide');
                            TABLA.ajax.reload();

                            $('#btnBack').click();

                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Ok!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            });

                            //return response;
                        }).fail(function (error) {
                            console.log(error);
                            var obj = error.responseJSON.errors;
                            $.each(obj, function (key, value) {
                                $("#error").html(value[0]);
                                $("#error").show();
                            });


                        }).always(function () {
                            $('#modal .modal-content').removeClass("is-loading");
                            $("#actualizar").prop('disabled', false);
                        });


                    }


                })


            });
            @endcan

            //@ can('crear.roles')

            $("#btnPermisos").on('click', function () {
                $('#permisos-container').show();
                $('#roles-container').hide();
                $('#btnBack').show();
                $('#btnPermisos').hide();

                $('#form-rol')[0].reset();

                //limpia el input de busqueda de la datatable
                $("#tablaPermisos").DataTable().search("").draw();
                $("#error").hide();
                $('#guardar-permiso').show();
                $('#actualizar-permiso').hide();
                $('#eliminar-permiso').hide();

            });

            $("#btnBack, .btnBack").on('click', function () {
                $('#btnBack').hide();
                $('#btnPermisos').show();
                $('#permisos-container').hide();
                $("input[type=search]").val("");
                $('#roles-container').show();
            });

            $("#guardar-permiso").on('click', function () {
                $("#error").hide();
                $.ajax({
                    url: '{{url('recurso/roles')}}',
                    type: 'POST',
                    data: $("#form-rol").serialize(),

                }).done(function (response) {
                    console.log(response);
                    $('#form-rol')[0].reset();
                    TABLA.ajax.reload();

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Ok!',
                        text: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    $(".btnBack").click();


                    //return response;
                }).fail(function (error) {
                    console.log(error);
                    var obj = error.responseJSON.errors;
                    $.each(obj, function (key, value) {
                        $("#error").html(value[0]);
                        $("#error").show();
                    });


                }).always(function () {
                    $("#guardar").prop('disabled', false);
                });

            });

            //@ endcan

            //@ can('editar.roles')
            TABLA.on('click', '.editar', function () {
                $tr = $(this).closest('tr');
                let data = TABLA.row($tr).data();
                $('#form-rol')[0].reset();
                $("#tablaPermisos").DataTable().search("").draw();
                $('#actualizar-permiso').show();
                $('#guardar-permiso').hide();

                $('#permisos-container').show();
                $('#roles-container').hide();

                $("#id").val(data.Id);
                $("#nombre").val(data.Nombre);
                console.log(data.Permisos);
                //señalar permisos
                for (let item in data.Permisos) {
                    $("#permiso" + data.Permisos[item].id).prop('checked', true);
                }
            });
            $("#actualizar-permiso").on('click', function () {
                $("#error").hide();
                $("#actualizar").prop('disabled', true);
                Swal.fire({
                    title: 'Por favor espera !',
                    html: 'Validando operacion..',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                });
                $.ajax({
                    url: '{{url('recurso/roles')}}/' + $('#id').val(),
                    type: 'PUT',
                    data: $("#form-rol").serialize(),
                }).done(function (response) {
                    $('#form-rol')[0].reset();
                    TABLA.ajax.reload();
                    $('#btnBack').click();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Ok!',
                        text: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    //return response;
                }).fail(function (error) {
                    console.log(error);
                    var obj = error.responseJSON.errors;
                    $.each(obj, function (key, value) {
                        Swal.fire({
                            title: 'Error!',
                            text: value[0],
                            icon: 'danger',
                            buttons: {
                                confirm: {
                                    className: 'btn btn-success'
                                }
                            }
                        });
                    });


                }).always(function () {
                    $('#modal .modal-content').removeClass("is-loading");
                    $("#actualizar").prop('disabled', false);
                });

            });
            //@ endcan



        });


    </script>

@endsection
