@extends('../layout/' . $layout)

@section('subhead')
    <title>Actualizacion de Perfil - GATEWAYTI</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Perfil Persona</h2>
    </div>
{{--    @if(auth()->user()->persona->pagos->last() && auth()->user()->persona->pagos->last()->estado==1)--}}
{{--        <div class="intro-y" style="z-index: auto;">--}}
{{--            @if(auth()->user()->persona->pagos->last()->fechaFin && auth()->user()->persona->pagos->last()->fechaFin > \Carbon\Carbon::now())--}}
{{--                <div id="perfil-completado" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
{{--                    <div class="flex items-center">--}}
{{--                        <div class="font-medium text-lg">Perfil certificado</div>--}}
{{--                        <div class="text-xs px-1 text-gray-800 ml-auto">--}}
{{--                            <a target="_blank"  href="/descargarCertificado/{{auth()->user()->persona->pagos->last()->id}}" class="btn btn-dark w-32 mr-2 mb-2"><i--}}
{{--                                    data-feather="award" class="w-4 h-4 mr-2"></i> Descargar--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="mt-3">--}}
{{--                        Tu perfil ha sido verificado, ya puedes gozar de los pribilegios de CREINTEC--}}
{{--                        por {{\Carbon\Carbon::now()->diffInDays(auth()->user()->persona->pagos->last()->fechaFin)}}dias.--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @elseif(!auth()->user()->persona->pagos->last()->fechaFin)--}}
{{--                <div id="perfil-completado" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
{{--                    <div class="flex items-center">--}}
{{--                        <div class="font-medium text-lg">Pago completado</div>--}}
{{--                        <div class="text-xs px-1 text-gray-800 ml-auto">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="mt-3">--}}
{{--                        Tu pago se ha confirmado en CREINTEC, estamos verificando tu perfil y generando tu certificado.--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @else--}}
{{--                <div id="perfil-completado" style="display: none"--}}
{{--                     class="rounded-md px-5 py-4 mb-2 bg-theme-6 text-white">--}}
{{--                    <div class="flex items-center">--}}
{{--                        <div class="font-medium text-lg">Menbrecia expirada</div>--}}
{{--                        <div class="text-xs px-1 text-gray-800 ml-auto">--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="mt-3">--}}
{{--                        Su certificacion ha expirado--}}
{{--                        hace {{\Carbon\Carbon::now()->diffInDays(auth()->user()->empresa->pagos->last()->fechaFin)}}--}}
{{--                        dias, actualize la informacion del perfil y renueve su membrecia a CREINTEC para gozar de los--}}
{{--                        servicos.--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    @else--}}
{{--        <div class="intro-y">--}}
{{--            <div id="perfil-completado" style="display: none" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
{{--                <div class="flex items-center">--}}
{{--                    <div class="font-medium text-lg">Perfil completado</div>--}}
{{--                    <div class="text-xs px-1 text-gray-800 ml-auto">--}}
{{--                        <button onclick="pagarWompi()" class="btn btn-dark w-32 mr-2 mb-2"><i data-feather="dollar-sign"--}}
{{--                                                                                              class="w-4 h-4 mr-2"></i>--}}
{{--                            Pagar--}}
{{--                        </button>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="mt-3">--}}
{{--                    Por favor complete todo el formulario y realice el pago para recibir su certificado de asociado CREINNTEC.--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div id="perfil-por-completar" style="display: none"--}}
{{--                 class="rounded-md px-5 py-4 mb-2 bg-theme-12 text-white">--}}
{{--                <div class="flex items-center">--}}
{{--                    <div class="font-medium text-lg">Perfil por completar</div>--}}
{{--                    <div class="text-xs bg-white px-1 rounded-md text-gray-800 ml-auto">Alerta</div>--}}
{{--                </div>--}}
{{--                <div class="mt-3">Por favor completa tu perfil para obtener todos nuestros beneficios.--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="Tornado Persona" class="rounded-full"
                             src="{{auth()->user()->photo?asset(auth()->user()->photo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div
                            class="font-medium text-base">{{ auth()->user()->nombres." ".auth()->user()->apellidos  }}</div>
                        <div class="text-gray-600">{{ auth()->user()->tipo_usuario->descripcion}}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <ul>
                        <li class="flex items-center text-theme-1 dark:text-theme-10 font-medium link-tab cursor-pointer"
                            aria-expanded="datos_personales">
                            <i data-feather="info" class="w-4 h-4 mr-2"></i> Datos personales
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="informacion_familiar">
                            <i data-feather="user" class="w-4 h-4 mr-2"></i> Información familiar
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="estudios_formales">
                            <i data-feather="bar-chart" class="w-4 h-4 mr-2"></i> Estudios formales
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="seguridad_social">
                            <i data-feather="globe" class="w-4 h-4 mr-2"></i> Información seguridad social
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="educacion_no_formal">
                            <i data-feather="eye" class="w-4 h-4 mr-2"></i> Educación no formal
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="certificaciones">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Idiomas
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="experiencia_laboral">
                            <i data-feather="eye" class="w-4 h-4 mr-2"></i> Experiencia laboral
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="habilidades_ti">
                            <i data-feather="home" class="w-4 h-4 mr-2"></i> Habilidades
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
            <div id="datos_personales" class="intro-y box lg:mt-5 card-fom-empresa ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Datos personales</h2>
                </div>
                <div class="p-5">
                    <form id="datos-personales-form">
                        <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                <img class="rounded-md" id="image_logo"
                                     alt="Midone Tailwind HTML Admin Template"
                                     src="{{auth()->user()->photo?asset(auth()->user()->photo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                                <div title="Remove this profile photo?" id="remove_logo"
                                     class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                    <i data-feather="x" class="w-4 h-4"></i>
                                </div>
                            </div>
                            <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                <button type="button" class="btn btn-primary w-full">Cargar Imagen
                                </button>
                                <input type="file" name="logo_rutararchivo" id="logo_rutararchivo" accept="image/*"
                                       class="w-full h-full top-0 left-0 absolute opacity-0">
                            </div>
                            <div
                                class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden mt-2"
                                id="logoEmpresa">
                                <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga el
                                logo de la empresa
                            </div>
                        </div>

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Nombres</label>
                                <input type="text" id="input-nombres" name="nombres"
                                       value="{{auth()->user()->nombres}}"
                                       class="form-control">
                                <div id="error-nombres" class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Apellidos</label>
                                <input type="text" id="input-apellidos" name="apellidos"
                                       class="form-control"
                                       value="{{auth()->user()->apellidos}}">
                                <div id="error-apellidos" class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Documento de indentidad</label>
                                <input type="file" id="input-identificacion_rurataarchivo"
                                       name="identificacion_rurataarchivo"
                                       class="form-control" accept="application/pdf">
                                <a href="javascript:;" id="verDocumento" class="btn btn-primary" style="display: none;">Ver
                                    Documento</a>

                                <div id="error-identificacion_rurataarchivo"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden mt-2"
                                    id="idPersona">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga tu identificacion
                                </div>

                            </div>

                            <div class="">
                                <label>Tipo Identificacion</label>
                                <select class="form-control" name="tipoid"
                                        id="input-tipoid">
                                    @foreach(\App\Models\Tipo_identificacion::all() as $tipo_identificacion)
                                        <option
                                            {{auth()->user()->persona->tipoid==$tipo_identificacion->id?'selected':''}} value="{{$tipo_identificacion->id}}">{{$tipo_identificacion->descripcion}}</option>
                                    @endforeach
                                </select>
                                <div id="error-tipoid"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Genero</label>
                                <div class="mt-2">
                                    <select data-search="false" class="tail-select w-full" name="genero_id"
                                            id="input-genero_id">
                                        @foreach(\App\Models\Genero::all() as $data)
                                            <option
                                                {{auth()->user()->persona->genero_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-genero_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>

                            <div class="">
                                <label>Numero de documento de identidad</label>
                                <input type="text" id="input-identificacion" name="identificacion"
                                       class="form-control"
                                       value="{{auth()->user()->persona->identificacion}}">
                                <div id="error-identificacion"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Pais</label>
                                <div class="mt-2">
                                    <select data-search="true" class="w-full" id="select-pais">
                                        @foreach(\App\Models\Countries::all() as $pais)
                                            <option value="{{$pais->id}}">{{$pais->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <label>Fecha de nacimiento</label>
                                <input type="date" id="input-fecha_nacimiento" name="fecha_nacimiento"
                                       class="form-control"
                                       value="{{auth()->user()->persona->fecha_nacimiento}}">
                                <div id="error-fecha_nacimiento"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Departamento</label>
                                <div class="mt-2">
                                    <select data-search="true" class="w-full" id="select-departamento">
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <label>Estado Civil</label>
                                <div class="mt-2">
                                    <select data-search="false" class="tail-select w-full" name="estado_civil_id"
                                            id="input-estado_civil_id">
                                        @foreach(\App\Models\EstadoCivil::all() as $data)
                                            <option
                                                {{auth()->user()->persona->estado_civil_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-estado_civil_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>

                            <div class="">
                                <label>Ciudad</label>
                                <div class="mt-2">
                                    <select data-search="true" class="w-full" id="ciudad" name="ciudad">
                                    </select>
                                </div>
                                <div id="error-ciudad" class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>
                            <div class="">
                                <label>Celular</label>
                                <input type="text" id="input-celular" name="celular"
                                       class="form-control"
                                       value="{{auth()->user()->telefono}}">
                                <div id="error-celular" class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>


                            <div class="">
                                <label>RH sanguineo</label>
                                <div class="mt-2">
                                    <select data-search="false" class="tail-select w-full" name="rh_sanguineo_id"
                                            id="input-rh_sanguineo_id">
                                        @foreach(\App\Models\RhSanguineo::all() as $data)
                                            <option
                                                {{auth()->user()->persona->rh_sanguineo_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-rh_sanguineo_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>
                            <div class="">
                                <label>Link Linkedin</label>
                                <input type="text" id="input-link_linkedin" name="link_linkedin"
                                       class="form-control"
                                       value="{{auth()->user()->persona->link_linkedin}}">
                                <div id="error-link_linkedin"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Direccion residencia</label>
                                <input type="text" id="input-direccion_recidencia" name="direccion_recidencia"
                                       class="form-control"
                                       value="{{auth()->user()->persona->direccion_recidencia}}">
                                <div id="error-direccion_recidencia"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>
                            <div class="">
                                <label>Link Facebook</label>
                                <input type="text" id="input-link_facebook" name="link_facebook"
                                       class="form-control"
                                       value="{{auth()->user()->persona->link_facebook}}">
                                <div id="error-link_facebook"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Link instagram</label>
                                <input type="text" id="input-link_instagram" name="link_instagram"
                                       class="form-control"
                                       value="{{auth()->user()->persona->link_instagram}}">
                                <div id="error-link_instagram"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Link twitter</label>
                                <input type="text" id="input-link_twitter" name="link_twitter"
                                       class="form-control"
                                       value="{{auth()->user()->persona->link_twitter}}">
                                <div id="error-link_twitter"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>


                        </div>

                        <div class="flex justify-start mt-4">
                            <button type="button" id="btn-datos-personales"
                                    class="btn btn-primary w-20 mt-3">Guardar
                            </button>
                        </div>

                    </form>
                </div>
            </div>
            <div id="informacion_familiar" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Información familiar</h2>
                </div>
                <div class="p-5">
                    <form id="informacion_familiar-form">
                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">
                            <div>
                                <label>Numero de personas a cargo</label>
                                <input type="number" class="form-control"
                                       name="num_personas_dependientes"
                                       id="input-num_personas_dependientes"
                                       value="{{auth()->user()->persona->num_personas_dependientes}}">
                            </div>
                            <div class="">
                                <label>Numero de contacto en caso de emergencia</label>
                                <input type="text" class="form-control"
                                       name="numero_contacto_emergencias"
                                       id="input-numero_contacto_emergencias"
                                       value="{{auth()->user()->persona->numero_contacto_emergencias}}">
                            </div>
                            <div class="">
                                <label>Nombre de contacto en caso de emergencia</label>
                                <input type="text" class="form-control" placeholder=""
                                       name="nombre_contacto_emergencias"
                                       id="input-nombre_contacto_emergencias"
                                       value="{{auth()->user()->persona->nombre_contacto_emergencias}}">
                            </div>
                        </div>

                        <div class="flex justify-start mt-4">
                            <button id="btn-informacion_familiar" type="button"
                                    class="btn btn-primary w-20 mt-3">Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="estudios_formales" class="intro-y lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Estudios formales</h2>
                        <button class="btn btn-primary mr-1 mb-2" onclick="agregarEstudioFormal()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerEstudiosFormales">
                    @foreach(auth()->user()->persona->estudiosFormales as $estudios)
                        <div class="" id="estudio-{{$estudios->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$estudios->titulo_obtenido}}</h2>
                                    <button onclick="eliminarEstudioFormal({{$estudios->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">

                                            <tbody>
                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Finalizado: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->finalizado?'Si':'No'}}</span>
                                                    </div>

                                                </td>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Fecha <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->fecha_obtenido_titulo}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Nivel de formacion:
                                                        <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->nivelFormacion->descripcion}}</span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Area conocimiento:
                                                        <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->subAreaConocimiento->descripcion}}</span>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Sub area conocimiento:
                                                        <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->subAreaConocimiento->areaConocimiento->descripcion}}</span>
                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    @endforeach
                </div>


            </div>
            <div id="seguridad_social" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Informacion seguridad social</h2>
                </div>
                <div class="p-5">
                    <form id="seguridad_social-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>EPS</label>
                                <div class="mt-2">
                                    <select data-search="true" class="tail-select w-full" name="eps_id"
                                            id="input-eps_id">
                                        @foreach(\App\Models\EntidadesSeguridadSocial::all() as $data)
                                            <option
                                                {{auth()->user()->persona->eps_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="">
                                <label>Pension</label>
                                <div class="mt-2">
                                    <select data-search="true" class="tail-select w-full" name="pensiones_id"
                                            id="input-pensiones_id">
                                        @foreach(\App\Models\EntidadesSeguridadSocial::all() as $data)
                                            <option
                                                {{auth()->user()->persona->pensiones_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="">
                                <label>Cesantias</label>
                                <div class="mt-2">
                                    <select data-search="true" class="tail-select w-full" name="cesantias_id"
                                            id="input-cesantias_id">
                                        @foreach(\App\Models\EntidadesSeguridadSocial::all() as $data)
                                            <option
                                                {{auth()->user()->persona->cesantias_id==$data->id?"selected":""}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-seguridad_social" type="button" class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="educacion_no_formal" class="intro-y lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Estudios no formales</h2>
                        <button class="btn btn-primary mr-1 mb-2" onclick="agregarEstudioNoFormal()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerEstudiosNoFormales">
                    @foreach(auth()->user()->persona->educacionNoFormales as $estudios)

                        <div class="" id="estudioNoFormal-{{$estudios->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$estudios->tituloEstudio}}</h2>
                                    <button onclick="eliminarEstudioNoFormal({{$estudios->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">

                                            <tbody>
                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Numero de horas: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->num_horas}}</span>
                                                    </div>

                                                </td>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Fecha certificado: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->fecha_certificado}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Nivel de formación:
                                                        <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$estudios->nivel->descripcion}}</span>
                                                    </div>
                                                </td>


                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="certificaciones" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Idiomas</h2>
                        <button class="btn btn-primary mr-1 mb-2"
                                onclick="agregarIdioma()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerIdioma">
                    @foreach(auth()->user()->persona->idiomas as $idioma)
                        <div class="" id="idioma-{{$idioma->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$idioma->idioma->nombre}}</h2>
                                    <button onclick="eliminarIdioma({{$idioma->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">

                                            <tbody>
                                            <tr>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Escrito: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$idioma->escrito}}</span>
                                                    </div>
                                                </td>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Leido:<span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$idioma->leido}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            <tr>


                                                <td>
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Hablado: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$idioma->hablado}}</span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Escuchado: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$idioma->escuchado}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="experiencia_laboral" class="intro-y lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Experiencia Laboral</h2>
                        <button class="btn btn-primary mr-1 mb-2"
                                onclick="agregarExperienciaLaboral()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerExperiencia">
                    @foreach(auth()->user()->persona->experiencias as $experiencia)
                        <div class="" id="experiencia-{{$experiencia->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$experiencia->nombre_empresa_empleadora}}</h2>
                                    <button onclick="eliminarExperienciaLaboral({{$experiencia->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">

                                            <tbody>
                                            <tr>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Tipo de trabajo: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$experiencia->actividad_economina_empleador}}</span>
                                                    </div>
                                                </td>

                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Principales funciones:<span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$experiencia->principal_funciones_cargo}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            <tr>


                                                <td>
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Fecha inicio: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$experiencia->fecha_inicio}}</span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="font-medium whitespace-nowrap dark:border-dark-5">
                                                        Fecha retiro: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$experiencia->fecha_retiro!=null?$experiencia->fecha_retiro:'Actual'}}</span>
                                                    </div>
                                                </td>

                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="habilidades_ti" class="intro-y lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Habilidades</h2>
                        <button class="btn btn-primary mr-1 mb-2"
                                onclick="agregarHabilidad()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerHabilidades">
                    @foreach(auth()->user()->persona->habilidades as $habilidad)
                        <div class="" id="habilidad-{{$habilidad->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$habilidad->nivel_habilidad->habilidad->descripcion}}</h2>
                                    <button onclick="eliminarHabilidad({{$habilidad->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Nivel de Habilidad: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$habilidad->nivel_habilidad->descripcion}}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- END: Modal Toggle -->
    <!-- BEGIN: Modal Content -->
    <div id="modal-visor" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-10 text-center">
                    <p id="modal-mensaje"></p>
                    <iframe width="100%" height="600px" id="visor-documental">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>
    <script>
        cash(function () {

            async function guardar(btn, form, url) {
                // Reset state
                cash(form).find('.input').removeClass('border-theme-6')
                cash(form).find('.login__input-error').html('')
                // Loading state
                cash(btn).html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1500)
                let formData = new FormData(cash(form)[0]);
                axios.post(url,
                    formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(function (res) {
                    //location.href = '/'
                    Swal.fire({
                        icon: 'success',
                        title: res.data.message,
                        showConfirmButton: false,
                        timer: 3000
                    })
                    cash(btn).html('Guardar')
                    getContadoresPersona();
                }).catch(err => {
                    cash(btn).html('Guardar')
                    if (err.response.data.message != 'Wrong email or password.') {
                        for (const [key, val] of Object.entries(err.response.data.errors)) {
                            cash(`#input-${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                    } else {
                        cash(`#input-password`).addClass('border-theme-6')
                        cash(`#error-password`).html(err.response.data.message)
                    }
                })
            }

            cash('#btn-datos-personales').on('click', function () {
                guardar('#btn-datos-personales', '#datos-personales-form', 'actualizarDatosPersonales')
            })
            cash('#btn-informacion_familiar').on('click', function () {
                guardar('#btn-informacion_familiar', '#informacion_familiar-form', 'actualizarInfoFamiliar')
            })
            cash('#btn-seguridad_social').on('click', function () {
                guardar('#btn-seguridad_social', '#seguridad_social-form', 'actualizarInfoSeguridadSocial')
            })
            cash('#btn-certificaciones').on('click', function () {
                guardar('#btn-certificaciones', '#certificaciones-form', 'actualizarCertificaciones')
            })

            cash('.link-tab').on('click', function () {

                let divs = document.getElementsByClassName("link-tab");
                let numDivs = divs.length;
                for (let i = 0; i < numDivs; i++) {
                    divs[i].classList.remove("text-theme-1", "dark:text-theme-10", "font-medium");
                }

                this.classList.add("text-theme-1", "dark:text-theme-10", "font-medium");

                let divs2 = document.getElementsByClassName("card-fom-empresa");
                let numDivs2 = divs2.length;
                for (let i = 0; i < numDivs2; i++) {
                    divs2[i].classList.add("hidden");
                }
                document.getElementById(this.getAttribute('aria-expanded')).classList.remove("hidden")

            })

            cash('#verDocumento').on('click', function (e) {
                axios.post(`{{url('getContadoresPersona')}}`
                ).then(function (res) {
                    console.log(res.data['identificacion_rurataarchivo']);
                    if (res.data['identificacion_rurataarchivo'] == "" || res.data['identificacion_rurataarchivo'] == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['identificacion_rurataarchivo']);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });
            cash('#verCertificadoIngles').on('click', function (e) {
                axios.post(`{{url('getContadoresEmpresa')}}`
                ).then(function (res) {
                    console.log(res.data['empresa_data'].rut_rutaarachivo);
                    if (res.data['empresa_data'].rut_rutaarachivo == "" || res.data['empresa_data'].rut_rutaarachivo == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['empresa_data'].rut_rutaarachivo);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });

            function loadIframe(iframeName, url) {
                let $iframe = cash('#' + iframeName);
                if ($iframe.length) {
                    $iframe.attr('src', url);
                    return false;
                }
                return true;
            }
        })

        $('#select-pais').on('change', function () {
            $("#select-departamento").empty();
            let countryID = $(this).val();
            let departamentos = [];
            if (countryID) {
                axios.post(`{{url('get-states-by-country')}}`, {
                    country_id: countryID
                }).then(function (res) {
                    // console.log(res.data);
                    if (res) {
                        for (let item in res.data.states) {
                            let itemSelect2 = {
                                id: res.data.states[item].id,
                                text: res.data.states[item].name
                            };
                            departamentos.push(itemSelect2)
                        }
                        $('#select-departamento').select2({
                            allowClear: true,
                            placeholder: "Selecciona una Departamento...",
                            data: departamentos,
                        });
                        $('#select-departamento').val({{auth()->user()->persona->ciudad_nacimiento_id?auth()->user()->persona->ciudad->departamento->id:'788'}}).trigger('change');
                    } else {
                        $("#select-departamento").empty();
                    }
                }).catch(e => {
                    console.log(e);
                })
            } else {
                $("#select-departamento").empty();
                $("#ciudad").empty();
            }
        })

        $('#select-departamento').on('change', function () {
            $("#ciudad").empty();
            let stateID = $(this).val();
            let ciudad = [];
            if (stateID) {
                axios.post(`{{url('get-cities-by-state')}}`, {
                    state_id: stateID
                }).then(function (res) {
                    // console.log(res.data);
                    if (res) {
                        for (let item in res.data.cities) {
                            let itemSelect2 = {
                                id: res.data.cities[item].id,
                                text: res.data.cities[item].name
                            };
                            ciudad.push(itemSelect2)
                        }
                        $('#ciudad').select2({
                            allowClear: true,
                            placeholder: "Selecciona una Ciudad...",
                            data: ciudad
                        });
                        $('#ciudad').val({{auth()->user()->persona->ciudad_nacimiento_id?auth()->user()->persona->ciudad->id:'13029'}}).trigger('change');
                    } else {
                        $("#ciudad").empty();
                    }
                }).catch(e => {
                    console.log(e);
                })
            } else {
                $("#ciudad").empty();
            }
        })

        $('#remove_logo').on('click', function (e) {
            let image = document.getElementById('image_logo')
            image.src = '{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}';
        });

        $('#input-logo').on('change', function (e) {
            // Creamos el objeto de la clase FileReader
            let reader = new FileReader();
            // Leemos el archivo subido y se lo pasamos a nuestro fileReader
            reader.readAsDataURL(e.target.files[0]);
            // Le decimos que cuando este listo ejecute el código interno
            reader.onload = function () {
                let image = document.getElementById('image_logo')
                image.src = reader.result;
            };
        })

            $('#remove_logo').on('click', function (e) {
                let image = document.getElementById('image_logo')
                image.src = '{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}';
                $('#logo_rutararchivo').trigger("change");
            });

            $('#logo_rutararchivo').on('change', function (e) {
                // Creamos el objeto de la clase FileReader
                let reader = new FileReader();
                // Leemos el archivo subido y se lo pasamos a nuestro fileReader
                reader.readAsDataURL(e.target.files[0]);
                // Le decimos que cuando este listo ejecute el código interno
                reader.onload = function () {
                    let image = document.getElementById('image_logo')
                    image.src = reader.result;
                };
            })
        /*******************************/
        function agregarEstudioFormal() {
            Swal.fire({
                title: 'Estudios formales',
                html:
                    '<select id="nivelFormacion" class="input w-full border form-select">' +
                    '<option selected disabled>Nivel de formacion*</option>' +
                    @foreach(\App\Models\NivelFormacion::all() as $nivelFormacion)
                        '<option value="{{$nivelFormacion->id}}">{{$nivelFormacion->descripcion}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<input id="tituloObtenino" placeholder="Titulo obtenido*" class="form-control"><br><br>' +
                    '<label class="w-full" style="float: left">Diploma del titulo(opcional)</label>' +
                    '<input id="diploma_rutaarchivo" type="file" class="form-control" accept="application/pdf"><br><br>' +
                    '<select id="finalizado" class="input w-full border form-select">' +
                    '<option selected disabled>Finalizado</option>' +
                    '<option value="1">Si</option>' +
                    '<option value="0">No</option>' +
                    '</select><br><br>' +
                    '<label class="w-full" style="float: left">Fecha de obtencion del titulo*</label>' +
                    '<input type="date" class="form-control" id="fecha-titulo"><br><br>' +
                    '<select id="area-conocimiento" class="input w-full border form-select">' +
                    '<option selected disabled>Area del conocimiento</option>' +
                    @foreach(\App\Models\AreaConocimiento::all() as $data)
                        '<option value="{{$data->id}}">{{$data->descripcion}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<select id="sub-area-conocimiento" class="input w-full border form-select">' +
                    '<option selected disabled>Sub Area del conocimiento</option>' +
                    '<option></option>' +
                    '</select>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {
                    $('#area-conocimiento').on('change', function () {
                        $("#sub-area-conocimiento").empty();
                        $('#sub-area-conocimiento').prepend('<option selected disabled>Sub Area del conocimiento</option>');
                        let area = $(this).val();
                        let subArea = [];
                        if (area) {
                            axios.post(`{{url('getSubAreaConocimiento')}}`, {
                                area_id: area
                            }).then(function (res) {
                                // console.log(res.data);
                                if (res) {
                                    for (let item in res.data.subAreas) {
                                        let $option = $('<option />', {
                                            text: res.data.subAreas[item].descripcion,
                                            value: res.data.subAreas[item].id,
                                        });
                                        $('#sub-area-conocimiento').append($option);
                                    }
                                } else {
                                    $("#sub-area-conocimiento").empty();
                                }
                            }).catch(e => {
                                console.log(e);
                            })
                        } else {
                            $("#select-departamento").empty();
                            $("#ciudad").empty();
                        }
                    })

                },
                preConfirm: () => {
                    // console.log($('#nivelFormacion').val());
                    if ($('#nivelFormacion').val() === null) {
                        Swal.showValidationMessage(
                            `Escoja un nivel de formacion de la lista`
                        )
                        return;
                    }
                    if ($('#finalizado').val() === null) {
                        Swal.showValidationMessage(
                            `Especifique si ha finalizado`
                        )
                        return;
                    }
                    if ($('#sub-area-conocimiento').val() === null) {
                        Swal.showValidationMessage(
                            `Escoja una Sub area del conocimiento de la lista`
                        )
                        return;
                    }

                    if ($('#tituloObtenino').val() === "") {
                        Swal.showValidationMessage(
                            `Especifica el titulo obtenido`
                        )
                        return;
                    }
                    if ($('#fecha-titulo').val() === "") {
                        Swal.showValidationMessage(
                            `Especifique la fecha de titulacion`
                        )
                        return;
                    }
                    //ajax
                    let nivelFormacion = $('#nivelFormacion').val();
                    let finalizado = $('#finalizado').val();
                    let subAreaConocimiento = $('#sub-area-conocimiento').val();
                    let tituloObtenino = $('#tituloObtenino').val();
                    let fechaTitulo = $('#fecha-titulo').val();
                    let diploma_rutaarchivo = $('#diploma_rutaarchivo').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarEstudioFormal')}}`, {
                        nivelFormacion: nivelFormacion,
                        finalizado: finalizado,
                        subAreaConocimiento: subAreaConocimiento,
                        tituloObtenino: tituloObtenino,
                        fechaTitulo: fechaTitulo,
                        diploma_rutaarchivo: diploma_rutaarchivo,
                    }).then(function (res) {

                        $('#containerEstudiosFormales').append("" +
                            "<div class='' id='estudio-" + res.data['estudio'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['estudio'].titulo_obtenido + "</h2>" +
                            "<button onclick='eliminarEstudioFormal(" + res.data['estudio'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Finalizado: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].finalizado + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Fecha: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].fecha_obtenido_titulo + "</span> </div></td>" +
                            "</tr>" +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Nivel de formacion: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].nivel_formacion_id + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Area conocimiento: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].finalizado + "</span> </div></td>" +
                            "</tr>" +
                            "<tr> " +
                            "<td colspan='2'> <div class='font-medium whitespace-nowrap'> Sub area conocimiento: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].sub_area_conocimiento_id + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>")


                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego el estudio formal correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                // console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarEstudioFormal(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando Estudio Formal..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarEstudioFormal')}}`, {
                idEstudio: id
            }).then(function (res) {
                // console.log(res.data);
                $("#estudio-" + id).remove();
                swal.close()
            }).catch(e => {
                console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /********************************/
        function agregarEstudioNoFormal() {
            Swal.fire({
                title: 'Estudios no formales',
                html:
                    '<input id="tituloEstudio" placeholder="Titulo Estudio" class="form-control"><br><br>' +
                    '<select id="nivelFormacion" class="input w-full border form-select">' +
                    '<option selected disabled>Nivel de formacion</option>' +
                    @foreach(\App\Models\NivelFormacionNoFormal::all() as $nivelFormacion)
                        '<option value="{{$nivelFormacion->id}}">{{$nivelFormacion->descripcion}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<label class="w-full" style="float: left">Numero de horas</label>' +
                    '<input id="horasCursoNoFormal" type="number" class="form-control"><br><br>' +
                    '<label class="w-full" style="float: left">Fecha de obtencion del titulo</label>' +
                    '<input type="date" class="form-control" id="fecha-certificado"><br><br>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {

                },
                preConfirm: () => {
                    if ($('#tituloEstudio').val() === null) {
                        Swal.showValidationMessage(
                            `Dijite el titulo del estudio realizado`
                        )
                        return;
                    }
                    if ($('#nivelFormacion').val() === null) {
                        Swal.showValidationMessage(
                            `Escoja un nivel de formacion de la lista`
                        )
                        return;
                    }
                    if ($('#horasCursoNoFormal').val() === null) {
                        Swal.showValidationMessage(
                            `Especifique el numero de horas del curso`
                        )
                        return;
                    }
                    if ($('#fecha-certificado').val() === null) {
                        Swal.showValidationMessage(
                            `Seleccione la fecha de certificacion`
                        )
                        return;
                    }
                    //ajax
                    let tituloEstudio = $('#tituloEstudio').val();
                    let nivelFormacion = $('#nivelFormacion').val();
                    let horasCursoNoFormal = $('#horasCursoNoFormal').val();
                    let fechaCertificado = $('#fecha-certificado').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarEstudioNoFormal')}}`, {
                        tituloEstudio: tituloEstudio,
                        nivel_formacion_no_formal_id: nivelFormacion,
                        num_horas: horasCursoNoFormal,
                        fecha_certificado: fechaCertificado,
                    }).then(function (res) {
                        // console.log(res.data);


                        $('#containerEstudiosNoFormales').append("" +
                            "<div class='' id='estudioNoFormal-" + res.data['estudio'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['estudio'].tituloEstudio + "</h2>" +
                            "<button onclick='eliminarEstudioNoFormal(" + res.data['estudio'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Numero de horas: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].num_horas + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Fecha certificado: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].fecha_certificado + "</span> </div></td>" +
                            "</tr>" +
                            "<tr> " +
                            "<td colspan='2'> <div class='font-medium whitespace-nowrap'> Nivel de formación: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['estudio'].nivel_formacion_no_formal_id + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>")


                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego el estudio no formal correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarEstudioNoFormal(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando estudio no formal..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarEstudioNoFormal')}}`, {
                idEstudio: id
            }).then(function (res) {
                // console.log(res.data);
                $("#estudioNoFormal-" + id).remove();
                swal.close()
            }).catch(e => {
                console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /********************/
        function agregarExperienciaLaboral() {
            Swal.fire({
                title: 'Experiencia Laboral',
                html:
                    '<label class="w-full" style="float: left">Nombre de la empresa empleadora</label>' +
                    '<input id="nombreEmpresa" type="text" class="form-control"><br><br>' +
                    '<label class="w-full" style="float: left">Tipo de trabajo a que se dedica la empresa</label>' +
                    '<input id="tipoTrabajo" type="text" class="form-control"><br><br>' +
                    '<label class="w-full" style="float: left">Principales funciones del cargo ocupado</label>' +
                    '<input id="funcionesPrincipales" type="text" class="form-control"><br><br>' +
                    '<label class="w-full" style="float: left">Fecha de inicio</label>' +
                    '<input type="date" class="form-control" id="fecha-inicio"><br><br>' +
                    '<label class="w-full" style="float: left">Fecha de retiro</label>' +
                    '<input type="date" class="form-control" id="fecha-retiro"><br><br>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {

                },
                preConfirm: () => {
                    if ($('#nombreEmpresa').val() === "") {
                        Swal.showValidationMessage(
                            `Nombre de la empresa obligatorio`
                        )
                        return;
                    }
                    if ($('#tipoTrabajo').val() === "") {
                        Swal.showValidationMessage(
                            `Comente que tipo de trabajo realizaba`
                        )
                        return;
                    }
                    if ($('#funcionesPrincipales').val() === "") {
                        Swal.showValidationMessage(
                            `Comente sus funciones principales`
                        )
                        return;
                    }
                    if ($('#fecha-inicio').val() === "") {
                        Swal.showValidationMessage(
                            `Seleccione la fecha de inicio`
                        )
                        return;
                    }
                    //ajax
                    let nombreEmpresa = $('#nombreEmpresa').val();
                    let tipoTrabajo = $('#tipoTrabajo').val();
                    let funcionesPrincipales = $('#funcionesPrincipales').val();
                    let fechaInicio = $('#fecha-inicio').val();
                    let fechaRetiro = $('#fecha-retiro').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarExperienciaLaboral')}}`, {
                        nombre_empresa_empleadora: nombreEmpresa,
                        actividad_economina_empleador: tipoTrabajo,
                        principal_funciones_cargo: funcionesPrincipales,
                        fecha_inicio: fechaInicio,
                        fecha_retiro: fechaRetiro,
                    }).then(function (res) {
                        // console.log(res.data);


                        $('#containerExperiencia').append("" +
                            "<div class='' id='estudio-" + res.data['experiencia'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['experiencia'].nombre_empresa_empleadora + "</h2>" +
                            "<button onclick='eliminarExperienciaLaboral(" + res.data['experiencia'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Tipo de trabajo: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['experiencia'].actividad_economina_empleador + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Principales funciones: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['experiencia'].principal_funciones_cargo + "</span> </div></td>" +
                            "</tr>" +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Fecha inicio: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['experiencia'].fecha_inicio + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Fecha retiro: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['experiencia'].fecha_retiro + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>")


                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego la experiencia laboral correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarExperienciaLaboral(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando experiencia laboral..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarExperienciaLaboral')}}`, {
                id: id
            }).then(function (res) {
                console.log(res.data);
                $("#experiencia-" + id).remove();
                swal.close()
            }).catch(e => {
                console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /********************/
        function agregarHabilidad() {
            Swal.fire({
                title: 'Habilida',
                html:
                    '<select id="select-habilidad" class="input w-full border form-select">' +
                    '<option selected disabled>Habilidad</option>' +
                    @foreach(\App\Models\Habilidades::all() as $habilidad)
                        '<option value="{{$habilidad->id}}">{{$habilidad->descripcion}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<select id="select-nivel-habilidad" class="input w-full border form-select">' +
                    '<option selected disabled>Seleccione un nivel</option>' +
                    '<option></option>' +
                    '</select>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {
                    $('#select-habilidad').on('change', function () {
                        $("#select-nivel-habilidad").empty();
                        $('#select-nivel-habilidad').prepend('<option selected disabled>Seleccione un nivel</option>');
                        let habilidad = $(this).val();
                        let subArea = [];
                        if (habilidad) {
                            axios.post(`{{url('getNivelesHabilidad')}}`, {
                                habilidad_id: habilidad
                            }).then(function (res) {
                                // console.log(res.data);
                                if (res) {
                                    for (let item in res.data.nivel) {
                                        let $option = $('<option />', {
                                            text: res.data.nivel[item].descripcion,
                                            value: res.data.nivel[item].id,
                                        });
                                        $('#select-nivel-habilidad').append($option);
                                    }
                                } else {
                                    $("#select-nivel-habilidad").empty();
                                }
                            }).catch(e => {
                                console.log(e);
                            })
                        } else {
                            $("#select-nivel-habilidad").empty();
                        }
                    })
                },
                preConfirm: () => {
                    if ($('#select-nivel-habilidad').val() === "") {
                        Swal.showValidationMessage(
                            `Seleccione una habilidad y su nivel`
                        )
                        return;
                    }
                    //ajax
                    let nivelHabilidad = $('#select-nivel-habilidad').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarHabilidad')}}`, {
                        nivel_habilidad_id: nivelHabilidad,
                    }).then(function (res) {
                        console.log(res.data);
                        console.log(res.data['actualizar'] == true);
                        console.log(res.data['actualizar']);
                        if (res.data['actualizar'] == true) {
                            $("#habilidad-" + res.data['habilidad'].id).remove();
                        }
                        $('#containerHabilidades').append("" +
                            "<div class='' id='habilidad-" + res.data['habilidad'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['titulo'] + "</h2>" +
                            "<button onclick='eliminarHabilidad(" + res.data['habilidad'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Nivel de Habilidad: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['nivel'] + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>");

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego la experiencia laboral correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarHabilidad(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando habilidad..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarHabilidad')}}`, {
                idHabilidad: id
            }).then(function (res) {
                console.log(res.data);
                $("#habilidad-" + id).remove();
                swal.close()
            }).catch(e => {
                console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /********************/
        function agregarIdioma() {
            Swal.fire({
                title: 'Idioma',
                html:
                    '<select id="select-idioma" class="input w-full border form-select">' +
                    '<option selected disabled>Idioma</option>' +
                    @foreach(\App\Models\Idioma::all() as $idioma)
                        '<option value="{{$idioma->id}}">{{$idioma->nombre}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<label class="w-full" style="float: left">Nivel escrito</label>' +
                    '<select id="select-escrito" class="input w-full border form-select">' +
                    '<option val="Bajo">Bajo</option>' +
                    '<option val="Medio">Medio</option>' +
                    '<option val="Alto">Alto</option>' +
                    '</select>' +
                    '<label class="w-full" style="float: left">Nivel leido</label>' +
                    '<select id="select-leido" class="input w-full border form-select">' +
                    '<option val="Bajo">Bajo</option>' +
                    '<option val="Medio">Medio</option>' +
                    '<option val="Alto">Alto</option>' +
                    '</select>' +
                    '<label class="w-full" style="float: left">Nivel hablado</label>' +
                    '<select id="select-hablado" class="input w-full border form-select">' +
                    '<option val="Bajo">Bajo</option>' +
                    '<option val="Medio">Medio</option>' +
                    '<option val="Alto">Alto</option>' +
                    '</select>' +
                    '<label class="w-full" style="float: left">Nivel escuchado</label>' +
                    '<select id="select-escuchado" class="input w-full border form-select">' +
                    '<option val="Bajo">Bajo</option>' +
                    '<option val="Medio">Medio</option>' +
                    '<option val="Alto">Alto</option>' +
                    '</select>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {

                },
                preConfirm: () => {
                    if ($('#select-idioma').val() === "") {
                        Swal.showValidationMessage(
                            `Seleccione un idioma`
                        )
                        return;
                    }
                    //ajax
                    let idioma = $('#select-idioma').val();
                    let escuchado = $('#select-escuchado').val();
                    let hablado = $('#select-hablado').val();
                    let leido = $('#select-leido').val();
                    let escrito = $('#select-escrito').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarIdioma')}}`, {
                        idIdioma: idioma,
                        escrito: escrito,
                        leido: leido,
                        hablado: hablado,
                        escuchado: escuchado,
                    }).then(function (res) {
                        console.log(res.data);
                        console.log(res.data['actualizar'] == true);
                        console.log(res.data['actualizar']);
                        if (res.data['actualizar'] == true) {
                            $("#idioma-" + res.data['peronaIdioma'].id).remove();
                        }
                        $('#containerIdioma').append("" +
                            "<div class='' id='idioma-" + res.data['peronaIdioma'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['idioma']+ "</h2>" +
                            "<button onclick='eliminarIdioma(" + res.data['peronaIdioma'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Escrito: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['peronaIdioma'].escrito + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Leido: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['peronaIdioma'].leido + "</span> </div></td>" +
                            "</tr>" +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Hablado: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['peronaIdioma'].hablado + "</span> </div></td>" +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Escuchado: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['peronaIdioma'].escuchado + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>")


                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego un idioma correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarIdioma(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando idioma..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarIdioma')}}`, {
                idIdioma: id
            }).then(function (res) {
                console.log(res.data);
                $("#idioma-" + id).remove();
                swal.close()
            }).catch(e => {
                console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /********************/
        function getContadoresPersona() {
            var resultado = 0;

            axios.post(`{{url('getContadoresPersona')}}`
            ).then(function (res) {

                // console.log(res)

                res.data['contID'] >= 1 ? $("#idPersona").removeClass("hidden") : "";
                res.data['contID'] >= 1 ? $("#verDocumento").hide() : $("#verDocumento").show();
                res.data['cont-cert-ingles'] >= 1 ? $("#verCertificadoIngles").hide() : $("#verCertificadoIngles").show();
                res.data['contID'] >= 1 ? $("#verDocumento").hide() : $("#verDocumento").show();

                for (var item in res.data) {

                    let tabs = document.getElementsByClassName("link-tab");
                    let numDivs = tabs.length;
                    for (let j = 0; j < numDivs; j++) {

                        if (tabs[j].getAttribute('aria-expanded') === item) {
                            // console.log(tabs[j].getAttribute('aria-expanded') + "-" + item + " - " + res.data[item])
                            resultado += res.data[item];
                            if (res.data[item] === 0) {
                                tabs[j].removeChild(tabs[j].lastElementChild);
                            } else {
                                tabs[j].lastElementChild.innerHTML = res.data[item]
                            }
                        }
                    }

                }
                if (resultado) {
                    $("#perfil-completado").hide();
                    $("#perfil-por-completar").show();
                } else {
                    $("#perfil-completado").show();
                    $("#perfil-por-completar").hide();
                    //generar reference boton de pago//
                }

            });
        }

        function pagarWompi() {
            axios.post(`{{url('getInfoPago')}}`
            ).then(function (res) {
                console.log(res);
                console.log(res.data);
                var checkout = new WidgetCheckout({
                    currency: 'COP',
                    amountInCents: res.data['precio'],//50000000,
                    reference: res.data['referencia'],//generada por la plataforma
                    publicKey: res.data['publicKey'],
                    //redirectUrl: 'https://transaction-redirect.wompi.co/check' // Opcional
                })
                checkout.open(function (result) {
                    var transaction = result.transaction
                    console.log('Transaction ID: ', transaction.id)
                    console.log('Transaction object: ', transaction)
                    if (transaction.status == "APPROVED") {
                        location.reload();
                    }
                    //location.reload();
                })
            });
        }


        $(document).ready(function () {

            getContadoresPersona()

            $('#select-pais').select2({
                allowClear: true,
                placeholder: "Selecciona un Pais...",
            });

            $('#select-pais').val({{auth()->user()->persona->ciudad_nacimiento_id?auth()->user()->persona->ciudad->departamento->pais->id:'47'}}).trigger('change');
            $('#select-departamento').select2({
                allowClear: true,
                placeholder: "Selecciona un Departamento...",
                data: []
            });
            $('#select-departamento').val({{auth()->user()->persona->ciudad_nacimiento_id?auth()->user()->persona->ciudad->departamento->id:'788'}}).trigger('change');
            $('#ciudad').select2(
                {
                    allowClear: true,
                    placeholder: "Selecciona una Ciudad...",
                    data: []
                }
            );
            $('#ciudad').val({{auth()->user()->persona->ciudad_nacimiento_id?auth()->user()->persona->ciudad->id:'13029'}}).trigger('change');
        });
    </script>
@endsection
