@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{$proyecto->nombre}}<p style="font-weight: 300;">Documento guía para proyectos I+D+i , ver página 15 , punto 3.8 Niveles de madurez tecnológica.
                <a href="">Aqui</a></p></h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button id="eliminarProyecto" class="btn btn-danger shadow-md mr-2">Eliminar Proyecto</button>
            <button id="registrarCotizacion" class="btn btn-primary shadow-md mr-2 hidden">Agregar Cotizacion</button>
        </div>
    </div>
    <div class="pos intro-y p-5 mt-5">
        <div class="col-span-12 lg:col-span-12">
            <div class="intro-y pr-1">
                <div class="box p-2">
                    <div class="pos__tabs nav nav-tabs justify-center" role="tablist">
                        <a id="pendiente-tab" data-toggle="tab" data-target="#pendiente" href="javascript:;"
                           class="flex-1 py-2 rounded-md text-center active" role="tab" aria-controls="pendientes"
                           aria-selected="true">Proyecto</a>
                        <a id="persona-tab" data-toggle="tab" data-target="#persona" href="javascript:;"
                           class="flex-1 py-2 rounded-md text-center" role="tab" aria-controls="personas"
                           aria-selected="false">Cotizaciones</a>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="pendiente" class="tab-pane active" role="tabpanel" aria-labelledby="pendiente-tab">
                    <div class="box p-5 mt-5">
                        <form id="form-proyecto">
                            @csrf
                            <input type="hidden" name="id" id="id" value="{{$proyecto->id}}">
                            <div class="grid grid-cols-12 gap-6 mt-5">
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>Nombre proyecto</label>
                                    <input type="text" id="input-nombre"
                                           name="nombre" class="form-control"
                                           value="{{$proyecto->nombre}}">
                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>Descripcion</label>
                                    <textarea rows="5" class="form-control" id="input-descripcion"
                                              name="descripcion"
                                              placeholder="Descripción: En este apartado se detallan aspectos como el problema general que resuelve la tecnología o la innovación, así como la solución que se ha desarrollado, involucrando algunos datos que puedan ser de interés para un lector. Adicionalmente, se incluyen detalles técnicos y económicos que pueden ser comprendidos por un potencial comprador o inversionista con un nivel intermedio de conocimiento en el campo técnico.">{{$proyecto->descripcion}}</textarea>
                                    <div id="error-descripcion"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>Objetivo general</label>
                                    <textarea rows="5" class="form-control" id="input-obj_general"
                                              name="obj_general"
                                              placeholder="">{{$proyecto->obj_general}}</textarea>
                                    <div id="error-obj_general"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>Objetivo especifico</label>
                                    <textarea rows="5" class="form-control" id="input-obj_especifico"
                                              name="obj_especifico"
                                              placeholder="">{{$proyecto->obj_especifico}}</textarea>
                                    <div id="error-obj_especifico"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>ÁREA TEMÁTICA, APLICACIONES Y USUARIOS</label>
                                    <textarea rows="5" class="form-control" id="input-area_tematica"
                                              name="area_tematica"
                                              placeholder="Descripción: En esta sección se describe el área temática a la que se circunscribe la tecnología o la innovación y se describen sus principales aplicaciones. Adicionalmente, se detallan los correspondientes sectores o subsectores económicos que serían potenciales usuarios, especificando tipos de empresas o industrias, perfiles de usuarios finales, o aplicaciones técnicas específicas, transversales a diversos sectores.">{{$proyecto->area_tematica}}</textarea>
                                    <div id="error-area_tematica"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>BENEFICIOS Y VENTAJAS</label>
                                    <textarea rows="5" class="form-control" id="input-beneficios"
                                              name="beneficios"
                                              placeholder="Descripción: Aquí se presentan los principales beneficios y ventajas derivadas del uso de la tecnología o la innovación, que podrían percibir sus usuarios. Puede tratarse de aspectos económicos (reducción de costos, ahorros en el proceso), técnicos (mejores tiempos, menores consumos energéticos, mayor rendimiento), ambientales (reducción de emisiones, menor impacto) y culturales. En la medida de lo posible, es recomendable divulgar datos precisos sobre los beneficios, que hayan sido previamente analizados y cuantificados, tomando como referencia la investigación previa desarrollada y los PAED de las entidades territoriales, atendiendo a las demandas establecidas en éstas.">{{$proyecto->beneficios}}</textarea>
                                    <div id="error-beneficios"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>ESTADO DE DESARROLLO</label>
                                    <textarea rows="5" class="form-control" id="input-estado_desarrollo"
                                              name="estado_desarrollo"
                                              placeholder="Descripción: En esta sección se describe el estado de desarrollo de la tecnología, aclarando si se encuentra a nivel de laboratorio, prototipo, escalado, detallando el tamaño, volumen o características actuales del equipo, dispositivo, proceso, entre otros.">{{$proyecto->estado_desarrollo}}</textarea>
                                    <div id="error-estado_desarrollo"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>PROTECCIÓN DE LA PROPIEDAD INTELECTUAL</label>
                                    <textarea rows="5" class="form-control" id="input-propiedad_intelectual"
                                              name="propiedad_intelectual"
                                              placeholder="Descripción: Se declara si existe protección de la propiedad intelectual (patente de invención, patente de utilidad, diseño industrial, registro de marca, registro de derecho de autor y registro de obtentor, entre otros). En caso de existir protección, se sugiere detallar el número del expediente del registro o solicitud, así como los países en los que se ha solicitado protección.">{{$proyecto->propiedad_intelectual}}</textarea>
                                    <div id="error-propiedad_intelectual"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-12 xl:col-span-12 intro-y">
                                    <label>DIFERENCIAL</label>
                                    <textarea rows="5" class="form-control" id="input-diferencial"
                                              name="diferencial"
                                              placeholder="Descripción: En este apartado se presentan las ventajas de la tecnología, si se trata de una tecnología disruptiva (p. e. tecnologías emergentes), si tiene potencial de ser disruptiva, o es una tecnología tradicional. También se presenta un análisis comparativo de la tecnología o sus productos o servicios tecnológicos derivados, con relación a otras tecnologías existentes bien sea como inventos o como productos o servicios tecnológicos disponibles en el mercado.">{{$proyecto->diferencial}}</textarea>
                                    <div id="error-diferencial"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                                    <label>EXPLICACIÓN PDF/PPT</label>
                                    <input id="input-explicacion_PDF_PPT" name="explicacion_PDF_PPT" type="file"
                                           class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf, image/*">
                                    <a href="javascript:;" id="verExplicacion" class="btn btn-primary"
                                       style="display: none;">Ver Documento</a>

                                    <div id="error-explicacion_PDF_PPT"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                    <div
                                        class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                        id="idExplicacionExiste">
                                        <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga la
                                        Explicacion del proyecto
                                    </div>
                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                                    <label>URL VIDEO</label>
                                    <input type="text" id="input-url_video"
                                           name="url_video" class="form-control"
                                           value="{{$proyecto->url_video}}">
                                </div>
                                <div class="col-span-12 sm:col-span-6 xl:col-span-4 intro-y">
                                    <label>PRESUPUESTO EXCEL</label>
                                    <input id="input-presupuesto_EXEL" name="presupuesto_EXEL" type="file"
                                           class="form-control" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf, image/*">
                                    <a href="javascript:;" id="verPresupuesto" class="btn btn-primary"
                                       style="display: none;">Ver Documento</a>

                                    <div id="error-presupuesto_EXEL"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                    <div
                                        class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                        id="idPresupuestoExiste">
                                        <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga el
                                        Presupuesto del proyecto
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="flex justify-center mt-4">
                            <button id="btn-guardar-proyecto" type="button"
                                    class="btn btn-primary w-20 mt-3">
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
                <div id="persona" class="tab-pane" role="tabpanel" aria-labelledby="persona-tab">
                    <div class="box p-5 mt-5">
                        <p>Se sugiere contar con 3 cotizaciones </p>
                        <br>
                        <table id="dt-cotizaciones" class="table table-bordered table-hover table-striped w-100"
                               style="width:100%">
                            <thead>
                            <tr>
                                <th>Empresa</th>
                                <th>Cotizacion formal</th>
                                <th>Empresa</th>
                                <th>Cámara de comercio</th>
                                <th>Rut</th>
                                <th>Representante legal</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Empresa</th>
                                <th>Cotizacion formal</th>
                                <th>Empresa</th>
                                <th>Cámara de comercio</th>
                                <th>Rut</th>
                                <th>Representante legal</th>
                                <th class="text-center">Opciones</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END: Modal Toggle -->
    <!-- BEGIN: Modal Content -->
    <div id="modal-visor" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-10 text-center">
                    <p id="modal-mensaje"></p>
                    <iframe width="100%" height="600px" id="visor-documental">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
    @include('page.modal.cotizacion')

@endsection
@section('script')
    <script type="text/javascript">
        var PROYECTO = {!!$proyecto!!};
        cash(function () {
            cash("#proyecto_id").val(PROYECTO.id);
            $(document).ready(function () {
                cash('#pendiente-tab').on('click', function (e) {
                    cash("#registrarCotizacion").hide();
                    cash("#eliminarProyecto").show();
                })
                cash('#persona-tab').on('click', function (e) {
                    cash("#registrarCotizacion").show();
                    cash("#eliminarProyecto").hide();
                })
                $("#btn-guardar-proyecto").on('click', function () {
                    $("#guardar").prop('disabled', true);
                    var form = $('#form-proyecto')[0];
                    var formData = new FormData(form)
                    $.ajax({
                        url: '{{url('recurso/editarProyecto')}}',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                    }).done(function (response) {
                        PROYECTO = response.proyecto;
                        validarDocumentos()
                        $("#btn-guardar-proyecto").prop('disabled', false);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Ok!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                        var obj = error.responseJSON.errors;
                        $.each(obj, function (key, value) {
                            $("#error").html(value[0]);
                            $("#error").show();
                        });
                    }).always(function () {
                        $('#modal-empresa-registro .modal-content').removeClass("is-loading");
                        $("#guardar").prop('disabled', false);
                    });
                });
                cash('#verExplicacion').on('click', function (e) {
                    console.log(PROYECTO.explicacion_PDF_PPT);
                    if (PROYECTO.explicacion_PDF_PPT == "" || PROYECTO.explicacion_PDF_PPT == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + PROYECTO.explicacion_PDF_PPT);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
                cash('#verPresupuesto').on('click', function (e) {
                    console.log(PROYECTO.presupuesto_EXEL);
                    if (PROYECTO.presupuesto_EXEL == "" || PROYECTO.presupuesto_EXEL == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + PROYECTO.presupuesto_EXEL);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });

                function validarDocumentos() {
                    if (PROYECTO.explicacion_PDF_PPT == "" || PROYECTO.explicacion_PDF_PPT == null) {
                        $("#verExplicacion").hide();
                    } else {
                        $("#verExplicacion").show();
                    }
                    if (PROYECTO.presupuesto_EXEL == "" || PROYECTO.presupuesto_EXEL == null) {
                        $("#verPresupuesto").hide();
                    } else {
                        $("#verPresupuesto").show();
                    }
                }

                validarDocumentos();
                var TABLA_COTIZACION = $('#dt-cotizaciones').DataTable({
                    "ajax": {
                        "url": "{{ url('recurso/verCotizacion') }}",
                        "data": function (d) {
                            d.proyecto_id = PROYECTO.id;
                            d._token = $('meta[name="csrf-token"]').attr('content');
                        },
                        "type": "GET",
                        "dataSrc": function (data) {
                            var json = [];
                            console.log(data);
                            for (var item in data.msg) {
                                var itemJson = {
                                    id: data.msg[item].id,
                                    nombreEmpresa: data.msg[item].nombreEmpresa,
                                    cotizacion_PDF: data.msg[item].cotizacion_PDF,
                                    documento_PDF: data.msg[item].documento_PDF,
                                    camara_comercio_PDF: data.msg[item].camara_comercio_PDF,
                                    rut_PDF: data.msg[item].rut_PDF,
                                    representante_legal_PDF: data.msg[item].representante_legal_PDF,
                                    Opciones: ''
                                };
                                json.push(itemJson)
                            }
                            return json;
                        }
                    },
                    columns: [
                        {data: "nombreEmpresa"},
                        {data: "cotizacion_PDF"},
                        {data: "documento_PDF"},
                        {data: "camara_comercio_PDF"},
                        {data: "rut_PDF"},
                        {data: "representante_legal_PDF"},
                        {data: "Opciones"},
                    ], columnDefs: [
                        {
                            targets: 1,
                            render: function (data, type, full, meta) {
                                return '<button  class="btn btn-success mr-1 mb-2 descargar1"' +
                                    '           data-toggle="tooltip" data-placement="top" title="Ver cotizacion formal" data-original-title="Edit">' +
                                    '           <i class="fas fa-eye"></i>\n' +
                                    ' </button>';
                            },
                        }, {
                            targets: 2,
                            render: function (data, type, full, meta) {
                                return '<button  class="btn btn-success mr-1 mb-2 descargar2"' +
                                    '           data-toggle="tooltip" data-placement="top" title="Ver Documento de la empresa" data-original-title="Edit">' +
                                    '           <i class="fas fa-eye"></i>\n' +
                                    ' </button>';
                            },
                        }, {
                            targets: 3,
                            render: function (data, type, full, meta) {
                                return '<button  class="btn btn-success mr-1 mb-2 descargar3"' +
                                    '           data-toggle="tooltip" data-placement="top" title="Ver Documento de la camara de comercio" data-original-title="Edit">' +
                                    '           <i class="fas fa-eye"></i>\n' +
                                    ' </button>';
                            },
                        }, {
                            targets: 4,
                            render: function (data, type, full, meta) {
                                return '<button  class="btn btn-success mr-1 mb-2 descargar4"' +
                                    '           data-toggle="tooltip" data-placement="top" title="Ver Documento RUT" data-original-title="Edit">' +
                                    '           <i class="fas fa-eye"></i>\n' +
                                    ' </button>';
                            },
                        }, {
                            targets: 5,
                            render: function (data, type, full, meta) {
                                return '<button  class="btn btn-success mr-1 mb-2 descargar5"' +
                                    '           data-toggle="tooltip" data-placement="top" title="Ver Documento del representante legal" data-original-title="Edit">' +
                                    '           <i class="fas fa-eye"></i>\n' +
                                    ' </button>';
                            },
                        }, {
                            targets: 6,
                            render: function (data, type, full, meta) {
                                return '<button type="button" class="btn btn-danger mr-1 mb-2 eliminar" ' +
                                    '           data-toggle="tooltip" data-placement="top" title="Deshabilitar" data-original-title="Edit">' +
                                    '           <i class="fas fa-trash"></i>\n' +
                                    ' </button>';
                            },
                        },
                    ]
                });

                TABLA_COTIZACION.on('click', '.descargar1', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    cash("#modal-mensaje").html('');
                    loadIframe('visor-documental', '/' + data.cotizacion_PDF);
                    cash("#visor-documental").show();
                    cash("#modal-visor").modal("show");
                });
                TABLA_COTIZACION.on('click', '.descargar2', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    cash("#modal-mensaje").html('');
                    loadIframe('visor-documental', '/' + data.documento_PDF);
                    cash("#visor-documental").show();
                    cash("#modal-visor").modal("show");
                });
                TABLA_COTIZACION.on('click', '.descargar3', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    cash("#modal-mensaje").html('');
                    loadIframe('visor-documental', '/' + data.camara_comercio_PDF);
                    cash("#visor-documental").show();
                    cash("#modal-visor").modal("show");
                });
                TABLA_COTIZACION.on('click', '.descargar4', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    cash("#modal-mensaje").html('');
                    loadIframe('visor-documental', '/' + data.rut_PDF);
                    cash("#visor-documental").show();
                    cash("#modal-visor").modal("show");
                });
                TABLA_COTIZACION.on('click', '.descargar5', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    cash("#modal-mensaje").html('');
                    loadIframe('visor-documental', '/' + data.representante_legal_PDF);
                    cash("#visor-documental").show();
                    cash("#modal-visor").modal("show");
                });
                TABLA_COTIZACION.on('click', '.eliminar', function () {
                    $tr = $(this).closest('tr');
                    let data = TABLA_COTIZACION.row($tr).data();
                    Swal.fire({
                        title: 'Estas seguro?',
                        text: "Vas a eliminar esta cotizacion",
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Si, eliminar',
                    }).then((Delete) => {
                        if (Delete.isConfirmed) {
                            $.ajax({
                                    url: '/recurso/eliminarCotizacion',
                                    type: 'POST',
                                    data: {
                                        id: data.id,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    },
                                }
                            ).done(function (response) {
                                console.log(response);
                                if (response.status == "Error") {
                                    //error
                                } else {
                                    TABLA_COTIZACION.ajax.reload();
                                    swal({
                                        title: 'Ok!',
                                        text: 'Cotizacion eliminada',
                                        icon: 'success',
                                        buttons: {
                                            confirm: {
                                                className: 'btn btn-success'
                                            }
                                        }
                                    });
                                }
                                //return response;
                            }).fail(function (error) {
                                console.log(error);
                            });
                        } else {
                            swal.close();
                        }
                    });
                });

                cash('#registrarCotizacion').on('click', function (e) {
                    cash('#modal-cotizacion-registro').modal('show');
                    cash('#form-cotizacion')[0].reset();
                    cash('#actualizarCotizacion').hide();
                    cash('#guardarCotizacion').show();
                    cash("#errorCotizacion").hide();
                    cash("#accion-modal").html('Registrar');
                })
                cash('#guardarCotizacion').on('click', function (e) {
                    $("#guardarCotizacion").prop('disabled', true);
                    var form = $('#form-cotizacion')[0];
                    var formData = new FormData(form)
                    $.ajax({
                        url: '{{url('recurso/registrarCotizacion')}}',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                    }).done(function (response) {
                        $("#guardarCotizacion").prop('disabled', false);
                        TABLA_COTIZACION.ajax.reload();
                        cash('#modal-cotizacion-registro').modal('hide');
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Ok!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        })
                        //return response;
                    }).fail(function (error) {
                        console.log(error);
                        var obj = error.responseJSON.errors;
                        $.each(obj, function (key, value) {
                            $("#errorCotizacion").html(value[0]);
                            $("#errorCotizacion").show();
                        });
                    }).always(function () {
                        $('#modal-cotizacion-registro .modal-content').removeClass("is-loading");
                        $("#guardar").prop('disabled', false);
                    });
                })
                cash('#eliminarProyecto').on('click', function (e) {
                    Swal.fire({
                        title: 'Estas seguro?',
                        text: "Vas a eliminar este proyecto",
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Si, eliminar',
                    }).then((Delete) => {
                        if (Delete.isConfirmed) {
                            $.ajax({
                                    url: '/recurso/eliminarProyecto',
                                    type: 'POST',
                                    data: {
                                        id: PROYECTO.id,
                                        _token: $('meta[name="csrf-token"]').attr('content')
                                    },
                                }
                            ).done(function (response) {
                                console.log(response);
                                if (response.status == "Error") {
                                    //error
                                } else {
                                    location.href ="/proyecto";
                                }
                                //return response;
                            }).fail(function (error) {
                                console.log(error);
                            });
                        } else {
                            swal.close();
                        }
                    });
                })
            });
        })

        function loadIframe(iframeName, url) {
            var $iframe = $('#' + iframeName);
            if ($iframe.length) {
                $iframe.attr('src', url);
                return false;
            }
            return true;
        }
    </script>

@endsection
