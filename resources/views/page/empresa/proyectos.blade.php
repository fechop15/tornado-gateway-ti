@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button id="nuevoProyecto" class="btn btn-primary shadow-md mr-2">Agregar Proyecto</button>
        </div>
    </div>
    <div class="pos intro-y p-5 mt-5">
        <div class="grid grid-cols-12 gap-6 mt-5">
            @foreach(auth()->user()->empresa->proyecto as $proyecto)
                <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                    <a href="/proyecto/{{$proyecto->id}}">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <i data-feather="book" class="report-box__icon text-theme-10"></i>
                                    <div class="ml-auto">
                                        <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer"
                                             title="{{$proyecto->estado}}">
                                            {{$proyecto->estado}} <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-base text-gray-600 mt-1">{{$proyecto->nombre}}</div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#nuevoProyecto").on('click', function () {
                Swal.fire({
                    title: 'Nombre del proyecto',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off',
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Crear',
                    showLoaderOnConfirm: true,
                    preConfirm: (login) => {
                        return $.ajax({
                            url: "{{ url('recurso/registrarProyecto') }}",
                            type: 'POST',
                            data: {
                                nombre: login,
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },
                        }).done(function (response) {
                            location.reload();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Ok!',
                                text: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }).fail(function (error) {
                            console.log(error)
                            Swal.showValidationMessage(
                                `error: ${error.errors.message}`
                            );
                        })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.isConfirmed) {

                    }
                });
            });
        });
    </script>

@endsection
