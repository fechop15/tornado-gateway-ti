@extends('../layout/' . $layout)

@section('subhead')
    <title>Mi Perfil - GATEWAYTI</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Perfil Empresa</h2>
    </div>

    {{--    @if(auth()->user()->empresa->pagos->last() && auth()->user()->empresa->pagos->last()->estado==1)--}}
    {{--        <div class="intro-y" style="z-index: auto;">--}}
    {{--            @if(auth()->user()->empresa->pagos->last()->fechaFin && auth()->user()->empresa->pagos->last()->fechaFin > \Carbon\Carbon::now())--}}
    {{--            <div id="perfil-completado" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
    {{--                <div class="flex items-center">--}}
    {{--                    <div class="font-medium text-lg">Perfil certificado</div>--}}
    {{--                    <div class="text-xs px-1 text-gray-800 ml-auto">--}}
    {{--                        <a target="_blank"  href="/descargarCertificado/{{auth()->user()->empresa->pagos->last()->id}}" class="btn btn-dark w-32 mr-2 mb-2"><i--}}
    {{--                                data-feather="award" class="w-4 h-4 mr-2"></i> Descargar--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="mt-3">--}}
    {{--                    Tu perfil ha sido verificado, ya puedes gozar de los pribilegios de CREINTEC por {{\Carbon\Carbon::now()->diffInDays(auth()->user()->empresa->pagos->last()->fechaFin)}} dias.--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            @elseif(!auth()->user()->empresa->pagos->last()->fechaFin)--}}
    {{--            <div id="perfil-completado" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
    {{--                <div class="flex items-center">--}}
    {{--                    <div class="font-medium text-lg">Pago completado</div>--}}
    {{--                    <div class="text-xs px-1 text-gray-800 ml-auto">--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="mt-3">--}}
    {{--                    Tu pago se ha confirmado en CREINTEC, estamos verificando tu perfil y generando tu certificado.--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            @else--}}
    {{--                <div id="perfil-completado" style="display: none" class="rounded-md px-5 py-4 mb-2 bg-theme-6 text-white">--}}
    {{--                    <div class="flex items-center">--}}
    {{--                        <div class="font-medium text-lg">Menbrecia expirada</div>--}}
    {{--                        <div class="text-xs px-1 text-gray-800 ml-auto">--}}

    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="mt-3">--}}
    {{--                        Su certificacion ha expirado hace {{\Carbon\Carbon::now()->diffInDays(auth()->user()->empresa->pagos->last()->fechaFin)}} dias, actualize la informacion del perfil y renueve su membrecia a CREINTEC para gozar de los servicos.--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            @endif--}}
    {{--        </div>--}}
    {{--    @else--}}
    {{--        <div class="intro-y">--}}
    {{--            <div id="perfil-completado" style="display: block" class="rounded-md px-5 py-4 mb-2 bg-theme-1 text-white">--}}
    {{--                <div class="flex items-center">--}}
    {{--                    <div class="font-medium text-lg">Perfil completado</div>--}}
    {{--                    <div class="text-xs px-1 text-gray-800 ml-auto">--}}
    {{--                        <button onclick="pagarWompi()"  class="btn btn-dark w-32 mr-2 mb-2" > <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Pagar </button>--}}

    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="mt-3">--}}
    {{--                    Ha completado la inscripcion en creintec, para confirmar su registo realize el pago.--}}

    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div id="perfil-por-completar" style="display: none" class="rounded-md px-5 py-4 mb-2 bg-theme-12 text-white">--}}
    {{--                <div class="flex items-center">--}}
    {{--                    <div class="font-medium text-lg">Perfil por completar</div>--}}
    {{--                    <div class="text-xs bg-white px-1 rounded-md text-gray-800 ml-auto">Alerta</div>--}}
    {{--                </div>--}}
    {{--                <div class="mt-3">Por favor completa tu perfil para obtener todos nuestros beneficios.--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    @endif--}}


    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="Tornado Empresa" class="rounded-full"
                             src="{{auth()->user()->empresa->logo_rutararchivo?asset(auth()->user()->empresa->logo_rutararchivo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div
                            class="font-medium text-base">{{ auth()->user()->nombres." ".auth()->user()->apellidos  }}</div>
                        <div class="text-gray-600">{{ auth()->user()->tipo_usuario->descripcion}}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <ul>
                        <li class="flex items-center text-theme-1 dark:text-theme-10 font-medium link-tab cursor-pointer"
                            aria-expanded="empresa">
                            <i data-feather="info" class="w-4 h-4 mr-2"></i> Información de la empresa
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="representante">
                            <i data-feather="user" class="w-4 h-4 mr-2"></i> Representante legal
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="actividad-economica">
                            <i data-feather="bar-chart" class="w-4 h-4 mr-2"></i> Actividad económica
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer" aria-expanded="web">
                            <i data-feather="globe" class="w-4 h-4 mr-2"></i> Imagen corportativa y presencia Web
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="competitividad">
                            <i data-feather="eye" class="w-4 h-4 mr-2"></i> Competitividad
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="capital-humano">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Capital Humano
                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto"></div>
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="expectativas">
                            <i data-feather="eye" class="w-4 h-4 mr-2"></i> Expectativas
                            {{--                            <div class="text-xs text-white px-1 rounded-full bg-theme-6 ml-auto">10</div>--}}
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer hidden" aria-expanded="empresas-ti">
                            <i data-feather="home" class="w-4 h-4 mr-2"></i> Empresas
                            {{--                            <div class="text-xs text-white px-1 rounded-full bg-theme-9 ml-auto">10</div>--}}
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer hidden"
                            aria-expanded="habilidades_ti">
                            <i data-feather="home" class="w-4 h-4 mr-2"></i> Habilidades
                        </li>
                        <li class="flex items-center mt-5 link-tab link-tab cursor-pointer"
                            aria-expanded="presupuesto">
                            <i data-feather="home" class="w-4 h-4 mr-2"></i> Presupuesto de inversion
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
            <div id="empresa" class="intro-y box lg:mt-5 card-fom-empresa ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Informacion De la Empresa</h2>
                </div>
                <div class="p-5">

                    <form id="info-empresa-form">
                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Razon Social</label>
                                <input type="text" id="input-razonsocial" name="razonsocial"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->razonsocial}}">
                                <div id="error-razonsocial"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Nombre Comercial</label>
                                <input type="text" id="input-nombrecomercial" name="nombrecomercial"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->nombrecomercial}}">
                                <div id="error-nombrecomercial"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>NIT</label>
                                <input type="text" id="input-nit" name="nit"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->nit}}">
                                <div id="error-nombrecomercial"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="" id="multi-select">
                                <div class="preview">
                                    <label>Tipo de empresa</label>
                                    <select data-placeholder="Selecciona tu tipo de empresa" data-search="true"
                                            class="mt-2 tail-select w-full" multiple name="tipo_empresas[]"
                                            id="input-tipo_empresas">
                                        @foreach(\App\Models\Tipo_empresa::all() as $data)
                                            @php
                                                $encontrado=0;
                                                foreach(auth()->user()->empresa->tipoEmpresas as $data2){
                                                    if ($data2->id==$data->id){
                                                        $encontrado=1;
                                                        echo '<option selected value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                    }
                                                }
                                                if (!$encontrado){
                                                    echo '<option value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                }
                                            @endphp
                                        @endforeach
                                    </select>
                                    <div id="error-tipo_empresas"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>

                            </div>

                            <div class="">
                                <label>Telefono</label>
                                <input type="text" id="input-telefono" name="telefono"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->telefono}}">
                                <div id="error-telefono" class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Celular</label>
                                <input type="text" id="input-celular" name="celular"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->celular}}">
                                <div id="error-celular" class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                        </div>

                        <div class="grid grid-cols-2 gap-4 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-4">

                            <div class="">
                                <label>Pais</label>
                                <div class="mt-2">
                                    <select data-search="true" class="form-select" id="select-pais">
                                        @foreach(\App\Models\Countries::all() as $pais)
                                            <option value="{{$pais->id}}">{{$pais->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="">
                                <label>Departamento</label>
                                <div class="mt-2">
                                    <select data-search="true" class="form-select" id="select-departamento">
                                    </select>
                                </div>
                            </div>

                            <div class="">
                                <label>Ciudad</label>
                                <div class="mt-2">
                                    <select data-search="true" class="form-select" id="ciudad" name="ciudad">
                                    </select>
                                    <div id="error-ciudad" class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>

                            <div class="">
                                <label>Dirección correspondencia</label>
                                <input type="text" id="input-direccion_correspondencia"
                                       name="direccion_correspondencia"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->direccion_correspondencia}}">
                                <div id="error-direccion_correspondencia"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                        </div>


                        <div class="grid grid-cols-2 gap-4 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Año de creación de al empresa (cámara de comercio)</label>
                                <input type="date" id="input-anno_creacion_empresa" name="anno_creacion_empresa"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->anno_creacion_empresa}}">
                                <div id="error-anno_creacion_empresa"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Numero empleados indirectos</label>
                                <input type="number" id="input-num_empleados_indirectos"
                                       name="num_empleados_indirectos"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_empleados_indirectos}}">
                                <div id="error-num_empleados_indirectos"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Numero de empleados directos</label>
                                <input type="number" id="input-num_empleados_directos" name="num_empleados_directos"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_empleados_directos}}">
                                <div id="error-num_empleados_directos"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">

                                <div class="">
                                    <label>Breve Descripcion de la Empresa</label>
                                    <textarea class="form-control" id="input-breve_descr_empresa"
                                              name="breve_descr_empresa"
                                              placeholder="descripcion">{{auth()->user()->empresa->breve_descr_empresa}}</textarea>
                                    <div id="error-breve_descr_empresa"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>

                            </div>

                            <div class="hidden">

                                <div class="d-none">
                                    <label>Mercado</label>
                                    <textarea rows="5" class="form-control" id="input-mercado"
                                              name="mercado"
                                              placeholder="Realice una breve descripción del mercado actual de la empresa (regional, nacional, internacional, sector a la que pertenece y los principales hitos o logros en el mercado)">{{auth()->user()->empresa->mercado}} default</textarea>
                                    <div id="error-mercado"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>

                            </div>

                            <div class="hidden">

                                <div class="">
                                    <label>Recurso Humano </label>
                                    <textarea rows="5" class="form-control" id="input-recurso_humano"
                                              name="recurso_humano"
                                              placeholder="Realice una descripción de la trayectoria de los integrantes de la propuesta con relación a la temática de la propuesta del proyecto">{{auth()->user()->empresa->recurso_humano}} default</textarea>
                                    <div id="error-recurso_humano"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>

                            </div>

                            <div class="">
                                <div>
                                    <label>RUT (PDF)</label>
                                    <input type="file" id="input-rut_rutaarachivo" name="rut_rutaarachivo"
                                           class="form-control" accept="application/pdf">
                                    <div id="error-rut_rutaarachivo"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                    <a href="javascript:;" id="verRut" class="btn btn-primary" style="display: none;">Ver
                                        Documento</a>
                                </div>

                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                    id="rutExiste">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga el RUT
                                </div>
                            </div>

                            <div class="">
                                <label>Si el archivo RUT tiene contraseña, suministrela</label>
                                <input type="text" id="input-clave_rut" name="clave_rut"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->clave_rut}}">
                                <div id="error-clave_rut"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <div>
                                    <label>Camara de comercio (PDF)</label>
                                    <input type="file" id="input-camara_comercio_rutaarachivo"
                                           name="camara_comercio_rutaarachivo"
                                           class="form-control" accept="application/pdf">
                                    <div id="error-camara_comercio_rutaarachivo"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                    <a href="javascript:;" id="verCamaraComercio" class="btn btn-primary"
                                       style="display: none;">Ver Documento</a>
                                </div>

                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                    id="camaraComercioExiste">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga la camara
                                    de comercio
                                </div>
                            </div>
                        </div>

                        <button type="button" id="btn-guardar-info-empresa"
                                class="btn btn-primary w-20 mt-3">Guardar
                        </button>

                    </form>


                </div>
            </div>
            <div id="representante" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Representante Legal</h2>
                </div>
                <div class="p-5">
                    <form id="representante-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">
                            <div class="">
                                <label>Nombre Completo</label>
                                <input type="text" id="input-nombte_representante_legal"
                                       name="nombte_representante_legal" class="form-control"
                                       value="{{auth()->user()->empresa->nombte_representante_legal}}">
                                <div id="error-nombte_representante_legal"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>
                            <div class="">
                                <label>Numero de Identificacion</label>
                                <input type="text" id="input-identifica_representante_legal"
                                       name="identifica_representante_legal" class="form-control"
                                       value="{{auth()->user()->empresa->identifica_representante_legal}}">
                                <div id="error-identifica_representante_legal"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>
                            <div class="">
                                <label>Numero de Celular</label>
                                <input type="text" id="input-celular_representante_legal"
                                       name="celular_representante_legal" class="form-control"
                                       value="{{auth()->user()->empresa->celular_representante_legal}}">
                                <div id="error-celular_representante_legal"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>
                            <div class="">
                                <label>Tipo Identificacion</label>
                                <select class="form-select" name="tipoid_represen_legal"
                                        id="input-tipoid_represen_legal">
                                    @foreach(\App\Models\Tipo_identificacion::all() as $tipo_identificacion)
                                        <option
                                            {{auth()->user()->empresa->tipoid_represen_legal==$tipo_identificacion->id?'selected':''}} value="{{$tipo_identificacion->id}}">{{$tipo_identificacion->descripcion}}</option>
                                    @endforeach
                                </select>
                                <div id="error-tipoid_represen_legal"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>
                            <div class="">
                                <label>Identificacion (PDF/imagen)</label>
                                <input id="input-identi_rutaarchivo" name="identi_rutaarchivo" type="file"
                                       class="form-control" accept="application/pdf">
                                <a href="javascript:;" id="verIdentificacion" class="btn btn-primary"
                                   style="display: none;">Ver Documento</a>

                                <div id="error-identi_rutaarchivo"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                    id="idRepresentanteExiste">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga la
                                    identificación
                                    del representante legal
                                </div>
                            </div>

                        </div>

                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-guardar-representante" type="button"
                                class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>

            </div>
            <div id="actividad-economica" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Actividad económica</h2>
                </div>
                <div class="p-5">
                    <form id="actividad-economica-form">
                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Sector de la empresa </label>
                                <div class="mt-2" data-placeholder="Seleccione un sector">
                                    <select data-search="true" class="tail-select w-full"
                                            id="input-sector_empresa_id"
                                            name="sector_empresa_id" data-placeholder="Seleccione un sector">
                                        <option disabled selected hidden>Seleccione un sector</option>
                                        @foreach(\App\Models\Sector_empresa::all() as $data)
                                            <option
                                                {{auth()->user()->empresa->sector_empresa_id==$data->id?'selected':''}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-sector_empresa_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>
                            </div>

                            <div class="">
                                <label>Actividad económica pricipal</label>
                                <div class="mt-2">
                                    <select data-placeholder="Selecciona la actividad pricipal" data-search="true" class="tail-select w-full"
                                            id="input-actividad_economica_1_id" name="actividad_economica_1_id">
                                        <option disabled selected hidden>Seleccione una actividad</option>
                                        @foreach(\App\Models\Actividad_economica::all() as $data)
                                            <option
                                                {{auth()->user()->empresa->actividad_economica_1_id==$data->id?'selected':''}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-actividad_economica_1_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>
                            </div>

                            <div class="">
                                <label>Actividad económica secundaria</label>
                                <div class="mt-2">
                                    <select data-search="true" class="tail-select w-full"
                                            id="input-actividad_economica_2_id" name="actividad_economica_2_id"
                                            data-placeholder="Selecciona la actividad secundaria">
                                        <option disabled hidden selected>Selecciona la actividade secundaria</option>
                                        @foreach(\App\Models\Actividad_economica::all() as $data)
                                            <option
                                                {{auth()->user()->empresa->actividad_economica_2_id==$data->id?'selected':''}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-actividad_economica_2_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>
                            </div>

                            <div class="" id="multi-select">
                                <div class="preview">
                                    <label>Actividad económica otros</label>
                                    <select data-placeholder="Selecciona actividades economicas" data-search="true"
                                            class="mt-2 tail-select w-full" multiple name="actividad_economica_otros[]"
                                            id="input-actividad_economica_otros">
                                        @foreach(\App\Models\Actividad_economica::all() as $data)
                                            @php
                                                $encontrado=0;
                                                foreach(auth()->user()->empresa->actividadEconomicaOtros as $data2){
                                                    if ($data2->id==$data->id){
                                                        $encontrado=1;
                                                        echo '<option selected value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                    }
                                                }
                                                if (!$encontrado){
                                                    echo '<option value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                }
                                            @endphp
                                        @endforeach
                                    </select>
                                    <div id="error-sector_negocio"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>

                            <div class="">
                                <label>Comercialización directo o con tercero</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="tipo_comercionalizacion_id" id="input-tipo_comercionalizacion_id">
                                    @foreach(\App\Models\Tipo_comercionalizacion::all() as $data)
                                        <option
                                            {{auth()->user()->empresa->tipo_comercionalizacion_id==$data->id?'selected':''}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                    @endforeach
                                </select>
                                <div id="error-tipo_comercionalizacion_id"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Balance general año anterior (PDF/imagen)</label>
                                <input type="file" class="form-control"
                                       id="input-balance_general_rutaarachivo" name="balance_general_rutaarachivo">
                                <a href="javascript:;" id="verBalance" class="btn btn-primary" style="display: none;">Ver
                                    Documento</a>

                                <div id="error-balance_general_rutaarachivo"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden"
                                    id="contBalance">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga el
                                    balance general del año anterior
                                </div>

                            </div>

                            <div class="">
                                <label>Promedio de ventas anuales (USD)</label>
                                <input type="number" id="input-prome_ventas_anuales" name="prome_ventas_anuales"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->prome_ventas_anuales}}">
                                <div id="error-prome_ventas_anuales"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Tipo de operaciones</label>
                                <div class="mt-2">
                                    <select data-search="false" class="tail-select w-full"
                                            id="input-tipo_operacion_id"
                                            name="tipo_operacion_id">
                                        @foreach(\App\Models\Tipo_operacion::all() as $data)
                                            <option
                                                {{auth()->user()->empresa->tipo_operacion_id==$data->id?'selected':''}} value="{{$data->id}}">{{$data->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    <div id="error-tipo_operacion_id"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>
                            </div>

                            <div class="">
                                <label>Total activos (USD)</label>
                                <input type="number" id="input-total_activos" name="total_activos"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->total_activos}}">
                                <div id="error-total_activos"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Ciudades de otras sedes</label>
                                <input type="text" id="input-ciudades_otras_sedes" name="ciudades_otras_sedes"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->ciudades_otras_sedes}}">
                                <div id="error-ciudades_otras_sedes"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Numero de sedes</label>
                                <input type="text" id="input-num_sedes" name="num_sedes"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_sedes}}">
                                <div id="error-num_sedes" class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>


                        </div>

                    </form>
                    <div class="flex justify-end mt-4">
                        <button id="btn-actividad-economica" type="button"
                                class="btn btn-primary w-20 mt-3">Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="web" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Imagen corportativa y presencia Web</h2>
                </div>
                <div class="p-5">
                    <form id="imagen-corporativa-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Pagina WEB</label>
                                <input type="text" id="input-pagina_web" name="pagina_web"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->pagina_web}}">
                                <div id="error-pagina_web" class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Link Portafolio</label>
                                <input type="text" id="input-link_portafolio" name="link_portafolio"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->link_portafolio}}">
                                <div id="error-link_portafolio"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Link Facebook</label>
                                <input type="text" id="input-link_facebook" name="link_facebook"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->link_facebook}}">
                                <div id="error-link_facebook"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Link Instagram</label>
                                <input type="text" id="input-link_instagram" name="link_instagram"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->link_instagram}}">
                                <div id="error-link_instagram"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Link Linkedin</label>
                                <input type="text" id="input-link_linkedin" name="link_linkedin"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->link_linkedin}}">
                                <div id="error-link_linkedin"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Link Twitter</label>
                                <input type="text" id="input-link_twitter" name="link_twitter"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->link_twitter}}">
                                <div id="error-link_twitter"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Realiza ventas en linea</label>
                                <select class="form-select realiza_ventas_linea"
                                        id="input-realiza_ventas_linea" name="realiza_ventas_linea">
                                    <option
                                        {{auth()->user()->empresa->realiza_ventas_linea=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->realiza_ventas_linea=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-realiza_ventas_linea"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Cuenta con canales virtuales de atención?</label>
                                <select class="form-select tiene_canales_virtuales_atencion"
                                        id="input-tiene_canales_virtuales_atencion"
                                        name="tiene_canales_virtuales_atencion">
                                    <option
                                        {{auth()->user()->empresa->tiene_canales_virtuales_atencion=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->tiene_canales_virtuales_atencion=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-tiene_canales_virtuales_atencion"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="border border-gray-200 dark:border-dark-5 rounded-md p-5">
                                <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                    <img class="rounded-md" id="image_logo"
                                         alt="Midone Tailwind HTML Admin Template"
                                         src="{{auth()->user()->empresa->logo_rutararchivo?asset(auth()->user()->empresa->logo_rutararchivo):asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                                    <div title="Remove this profile photo?" id="remove_logo"
                                         class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                        <i data-feather="x" class="w-4 h-4"></i>
                                    </div>
                                </div>
                                <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                    <button type="button" class="btn btn-primary w-full">Cargar Logo
                                    </button>
                                    <input type="file" name="logo_rutararchivo" id="logo_rutararchivo" accept="image/*"
                                           class="w-full h-full top-0 left-0 absolute opacity-0">
                                </div>


                                <div
                                    class="rounded-md flex items-center px-5 py-4 mb-2 border border-theme-6 text-theme-6 dark:border-theme-6 hidden mt-2"
                                    id="logoEmpresa">
                                    <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Por favor carga el
                                    logo de la empresa
                                </div>

                            </div>

                        </div>


                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-imagen-corporativa" type="button"
                                class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="competitividad" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Competitividad</h2>
                </div>
                <div class="p-5">
                    <form id="competitividad-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Certificación ISO 9001</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="certifica_iso9001"
                                        id="input-certifica_iso9001">
                                    <option
                                        {{auth()->user()->empresa->certifica_iso9001=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->certifica_iso9001=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-certifica_iso9001"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Certificación ISO 14001</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="certifica_iso14001"
                                        id="input-certifica_iso14001">
                                    <option
                                        {{auth()->user()->empresa->certifica_iso14001=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->certifica_iso14001=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-certifica_iso14001"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Otra certificacion internacional</label>
                                <input type="text" id="input-otra_certifica_internacional"
                                       name="otra_certifica_internacional"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->otra_certifica_internacional}}">
                                <div id="error-otra_certifica_internacional"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Uso de energías alternativas</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="uso_energias_alternativas" id="input-uso_energias_alternativas">
                                    <option
                                        {{auth()->user()->empresa->uso_energias_alternativas=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->uso_energias_alternativas=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-uso_energias_alternativas"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Numero de convenios o alianzas estrategicas establecidas</label>
                                <input type="number" id="input-num_convenios_estra_alianza_estable"
                                       name="num_convenios_estra_alianza_estable"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_convenios_estra_alianza_estable}}">
                                <div id="error-num_convenios_estra_alianza_estable"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Presupuesto dedicado a formación del personal (0 - 100%)</label>
                                <input type="number" min="0" max="100" id="input-porce_presupues_info_personal"
                                       name="porce_presupues_info_personal"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->porce_presupues_info_personal}}">
                                <div id="error-porce_presupues_info_personal"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Cuenta con un sistema CRM</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="cuenta_software_crm"
                                        id="input-cuenta_software_crm">
                                    <option
                                        {{auth()->user()->empresa->cuenta_software_crm=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->cuenta_software_crm=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-cuenta_software_crm"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Cuenta con un sistema ERP</label>
                                <select class="form-select realiza_ventas_linea"
                                        name="cuenta_software_erp" id="input-cuenta_software_erp">
                                    <option
                                        {{auth()->user()->empresa->cuenta_software_erp=="Si"?'selected':''}} value="Si">
                                        Si
                                    </option>
                                    <option
                                        {{auth()->user()->empresa->cuenta_software_erp=="No"?'selected':''}} value="No">
                                        No
                                    </option>
                                </select>
                                <div id="error-cuenta_software_erp"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>


                        </div>

                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-competitividad" type="button" class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="capital-humano" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Capital Humano</h2>
                </div>
                <div class="p-5">
                    <form id="capitalHumano-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="">
                                <label>Numero personal con dominio segunda lengua (directos+indirectos)</label>
                                <input type="number" id="input-num_persona_dominio_segunda_lengua"
                                       name="num_persona_dominio_segunda_lengua"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_persona_dominio_segunda_lengua}}">
                                <div id="error-num_persona_dominio_segunda_lengua"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Número mujeres empleados (directos+indirectos)</label>
                                <input type="number" id="input-num_mujeres_empleadas" name="num_mujeres_empleadas"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_mujeres_empleadas}}">
                                <div id="error-num_mujeres_empleadas"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Número personal con posgrado (directos+indirectos)</label>
                                <input type="number" id="input-num_empleados_con_postgrado"
                                       name="num_empleados_con_postgrado"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_empleados_con_postgrado}}">
                                <div id="error-num_empleados_con_postgrado"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Edad promedio personal (directo+indirectos)</label>
                                <input type="number" id="input-edad_promedio_empleados"
                                       name="edad_promedio_empleados"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->edad_promedio_empleados}}">
                                <div id="error-edad_promedio_empleados"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Número de madres cabeza de hogar</label>
                                <input type="number" id="input-num_madres_cabeza_hogar"
                                       name="num_madres_cabeza_hogar"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_madres_cabeza_hogar}}">
                                <div id="error-num_madres_cabeza_hogar"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                            <div class="">
                                <label>Número empleados con habilidades en TI</label>
                                <input type="number" id="input-num_empleados_habilidades_ti"
                                       name="num_empleados_habilidades_ti"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_empleados_habilidades_ti}}">
                                <div id="error-num_empleados_habilidades_ti"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                            <div class="">
                                <label>Número de empleados de la región</label>
                                <input type="number" id="input-num_empleados_region" name="num_empleados_region"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->num_empleados_region}}">
                                <div id="error-num_empleados_region"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                            </div>

                        </div>
                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-capitalHumano" type="button" class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="expectativas" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Expectativas</h2>
                </div>
                <div class="p-5">
                    <form id="expectativas-form">


                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="form-check mr-2 mt-3">

                                <input id="input-ingremento_ventas" name="ingremento_ventas"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->ingremento_ventas?'checked':''}}>
                                <label class="form-check-label" for="input-ingremento_ventas">Incremento de
                                    ventas</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-desarrollo_nuevos_canales" name="desarrollo_nuevos_canales"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->desarrollo_nuevos_canales?'checked':''}}>
                                <label class="form-check-label" for="input-desarrollo_nuevos_canales">Desarrollo de
                                    nuevos canales de comunicación
                                </label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-llegar_nuevos_mercados" name="llegar_nuevos_mercados"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->llegar_nuevos_mercados?'checked':''}}>
                                <label class="form-check-label" for="input-llegar_nuevos_mercados">Llegar a nuevos
                                    mercados</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-implementacion_ti" name="implementacion_ti"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->implementacion_ti?'checked':''}}>
                                <label class="form-check-label" for="input-implementacion_ti">Implementación de
                                    TI</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-lanzamiento_nuevos_productos" name="lanzamiento_nuevos_productos"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->lanzamiento_nuevos_productos?'checked':''}}>
                                <label class="form-check-label" for="input-lanzamiento_nuevos_productos">Lanzamiento
                                    de
                                    nuevos productos</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-infraestructura_fisica" name="infraestructura_fisica"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->infraestructura_fisica?'checked':''}}>
                                <label class="form-check-label" for="input-infraestructura_fisica">Insfraestructura
                                    física</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-mejoramiento_productividad" name="mejoramiento_productividad"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->mejoramiento_productividad?'checked':''}}>
                                <label class="form-check-label" for="input-mejoramiento_productividad">Mejoramiento
                                    de
                                    la productividad</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-compra_maquinaria_equipos" name="compra_maquinaria_equipos"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->compra_maquinaria_equipos?'checked':''}}>
                                <label class="form-check-label" for="input-compra_maquinaria_equipos">Compra de
                                    maquinaria y equipos</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->incremento_capacidad_productiva?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Incremento
                                    de la capacidad productiva</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->diversificacion_portafolio?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Diversificación de portafolio de inversión</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->transformacion_modelo_negocio?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Transformación el modelo de negocio</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->invertir_nuevos_negocios?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Invertir en nuevos negocios</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->apoyar_emprendimiento?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Apoyar/invertir en emprendimiento</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->transformar_digital_empresa?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Transformar digitalmente la empresa</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->adoptar_tecnologias?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Adoptar tecnologías de 4 y 5 Revolución Industrial</label>
                            </div>

                            <div class="form-check mr-2 mt-3">
                                <input id="input-incremento_capacidad_productiva"
                                       name="incremento_capacidad_productiva"
                                       class="form-check-input"
                                       type="checkbox"
                                    {{auth()->user()->empresa->transformar_modelo_base_tecnologica?'checked':''}}>
                                <label class="form-check-label" for="input-incremento_capacidad_productiva">Trasformar el modelo de negocio a base tecnológico. </label>
                            </div>

                            <div class="">
                                <label>Otros</label>
                                <input type="text" id="input-otros_expectativas"
                                       name="otros_expectativas"
                                       class="form-control"
                                       value="{{auth()->user()->empresa->otros_expectativas}}">
                                <div id="error-otros_expectativas"
                                     class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                            </div>

                        </div>
                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-expectativas" type="button" class="btn btn-primary w-20 mt-3">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="empresas-ti" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Empresas</h2>
                </div>
                <div class="p-5">
                    <form id="empresasTI-form">

                        <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2">

                            <div class="" id="multi-select">
                                <div class="preview">
                                    <label>Servicios ofertados</label>
                                    <select data-placeholder="Selecciona tus servicios" data-search="true"
                                            class="mt-2 tail-select w-full" multiple name="servicios_ofertados[]"
                                            id="input-servicios_ofertados">
                                        @foreach(\App\Models\Servicio::all() as $data)
                                            @php
                                                $encontrado=0;
                                                foreach(auth()->user()->empresa->servicios_ofertados as $data2){
                                                    if ($data2->id==$data->id){
                                                        $encontrado=1;
                                                        echo '<option selected value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                    }
                                                }
                                                if (!$encontrado){
                                                    echo '<option value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                }
                                            @endphp
                                        @endforeach
                                    </select>
                                    <div id="error-servicios_ofertados"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>
                                </div>

                            </div>

                            <div class="" id="multi-select">
                                <div class="preview">
                                    <label>Sector del negocio</label>
                                    <select data-placeholder="Selecciona tu sector" data-search="true"
                                            class="mt-2 tail-select w-full" multiple name="sector_negocio[]"
                                            id="input-sector_negocio">
                                        @foreach(\App\Models\Sector::all() as $data)
                                            @php
                                                $encontrado=0;
                                                foreach(auth()->user()->empresa->sector_negocios as $data2){
                                                    if ($data2->id==$data->id){
                                                        $encontrado=1;
                                                        echo '<option selected value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                    }
                                                }
                                                if (!$encontrado){
                                                    echo '<option value="'.$data->id.'">'.$data->descripcion.'</option>';
                                                }
                                            @endphp
                                        @endforeach
                                    </select>
                                    <div id="error-sector_negocio"
                                         class="login__input-error w-5/6 text-theme-6 mt-2"></div>

                                </div>

                            </div>





                        </div>

                    </form>
                    <div class="flex justify-start mt-4">
                        <button id="btn-empresasTI" type="button" class="btn btn-primary w-20 mt-3">Guardar
                        </button>
                    </div>
                </div>
            </div>
            <div id="habilidades_ti" class="intro-y lg:mt-5 card-fom-empresa hidden ">
                <div class="box">
                    <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Habilidades</h2>
                        <button class="btn btn-primary mr-1 mb-2"
                                onclick="agregarHabilidad()">
                            <span class="w-5 h-5 flex items-center justify-center">
                                <i data-feather="plus" class="w-4 h-4"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="grid grid-cols-2 gap-3 sm:grid-cols-1 md:grid-cols-2 pt-3" id="containerHabilidades">
                    @foreach(auth()->user()->empresa->habilidades as $habilidad)
                        <div class="" id="habilidad-{{$habilidad->id}}">
                            <div class="box">
                                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                    <h2 class="font-medium text-base mr-auto">{{$habilidad->nivel_habilidad->habilidad->descripcion}}</h2>
                                    <button onclick="eliminarHabilidad({{$habilidad->id}})">
                                        <div
                                            class="w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                 stroke-width="1.5"
                                                 stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>
                                        </div>
                                    </button>
                                </div>

                                <div class="px-3 sm:px-3 py-3 sm:py-3">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td class="border-b dark:border-dark-5">
                                                    <div class="font-medium whitespace-nowrap">
                                                        Nivel de Habilidad: <span
                                                            class="text-gray-600 text-xs whitespace-nowrap">{{$habilidad->nivel_habilidad->descripcion}}</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div id="presupuesto" class="intro-y box lg:mt-5 card-fom-empresa hidden ">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Presupuesto de inversion empresarial</h2>
                </div>
                <div class="p-5">
                        <div class="grid grid-cols-12 gap-3 sm:grid-cols-1 md:grid-cols-1">
                            <table class="table">
                                <thead>
                                <tr class="bg-gray-700 dark:bg-dark-1 text-white">
                                    <th class="whitespace-nowrap">Año</th>
                                    <th class="whitespace-nowrap">Especies</th>
                                    <th class="whitespace-nowrap">Efectivo</th>
                                    <th class="whitespace-nowrap">Total</th>
                                    <th class="whitespace-nowrap">Opcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(auth()->user()->empresa->presupuestos as $presupuesto)
                                    <tr>
                                        <td class="border-b dark:border-dark-5">{{$presupuesto->anio}}</td>
                                        <td id="especie-{{$presupuesto->anio}}"
                                            class="border-b dark:border-dark-5">{{$presupuesto->especie}}</td>
                                        <td id="efectivo-{{$presupuesto->anio}}"
                                            class="border-b dark:border-dark-5">{{$presupuesto->efectivo}}</td>
                                        <td id="total-{{$presupuesto->anio}}"
                                            class="border-b dark:border-dark-5">{{$presupuesto->especie+$presupuesto->efectivo}}</td>
                                        <td class="border-b dark:border-dark-5">
                                            <button  class="btn btn-primary mr-1 mb-2" onclick="editarPresupuesto({{$presupuesto->anio}})"
                                                       data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"
                                                       style="margin-right: 5px;">
                                                       <i class="fas fa-pen"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END: Modal Toggle -->
    <!-- BEGIN: Modal Content -->
    <div id="modal-visor" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-10 text-center">
                    <p id="modal-mensaje"></p>
                    <iframe width="100%" height="600px" id="visor-documental">
                    </iframe>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>
    <script>
        cash(function () {
            cash('.link-tab').on('click', function () {

                let divs = document.getElementsByClassName("link-tab");
                let numDivs = divs.length;
                for (let i = 0; i < numDivs; i++) {
                    divs[i].classList.remove("text-theme-1", "dark:text-theme-10", "font-medium");
                }

                this.classList.add("text-theme-1", "dark:text-theme-10", "font-medium");

                let divs2 = document.getElementsByClassName("card-fom-empresa");
                let numDivs2 = divs2.length;
                for (let i = 0; i < numDivs2; i++) {
                    divs2[i].classList.add("hidden");
                }
                document.getElementById(this.getAttribute('aria-expanded')).classList.remove("hidden")

            })

            async function guardarInfoEmpresa() {
                // Reset state
                cash('#info-empresa-form').find('.input').removeClass('border-theme-6')
                cash('#info-empresa-form').find('.login__input-error').html('')

                // Loading state
                cash('#btn-guardar-info-empresa').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1500)
                let formData = new FormData(cash("#info-empresa-form")[0]);

                //formData.append("breve_descr_empresa",cash('#input-breve_descr_empresa').val());
                //formData.append("ciudad",cash('#ciudad').val());
                //formData.append("rut_rutaarachivo",cash('#input-rut_rutaarachivo')[0]);

                axios.post(`actualizarInformacionDeLaEmpresa`,
                    formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(res => {
                    //location.href = '/'
                    Swal.fire({
                        icon: 'success',
                        title: 'Informacion guardada con exito',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    cash('#btn-guardar-info-empresa').html('Guardar')
                }).catch(err => {
                    cash('#btn-guardar-info-empresa').html('Guardar')
                    if (err.response.data.message != 'Wrong email or password.') {
                        for (const [key, val] of Object.entries(err.response.data.errors)) {
                            cash(`#input-${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                    } else {
                        cash(`#input-password`).addClass('border-theme-6')
                        cash(`#error-password`).html(err.response.data.message)
                    }
                })
            }

            async function guardar(btn, form, url) {
                // Reset state
                cash(form).find('.input').removeClass('border-theme-6')
                cash(form).find('.login__input-error').html('')
                // Loading state
                cash(btn).html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1500)
                let formData = new FormData(cash(form)[0]);
                axios.post(url,
                    formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
                ).then(res => {
                    //location.href = '/'
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha actualizado la informacion con exito',
                        showConfirmButton: false,
                        timer: 3000
                    })
                    getContadoresEmpresa()
                    cash(btn).html('Guardar')
                }).catch(err => {
                    cash(btn).html('Guardar')
                    if (err.response.data.message != 'Wrong email or password.') {
                        for (const [key, val] of Object.entries(err.response.data.errors)) {
                            cash(`#input-${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                    } else {
                        cash(`#input-password`).addClass('border-theme-6')
                        cash(`#error-password`).html(err.response.data.message)
                    }
                })
            }

            cash('#btn-guardar-info-empresa').on('click', function () {
                //guardarInfoEmpresa()
                guardar('#btn-guardar-info-empresa', '#info-empresa-form', 'actualizarInformacionDeLaEmpresa')

            })
            cash('#btn-guardar-representante').on('click', function () {
                guardar('#btn-guardar-representante', '#representante-form', 'actualizarRepresentanteLegal')
            })
            cash('#btn-actividad-economica').on('click', function () {
                guardar('#btn-actividad-economica', '#actividad-economica-form', 'actualizarActividadEconomica')
            })
            cash('#btn-imagen-corporativa').on('click', function () {
                guardar('#btn-imagen-corporativa', '#imagen-corporativa-form', 'actualizarImagenCorporativayWeb')
            })
            cash('#btn-competitividad').on('click', function () {
                guardar('#btn-competitividad', '#competitividad-form', 'actualizarCompetitividad')
            })
            cash('#btn-capitalHumano').on('click', function () {
                guardar('#btn-capitalHumano', '#capitalHumano-form', 'actualizarCapitalHumano')
            })
            cash('#btn-expectativas').on('click', function () {
                guardar('#btn-expectativas', '#expectativas-form', 'actualizarExpectativas')
            })
            cash('#btn-empresasTI').on('click', function () {
                guardar('#btn-empresasTI', '#empresasTI-form', 'actualizarEmpresasTI')
            })

            cash('#verRut').on('click', function (e) {
                axios.post(`{{url('getContadoresEmpresa')}}`
                ).then(function (res) {
                    //console.log(res.data['empresa_data'].rut_rutaarachivo);
                    if (res.data['empresa_data'].rut_rutaarachivo == "" || res.data['empresa_data'].rut_rutaarachivo == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['empresa_data'].rut_rutaarachivo);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });
            cash('#verCamaraComercio').on('click', function (e) {
                axios.post(`{{url('getContadoresEmpresa')}}`
                ).then(function (res) {
                    //console.log(res.data['empresa_data'].camara_comercio_rutaarachivo);
                    if (res.data['empresa_data'].camara_comercio_rutaarachivo == "" || res.data['empresa_data'].camara_comercio_rutaarachivo == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['empresa_data'].camara_comercio_rutaarachivo);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });
            cash('#verIdentificacion').on('click', function (e) {
                axios.post(`{{url('getContadoresEmpresa')}}`
                ).then(function (res) {
                    ////console.log(res.data['empresa_data'].identi_rutaarchivo);
                    if (res.data['empresa_data'].identi_rutaarchivo == "" || res.data['empresa_data'].identi_rutaarchivo == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['empresa_data'].identi_rutaarchivo);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });
            cash('#verBalance').on('click', function (e) {
                axios.post(`{{url('getContadoresEmpresa')}}`
                ).then(function (res) {
                    //console.log(res.data['empresa_data'].balance_general_rutaarachivo);
                    if (res.data['empresa_data'].balance_general_rutaarachivo == "" || res.data['empresa_data'].balance_general_rutaarachivo == null) {
                        cash("#modal-mensaje").html('No se encontro un documento asociado');
                        cash("#visor-documental").hide();
                    } else {
                        cash("#modal-mensaje").html('');
                        loadIframe('visor-documental', '/' + res.data['empresa_data'].balance_general_rutaarachivo);
                        cash("#visor-documental").show();
                    }
                    cash("#modal-visor").modal("show");
                });
            });
        })

        $('#select-pais').on('change', function () {
            $("#select-departamento").empty();
            let countryID = $(this).val();
            let departamentos = [];
            if (countryID) {
                axios.post(`{{url('get-states-by-country')}}`, {
                    country_id: countryID
                }).then(function (res) {
                    //console.log(res.data);
                    if (res) {
                        for (let item in res.data.states) {
                            let itemSelect2 = {
                                id: res.data.states[item].id,
                                text: res.data.states[item].name
                            };
                            departamentos.push(itemSelect2)
                        }
                        $('#select-departamento').select2({
                            allowClear: true,
                            placeholder: "Selecciona una Departamento...",
                            data: departamentos,
                        });
                        $('#select-departamento').val({{auth()->user()->empresa->city_id?auth()->user()->empresa->ciudad->departamento->id:'788'}}).trigger('change');


                    } else {
                        $("#select-departamento").empty();
                    }
                }).catch(e => {
                    //console.log(e);
                })
            } else {
                $("#select-departamento").empty();
                $("#ciudad").empty();
            }
        })

        $('#select-departamento').on('change', function () {
            $("#ciudad").empty();
            let stateID = $(this).val();
            let ciudad = [];
            if (stateID) {
                axios.post(`{{url('get-cities-by-state')}}`, {
                    state_id: stateID
                }).then(function (res) {
                    //console.log(res.data);
                    if (res) {
                        for (let item in res.data.cities) {
                            let itemSelect2 = {
                                id: res.data.cities[item].id,
                                text: res.data.cities[item].name
                            };
                            ciudad.push(itemSelect2)
                        }
                        $('#ciudad').select2({
                            allowClear: true,
                            placeholder: "Selecciona una Ciudad...",
                            data: ciudad
                        });
                        $('#ciudad').val({{auth()->user()->empresa->city_id?auth()->user()->empresa->ciudad->id:'13029'}}).trigger('change');

                    } else {
                        $("#ciudad").empty();
                    }
                }).catch(e => {
                    //console.log(e);
                })
            } else {
                $("#ciudad").empty();
            }
        })

        $('#remove_logo').on('click', function (e) {
            let image = document.getElementById('image_logo')
            image.src = '{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}';
            $('#logo_rutararchivo').trigger("change");
        });

        $('#logo_rutararchivo').on('change', function (e) {
            // Creamos el objeto de la clase FileReader
            let reader = new FileReader();
            // Leemos el archivo subido y se lo pasamos a nuestro fileReader
            reader.readAsDataURL(e.target.files[0]);
            // Le decimos que cuando este listo ejecute el código interno
            reader.onload = function () {
                let image = document.getElementById('image_logo')
                image.src = reader.result;
            };
        })

        function loadIframe(iframeName, url) {
            var $iframe = $('#' + iframeName);
            if ($iframe.length) {
                $iframe.attr('src', url);
                return false;
            }
            return true;
        }

        function getContadoresEmpresa() {
            var resultado = 0;
            axios.post(`{{url('getContadoresEmpresa')}}`
            ).then(function (res) {

                res.data['rut'] >= 1 ? $("#rutExiste").hide() : "";
                res.data['camaraComercio'] >= 1 ? $("#camaraComercioExiste").hide() : "";
                res.data['rut'] >= 1 ? $("#verRut").hide() : $("#verRut").show();
                res.data['camaraComercio'] >= 1 ? $("#verCamaraComercio").hide() : $("#verCamaraComercio").show();

                res.data['idRepresentante'] >= 1 ? $("#idRepresentanteExiste").removeClass("hidden") : "";
                res.data['idRepresentante'] >= 1 ? $("#verIdentificacion").hide() : $("#verIdentificacion").show();

                res.data['contBalance'] >= 1 ? $("#contBalance").removeClass("hidden") : "";
                res.data['contBalance'] >= 1 ? $("#verBalance").hide() : $("#verBalance").show();

                res.data['contLogo'] >= 1 ? $("#logoEmpresa").removeClass("hidden") : "";

                for (var item in res.data) {

                    let tabs = document.getElementsByClassName("link-tab");
                    let numDivs = tabs.length;
                    for (let j = 0; j < numDivs; j++) {

                        if (tabs[j].getAttribute('aria-expanded') === item) {
                            //console.log(tabs[j].getAttribute('aria-expanded') + "-" + item + " - " + res.data[item])
                            resultado += res.data[item];
                            if (res.data[item] === 0) {
                                tabs[j].removeChild(tabs[j].lastElementChild);
                            } else {
                                tabs[j].lastElementChild.innerHTML = res.data[item]
                            }
                        }
                    }

                }
                if (resultado) {
                    //$("#perfil-completado").hide();
                    //$("#perfil-por-completar").show();
                } else {
                    //$("#perfil-completado").show();
                    //$("#perfil-por-completar").hide();
                    //generar reference boton de pago//
                }

            });
        }

        function pagarWompi() {
            axios.post(`{{url('getInfoPago')}}`
            ).then(function (res) {
                //console.log(res);
                //console.log(res.data);
                var checkout = new WidgetCheckout({
                    currency: 'COP',
                    amountInCents: res.data['precio'],//50000000,
                    reference: res.data['referencia'],//generada por la plataforma
                    publicKey: res.data['publicKey'],
                    //redirectUrl: 'https://transaction-redirect.wompi.co/check' // Opcional
                })
                checkout.open(function (result) {
                    var transaction = result.transaction
                    //console.log('Transaction ID: ', transaction.id)
                    //console.log('Transaction object: ', transaction)
                    if (transaction.status == "APPROVED") {
                        location.reload();
                    }
                    //location.reload();
                })
            });
        }

        /********************/
        function agregarHabilidad() {
            Swal.fire({
                title: 'Habilidades',
                html:
                    '<select id="select-habilidad" class="input w-full border form-select">' +
                    '<option selected disabled>Habilidad</option>' +
                    @foreach(\App\Models\Habilidades::all() as $habilidad)
                        '<option value="{{$habilidad->id}}">{{$habilidad->descripcion}}</option>' +
                    @endforeach
                        '</select><br><br>' +
                    '<select id="select-nivel-habilidad" class="input w-full border form-select">' +
                    '<option selected disabled>Seleccione un nivel</option>' +
                    '<option></option>' +
                    '</select>' +
                    '',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {
                    $('#select-habilidad').on('change', function () {
                        $("#select-nivel-habilidad").empty();
                        $('#select-nivel-habilidad').prepend('<option selected disabled>Seleccione un nivel</option>');
                        let habilidad = $(this).val();
                        let subArea = [];
                        if (habilidad) {
                            axios.post(`{{url('getNivelesHabilidad')}}`, {
                                habilidad_id: habilidad
                            }).then(function (res) {
                                // console.log(res.data);
                                if (res) {
                                    for (let item in res.data.nivel) {
                                        let $option = $('<option />', {
                                            text: res.data.nivel[item].descripcion,
                                            value: res.data.nivel[item].id,
                                        });
                                        $('#select-nivel-habilidad').append($option);
                                    }
                                } else {
                                    $("#select-nivel-habilidad").empty();
                                }
                            }).catch(e => {
                                //console.log(e);
                            })
                        } else {
                            $("#select-nivel-habilidad").empty();
                        }
                    })
                },
                preConfirm: () => {
                    if ($('#select-nivel-habilidad').val() === "") {
                        Swal.showValidationMessage(
                            `Seleccione una habilidad y su nivel`
                        )
                        return;
                    }
                    //ajax
                    let nivelHabilidad = $('#select-nivel-habilidad').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('agregarHabilidadEmpresa')}}`, {
                        nivel_habilidad_id: nivelHabilidad,
                    }).then(function (res) {
                        //console.log(res.data);
                        //console.log(res.data['actualizar'] == true);
                        //console.log(res.data['actualizar']);
                        if (res.data['actualizar'] == true) {
                            $("#habilidad-" + res.data['habilidad'].id).remove();
                        }
                        $('#containerHabilidades').append("" +
                            "<div class='' id='habilidad-" + res.data['habilidad'].id + "'>" +
                            "<div class='box'>" +
                            "<div class='flex items-center p-5 border-b border-gray-200 dark:border-dark-5'>" +
                            "<h2 class='font-medium text-base mr-auto'>" + res.data['titulo'] + "</h2>" +
                            "<button onclick='eliminarHabilidad(" + res.data['habilidad'].id + ")'>" +
                            "<div class='w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2'>" +
                            "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-x w-4 h-4'> <line x1='18' y1='6' x2='6' y2='18'></line> <line x1='6' y1='6' x2='18' y2='18'></line> </svg>" +
                            "</div> </button></div>" +
                            "<div class='px-3 sm:px-3 py-3 sm:py-3'> <div class='overflow-x-auto'> <table class='table'>" +
                            "<tbody> " +
                            "<tr> " +
                            "<td class='border-b dark:border-dark-5'> <div class='font-medium whitespace-nowrap'> Nivel de Habilidad: <span class='text-gray-600 text-xs whitespace-nowrap'>" + res.data['nivel'] + "</span> </div></td>" +
                            "</tr>" +
                            "</tbody></table></div></div>");

                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se agrego la experiencia laboral correctamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        //console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                //console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        function eliminarHabilidad(id) {
            Swal.fire({
                title: 'Por favor espera !',
                html: 'Eliminando habilidad..',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });
            axios.post(`{{url('eliminarHabilidadEmpresa')}}`, {
                idHabilidad: id
            }).then(function (res) {
                //console.log(res.data);
                $("#habilidad-" + id).remove();
                swal.close()
            }).catch(e => {
                //console.log(e);
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Ups!',
                    text: "Ha ocurrido un error intente nuevamente",
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            })
        }

        /****************************/
        function editarPresupuesto(anio) {
            Swal.fire({
                title: 'Presupuesto año ' + anio,
                html:
                    '<label class="w-full" style="float: left">Valor en especie</label><br>' +
                    '<input id="especie" type="number" class="form-control" value="0"><br>' +
                    '<label class="w-full" style="float: left">Valor en efectivo</label><br>' +
                    '<input id="efectivo" type="number" class="form-control" value="0"><br>',
                focusConfirm: false,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                onOpen: () => {

                },
                preConfirm: () => {
                    if ($('#especie').val() === "") {
                        Swal.showValidationMessage(
                            `escriba el valor de especie`
                        )
                        return;
                    }
                    if ($('#efectivo').val() === "") {
                        Swal.showValidationMessage(
                            `escriba el valor del efectivo`
                        )
                        return;
                    }
                    //ajax
                    let especie = $('#especie').val();
                    let efectivo = $('#efectivo').val();
                    Swal.fire({
                        title: 'Por favor espera !',
                        html: 'Validando informacion..',// add html attribute if you want or remove
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                        },
                    });
                    axios.post(`{{url('actualizarPresupuesto')}}`, {
                        anio: anio,
                        especie: especie,
                        efectivo: efectivo,
                    }).then(function (res) {
                        //console.log(res.data);
                        //console.log(res.data['actualizar']);
                        $("#especie-" + anio).html(res.data['presupuesto']['especie']);
                        $("#efectivo-" + anio).html(res.data['presupuesto']['efectivo']);
                        $("#total-" + anio).html(res.data['total']);
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Bien!',
                            text: "Se actualizo el presupuesto de año " + anio,
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                        return true;
                    }).catch(e => {
                        //console.log(e);
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Ups!',
                            text: "Ha ocurrido un error intente nuevamente",
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        });
                    })

                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                //console.log(result);
                if (result.value) {
                    Swal.fire({
                        title: `Se ha asignado correctamente la atencion`,
                    });
                } else if (!result.dismiss) {
                    Swal.fire({
                        title: `Lo sentimos pero no se pudo procesar la solicitud`,
                    })
                }
            });
        }

        $(document).ready(function () {
            getContadoresEmpresa()

            $('#select-pais').select2({
                allowClear: true,
                placeholder: "Selecciona un Pais...",
            });
            $('#select-pais').val({{auth()->user()->empresa->city_id?auth()->user()->empresa->ciudad->departamento->pais->id:'47'}}).trigger('change');

            $('#select-departamento').select2({
                allowClear: true,
                placeholder: "Selecciona un Departamento...",
                data: []
            });
            $('#select-departamento').val({{auth()->user()->empresa->city_id?auth()->user()->empresa->ciudad->departamento->id:'788'}}).trigger('change');
            $('#ciudad').select2(
                {
                    allowClear: true,
                    placeholder: "Selecciona una Ciudad...",
                    data: []
                }
            );
            $('#ciudad').val({{auth()->user()->empresa->city_id?auth()->user()->empresa->ciudad->id:'13029'}}).trigger('change');

            // contar();

        });
    </script>
@endsection
