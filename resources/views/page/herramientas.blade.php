@extends('../layout/' . $layout)
@section('subhead')
    <title>{{ucwords(Route::current()->getName())}} - {{ ucwords(config('app.name'))}}</title>
@endsection
@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ucwords(Route::current()->getName())}}</h2>
    </div>
    <div class="pos intro-y p-5 mt-5">
        <div class="grid grid-cols-12 gap-6 mt-5">
            @foreach ($herramientas as $herramienta)
                <a href="{{$herramienta->url}}" target="_blank" class="intro-y block col-span-12 sm:col-span-3 xxl:col-span-3">
                    <div class="box rounded-md p-3 relative zoom-in">
                        <div class="flex-none pos-image relative block">
                            <div class="pos-image__preview image-fit">
                                <img alt="herramienta"
                                     src="{{ asset('dist/images/' . $herramienta->imagen) }}">
                            </div>
                        </div>
                        <div class="block font-medium text-center truncate my-3">{{ $herramienta->titulo }}</div>
                        <div class="block font-normal truncate my-3">{{ $herramienta->descripcion }}</div>
                        <bottom target="_blank" href="{{$herramienta->url}}" class="btn btn-primary w-32 shadow-md w-full">Ver</bottom>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
@section('script')
@endsection
