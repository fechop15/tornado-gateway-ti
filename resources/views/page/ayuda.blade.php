@extends('../layout/' . $layout)

@section('subhead')
    <title>Ayuda - GATEWAYTI</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Ayuda</h2>
    </div>
    <div class="intro-y grid grid-cols-12 gap-6 mt-5">
        <!-- BEGIN: Basic Accordion -->
        <div class="col-span-12 lg:col-span-12">
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Ayuda de Tornado Digital</h2>
                </div>
                <div id="basic-accordion" class="p-5">
                    <div class="preview">
                        <div id="faq-accordion-1" class="accordion">
                            <div class="accordion-item">
                                <div id="faq-accordion-content-1" class="accordion-header">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#faq-accordion-collapse-1" aria-expanded="true"
                                            aria-controls="faq-accordion-collapse-1">
                                        Politica de datos
                                    </button>
                                </div>
                                <div id="faq-accordion-collapse-1" class="accordion-collapse collapse show"
                                     aria-labelledby="faq-accordion-content-1" data-bs-parent="#faq-accordion-1">
                                    <div class="accordion-body text-gray-700 dark:text-gray-600 leading-relaxed">
                                        Condiciones del Servicio Adicionales de Tornado Digital y el Sistema de información Tornado Digital (R)<br><br>
                                        Última modificación: 25 de abrir de 2021<br><br>
                                        Al usar Tornado Digital (R), confirma que acepta las Condiciones del Servicio de Gateway TI (que se encuentran en su contrato) y las Condiciones del Servicio Adicionales de Tornado Digital (R).
                                        <br><br>
                                        Estas Condiciones del Servicio Adicionales de Tornado digital (R) se aplicarán a la versión de código ejecutable y web de Tornado Digital (R) . El código fuente de Tornado Digital (R) no está disponible de forma gratuita de conformidad con acuerdos de licencia de software tipo comercial.
                                        <br><br>
                                        El uso que usted haga del sistema de información Tornado Digital(R) también está sujeto a las condiciones y regulaciones de Software y datos de la ley Colombiana.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div id="faq-accordion-content-2" class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#faq-accordion-collapse-2" aria-expanded="false"
                                            aria-controls="faq-accordion-collapse-2">
                                        Contacto
                                    </button>
                                </div>
                                <div id="faq-accordion-collapse-2" class="accordion-collapse collapse"
                                     aria-labelledby="faq-accordion-content-2" data-bs-parent="#faq-accordion-1">
                                    <div class="accordion-body text-gray-700 dark:text-gray-600 leading-relaxed">
                                        --------
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <div id="faq-accordion-content-3" class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#faq-accordion-collapse-3" aria-expanded="false"
                                            aria-controls="faq-accordion-collapse-3">
                                        Extra
                                    </button>
                                </div>
                                <div id="faq-accordion-collapse-3" class="accordion-collapse collapse"
                                     aria-labelledby="faq-accordion-content-3" data-bs-parent="#faq-accordion-1">
                                    <div class="accordion-body text-gray-700 dark:text-gray-600 leading-relaxed">
                                        Proximamente...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Basic Accordion -->
    </div>
@endsection
