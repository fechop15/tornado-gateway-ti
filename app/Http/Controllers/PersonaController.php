<?php

namespace App\Http\Controllers;

use App\Models\EducacionNoFormal;
use App\Models\EstudiosFormales;
use App\Models\ExperienciaPersonal;
use App\Models\Habilidades;
use App\Models\NivelHabilidad;
use App\Models\PersonaHabilidades;
use App\Models\PersonaIdioma;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PersonaController extends Controller
{
    //
    public function actualizarDatosPersonales(Request $request)
    {
        $request->validate([
            'nombres' => 'required|string|max:255',
            'apellidos' => 'required|string|max:255',
            'identificacion' => 'required|string|max:255',
            'tipoid' => 'required|exists:tipo_identificacions,id',
            'fecha_nacimiento' => 'required|date|max:255',
            'ciudad' => 'required|exists:cities,id',
            'rh_sanguineo_id' => 'required|exists:rh_sanguineos,id',
            'estado_civil_id' => 'required|exists:estado_civils,id',
            'direccion_recidencia' => 'required|string|max:255',
            'celular' => 'required|string|max:255',
            'link_instagram' => 'max:255',
            'link_linkedin' => 'max:255',
            'link_twitter' => 'max:255',
            'link_facebook' => 'max:255',
            'identificacion_rurataarchivo' => 'file|mimes:pdf',
            'logo_rutararchivo' => 'file|mimes:png,jpg',
        ]);

        $usuario = auth()->user();
        $usuario->nombres = strtoupper($request->nombres);
        $usuario->apellidos = strtoupper($request->apellidos);
        $usuario->telefono = strtoupper($request->celular);
        $usuario->save();

        $path = $this->pathDocumentos();
        if ($request->identificacion_rurataarchivo) {
            move_uploaded_file($request->file('identificacion_rurataarchivo'), $path . 'identificacion.' . $request->identificacion_rurataarchivo->getClientOriginalExtension());
            auth()->user()->persona->identificacion_rurataarchivo = $path . 'identificacion.' . $request->identificacion_rurataarchivo->getClientOriginalExtension();
            auth()->user()->persona->save();
        }
        if ($request->logo_rutararchivo) {
            move_uploaded_file($request->file('logo_rutararchivo'), $path . 'foto.' . $request->logo_rutararchivo->getClientOriginalExtension());
            auth()->user()->photo = $path . 'foto.' . $request->logo_rutararchivo->getClientOriginalExtension();
            auth()->user()->save();
        }

        auth()->user()->persona->update([
            'identificacion' => $request->identificacion,
            'tipoid' => strtoupper($request->tipoid),
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'ciudad_nacimiento_id' => $request->ciudad,
            'rh_sanguineo_id' => $request->rh_sanguineo_id,
            'estado_civil_id' => $request->estado_civil_id,
            'direccion_recidencia' => strtoupper($request->direccion_recidencia),
            'celular' => $usuario->telefono,
            'correo_electro_personal' => $usuario->email,
            'link_instagram' => $request->link_instagram,
            'link_linkedin' => $request->link_linkedin,
            'link_twitter' => $request->link_twitter,
            'link_facebook' => $request->link_facebook,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarInfoFamiliar(Request $request)
    {
        $request->validate([
            'num_personas_dependientes' => 'required|string|max:255',
            'numero_contacto_emergencias' => 'required|string|max:255',
            'nombre_contacto_emergencias' => 'required|string|max:255',
        ]);
        auth()->user()->persona->update([
            'num_personas_dependientes' => $request->num_personas_dependientes,
            'numero_contacto_emergencias' => $request->numero_contacto_emergencias,
            'nombre_contacto_emergencias' => strtoupper($request->nombre_contacto_emergencias),
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function agregarEstudioFormal(Request $request)
    {
        $request->validate([
            'nivelFormacion' => 'required|exists:nivel_formacions,id',
            'finalizado' => 'required',
            'subAreaConocimiento' => 'required|exists:sub_area_conocimientos,id',
            'tituloObtenino' => 'required',
            'fechaTitulo' => 'required|date',
            //'diploma_rutaarchivo' => 'file|mimes:pdf',
        ]);
        $path = $this->pathDocumentos();

        $estudio = EstudiosFormales::create([
            'titulo_obtenido' => $request->tituloObtenino,
            'finalizado' => $request->finalizado,
            'fecha_obtenido_titulo' => $request->fechaTitulo,
            'persona_id' => auth()->user()->persona->id,
            'nivel_formacion_id' => $request->nivelFormacion,
            'sub_area_conocimiento_id' => $request->subAreaConocimiento,
        ]);

        /* if ($request->diploma_rutaarchivo){
             move_uploaded_file($request->file('diploma_rutaarchivo'), $path .$request->tituloObtenino.'-diploma.'.$request->diploma_rutaarchivo->getClientOriginalExtension());
             $estudio->diploma_rutaarchivo=$path.$request->tituloObtenino.'-diploma.'.$request->diploma_rutaarchivo->getClientOriginalExtension();
             $estudio->save();
         }*/
        return response(['message' => 'Informacion Agregada con exito', 'estudio' => $estudio]);

    }

    public function eliminarEstudioFormal(Request $request)
    {
        $request->validate([
            'idEstudio' => 'required|exists:estudios_formales,id',
        ]);
        $estudio = EstudiosFormales::find($request->idEstudio);
        if (File::exists($estudio->diploma_rutaarchivo)) {
            //unlink($estudio->diploma_rutaarchivo);
            File::delete($estudio->diploma_rutaarchivo);
        }
        $estudio->delete();

        return response(['message' => 'Informacion eliminada con exito', 'estudio' => $estudio]);

    }

    public function agregarEstudioNoFormal(Request $request)
    {
        $request->validate([
            'num_horas' => 'required',
            'tituloEstudio' => 'required',
            'nivel_formacion_no_formal_id' => 'required|exists:nivel_formacion_no_formals,id',
            'fecha_certificado' => 'required|date',
        ]);

        $estudio = EducacionNoFormal::create([
            'num_horas' => $request->num_horas,
            'tituloEstudio' => $request->tituloEstudio,
            'nivel_formacion_no_formal_id' => $request->nivel_formacion_no_formal_id,
            'fecha_certificado' => $request->fecha_certificado,
            'persona_id' => auth()->user()->persona->id,
        ]);

        return response(['message' => 'Informacion Agregada con exito', 'estudio' => $estudio]);

    }

    public function eliminarEstudioNoFormal(Request $request)
    {
        $request->validate([
            'idEstudio' => 'required|exists:educacion_no_formals,id',
        ]);
        $estudio = EducacionNoFormal::find($request->idEstudio);
        $estudio->delete();

        return response(['message' => 'Informacion eliminada con exito', 'estudio' => $estudio]);

    }

    public function agregarExperienciaLaboral(Request $request)
    {
        $request->validate([
            'nombre_empresa_empleadora' => 'required',
            'actividad_economina_empleador' => 'required',
            'principal_funciones_cargo' => 'required',
            'fecha_inicio' => 'required|date',
        ]);

        $experiencia = ExperienciaPersonal::create([
            'nombre_empresa_empleadora' => $request->nombre_empresa_empleadora,
            'actividad_economina_empleador' => $request->actividad_economina_empleador,
            'principal_funciones_cargo' => $request->principal_funciones_cargo,
            'fecha_inicio' => $request->fecha_inicio,
            'fecha_retiro' => $request->fecha_retiro == "" ? null : $request->fecha_retiro,
            'persona_id' => auth()->user()->persona->id,
        ]);

        return response(['message' => 'Informacion Agregada con exito', 'experiencia' => $experiencia]);

    }

    public function eliminarExperienciaLaboral(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:experiencia_personals,id',
        ]);
        $experiencia = ExperienciaPersonal::find($request->id);
        $experiencia->delete();

        return response(['message' => 'Informacion eliminada con exito', 'experiencia' => $experiencia]);

    }

    public function actualizarInfoSeguridadSocial(Request $request)
    {
        $request->validate([
            'eps_id' => 'required|exists:entidades_seguridad_socials,id',
            'pensiones_id' => 'required|exists:entidades_seguridad_socials,id',
            'cesantias_id' => 'required|exists:entidades_seguridad_socials,id',
        ]);
        auth()->user()->persona->update([
            'eps_id' => $request->eps_id,
            'pensiones_id' => $request->pensiones_id,
            'cesantias_id' => $request->cesantias_id,
        ]);

        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarCertificaciones(Request $request)
    {
        $request->validate([
            'nivel_ingles_id' => 'required|numeric|max:255',
            'nivel_ingles_certifica_rutaarachivo' => 'required|file|mimes:pdf',
        ]);
        $path = $this->pathDocumentos();
        auth()->user()->persona->update([
            'nivel_ingles_id' => $request->nivel_ingles_id,
        ]);
        if ($request->nivel_ingles_certifica_rutaarachivo) {
            move_uploaded_file($request->file('nivel_ingles_certifica_rutaarachivo'), $path . 'certificado-ingles.' . $request->nivel_ingles_certifica_rutaarachivo->getClientOriginalExtension());
            auth()->user()->persona->nivel_ingles_certifica_rutaarachivo = 'certificado-ingles.' . $request->nivel_ingles_certifica_rutaarachivo->getClientOriginalExtension();
            auth()->user()->persona->save();
        }
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarHabilidadesTI(Request $request)
    {

        foreach (Habilidades::all() as $habilidad) {
            if ($request['habilidad-' . $habilidad->id]) {
                $actualizar = false;
                //verificar si la persona tiene la habilidad
                foreach (auth()->user()->persona->habilidades as $hability) {
                    if ($hability->nivel_habilidad->habilidade_id == $habilidad->id) {
                        //actualizar
                        $actualizar = true;
                        $hability->update([
                            'nivel_habilidad_id' => $request['habilidad-' . $habilidad->id]
                        ]);
                    }
                }
                if (!$actualizar) {
                    //guardar nueva
                    PersonaHabilidades::create([
                        'nivel_habilidad_id' => $request['habilidad-' . $habilidad->id],
                        'persona_id' => auth()->user()->persona->id
                    ]);
                }
            }
        }

        return response(['message' => 'Informacion Actualizada con exito', 'data' => auth()->user()->persona->habilidades->toArray()]);
    }

    public function agregarHabilidad(Request $request)
    {
        $request->validate([
            'nivel_habilidad_id' => 'required|exists:nivel_habilidads,id',
        ]);
        $actualizar = false;

        $nivelHabilidad = NivelHabilidad::find($request->nivel_habilidad_id);
        foreach (auth()->user()->persona->habilidades as $hability) {
            if ($hability->nivel_habilidad->habilidade_id == $nivelHabilidad->habilidad->id) {
                //actualizar
                $actualizar = true;
                $hability->nivel_habilidad_id = $request->nivel_habilidad_id;
                $nivel_habilidad = $hability;
                $hability->save();
                break;
            }
        }
        $habilidad = NivelHabilidad::find($request->nivel_habilidad_id);
        if (!$actualizar) {
            $nivel_habilidad = PersonaHabilidades::create([
                'nivel_habilidad_id' => $request->nivel_habilidad_id,
                'persona_id' => auth()->user()->persona->id
            ]);
        }
        return response([
            'message' => 'Informacion Agregada con exito',
            'habilidad' => $nivel_habilidad,
            'titulo' => $habilidad->habilidad->descripcion,
            'nivel' => $habilidad->descripcion,
            'actualizar' => $actualizar,
        ]);

    }

    public function eliminarHabilidad(Request $request)
    {
        $request->validate([
            'idHabilidad' => 'required|exists:persona_habilidades,id',
        ]);
        $habilidad = PersonaHabilidades::find($request->idHabilidad);
        $habilidad->delete();

        return response(['message' => 'Informacion eliminada con exito', 'habilidad' => $habilidad]);

    }

    public function agregarIdioma(Request $request)
    {
        $request->validate([
            'idIdioma' => 'required|exists:idiomas,id',
        ]);

        $idioma = PersonaIdioma::where('idioma_id', $request->idIdioma)->where('persona_id', auth()->user()->persona->id)->first();
        $actualizar=false;

        if ($idioma) {
            //actualizar
            $idioma->escrito = $request->escrito;
            $idioma->leido = $request->leido;
            $idioma->hablado = $request->hablado;
            $idioma->escuchado = $request->escuchado;
            $idioma->save();
            $actualizar=true;
        }else{
            $idioma=PersonaIdioma::create([
                'idioma_id'=>$request->idIdioma,
                'persona_id'=>auth()->user()->persona->id,
                'escrito'=>$request->escrito,
                'leido'=>$request->leido,
                'hablado'=>$request->hablado,
                'escichado'=>$request->escichado,
            ]);
        }

        return response([
            'message' => 'Informacion Agregada con exito',
            'peronaIdioma' => $idioma,
            'idioma' => $idioma->idioma->nombre,
            'actualizar' => $actualizar,
        ]);

    }

    public function eliminarIdioma(Request $request)
    {
        $request->validate([
            'idIdioma' => 'required|exists:persona_idiomas,id',
        ]);
        $idioma = PersonaIdioma::find($request->idIdioma);
        $idioma->delete();

        return response(['message' => 'Informacion eliminada con exito', 'idioma' => $idioma]);

    }

    function validadInputsPersona()
    {
        $usuario = auth()->user()->persona;

        $contPersonales = 0;
        $contFamiliar = 0;

        $contID = 0;


        auth()->user()->nombres == "" || auth()->user()->nombres == null ? $contPersonales++ : 0;
        auth()->user()->apellidos == "" || auth()->user()->apellidos == null ? $contPersonales++ : 0;
        $usuario->identificacion == "" || $usuario->identificacion == null ? $contPersonales++ : 0;
        $usuario->tipoid == "" || $usuario->tipoid == null ? $contPersonales++ : 0;
        $usuario->fecha_nacimiento == "" || $usuario->fecha_nacimiento == null ? $contPersonales++ : 0;
        $usuario->ciudad == "" || $usuario->ciudad == null ? $contPersonales++ : 0;
        $usuario->rh_sanguineo_id == "" || $usuario->rh_sanguineo_id == null ? $contPersonales++ : 0;
        $usuario->estado_civil_id == "" || $usuario->estado_civil_id == null ? $contPersonales++ : 0;
        $usuario->direccion_recidencia == "" || $usuario->direccion_recidencia == null ? $contPersonales++ : 0;
        $usuario->correo_electro_personal == "" || $usuario->correo_electro_personal == null ? $contPersonales++ : 0;
        $usuario->identificacion_rurataarchivo == "" || $usuario->identificacion_rurataarchivo == null ? $contPersonales++ : 0;

        $usuario->identificacion_rurataarchivo == "" || $usuario->identificacion_rurataarchivo == null ? $contID++ : 0;

        $usuario->num_personas_dependientes == "" || $usuario->num_personas_dependientes == null ? $contFamiliar++ : 0;
        $usuario->numero_contacto_emergencias == "" || $usuario->numero_contacto_emergencias == null ? $contFamiliar++ : 0;
        $usuario->nombre_contacto_emergencias == "" || $usuario->nombre_contacto_emergencias == null ? $contFamiliar++ : 0;

        $response = array(
            'identificacion_rurataarchivo' => $usuario->identificacion_rurataarchivo,
            'datos_personales' => $contPersonales,
            'informacion_familiar' => $contFamiliar,
            'contID' => $contID,
            'cert-ingles' => $usuario->nivel_ingles_certifica_rutaarachivo,
            'cont-cert-ingles' => $usuario->nivel_ingles_certifica_rutaarachivo ? '0' : '1',
        );

        return response()->json($response);


    }

    function pathDocumentos()
    {
        $path = "documentos/persona/" . auth()->user()->persona->id . "/";
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }

}
