<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\EmpresaHabilidades;
use App\Models\NivelHabilidad;
use App\Models\PersonaHabilidades;
use App\Models\Presupuesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{
    //

    public function actualizarInformacionDeLaEmpresa(Request $request)
    {
        /*$request->validate([
            'razonsocial' => 'required|string|max:255',
            'nombrecomercial' => 'required|string|max:255',
            'nit' => 'required|string|max:255',
            'rut_rutaarachivo' => 'file|mimes:pdf',
            'camara_comercio_rutaarachivo' => 'file|mimes:pdf',
            'breve_descr_empresa' => 'required',
            'direccion_correspondencia' => 'required|string|max:255',
            'celular' => 'required|string|max:255',
            'anno_creacion_empresa' => 'required|string|max:255',
            'num_empleados_directos' => 'required|string|max:255',
            'num_empleados_indirectos' => 'required|string|max:255',
            'ciudad' => 'required|exists:cities,id',
            'mercado' => 'required',
            'recurso_humano' => 'required',
        ]);*/
        $path = $this->pathDocumentos();
        if ($request->rut_rutaarachivo) {
            move_uploaded_file($request->file('rut_rutaarachivo'), $path . 'rut.' . $request->rut_rutaarachivo->getClientOriginalExtension());
            auth()->user()->empresa->rut_rutaarachivo = $path . 'rut.' . $request->rut_rutaarachivo->getClientOriginalExtension();
            auth()->user()->empresa->save();
        }
        if ($request->camara_comercio_rutaarachivo) {
            move_uploaded_file($request->file('camara_comercio_rutaarachivo'), $path . 'camara_comercio.' . $request->camara_comercio_rutaarachivo->getClientOriginalExtension());
            auth()->user()->empresa->camara_comercio_rutaarachivo = $path . 'camara_comercio.' . $request->camara_comercio_rutaarachivo->getClientOriginalExtension();
            auth()->user()->empresa->save();
        }
        auth()->user()->empresa->update([
            'razonsocial' => $request->razonsocial,
            'nit' => $request->nit,
            'nombrecomercial' => $request->nombrecomercial,
            'breve_descr_empresa' => $request->breve_descr_empresa,
            'direccion_correspondencia' => $request->direccion_correspondencia,
            'telefono' => $request->telefono,
            'celular' => $request->celular,
            'anno_creacion_empresa' => $request->anno_creacion_empresa,
            'num_empleados_directos' => $request->num_empleados_directos,
            'num_empleados_indirectos' => $request->num_empleados_indirectos,
            'city_id' => $request->ciudad,
            'clave_rut' => $request->clave_rut,
            'mercado' => $request->mercado,
            'recurso_humano' => $request->recurso_humano,
        ]);
        auth()->user()->empresa->tipoEmpresas()->sync($request->tipo_empresas);
        return response(['message' => 'Informacion Actualizada con exito', 'request' => $request->all()]);
    }

    public function actualizarRepresentanteLegal(Request $request)
    {
        /*$request->validate([
            'nombte_representante_legal' => 'required|string|max:255',
            'identifica_representante_legal' => 'required',
            'tipoid_represen_legal' => 'required|exists:tipo_identificacions,id',
            'identi_rutaarchivo' => 'file|mimes:pdf',
        ]);*/
        $path = $this->pathDocumentos();
        if ($request->identi_rutaarchivo) {
            move_uploaded_file($request->file('identi_rutaarchivo'), $path . 'identificacion.' . $request->identi_rutaarchivo->getClientOriginalExtension());
            auth()->user()->empresa->identi_rutaarchivo = $path . 'identificacion.' . $request->identi_rutaarchivo->getClientOriginalExtension();
            auth()->user()->empresa->save();
        }
        auth()->user()->empresa->update([
            'nombte_representante_legal' => $request->nombte_representante_legal,
            'identifica_representante_legal' => $request->identifica_representante_legal,
            'tipoid_represen_legal' => $request->tipoid_represen_legal,
            'celular_representante_legal' => $request->celular_representante_legal,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarActividadEconomica(Request $request)
    {
        /*$request->validate([
            'sector_empresa_id' => 'required|exists:sector_empresas,id',
            'actividad_economica_1_id' => 'required|exists:actividad_economicas,id',
            'actividad_economica_2_id' => 'required|exists:actividad_economicas,id',
            'prome_ventas_anuales' => 'required|string|max:255',
            'balance_general_rutaarachivo' => 'file|mimes:pdf',
            'total_activos' => 'required|string|max:255',
            'num_sedes' => 'required|string|max:255',
            'ciudades_otras_sedes' => 'required|string|max:255',
            'tipo_operacion_id' => 'required|exists:tipo_operacions,id',
            'actividad_economica_otros' => 'exists:actividad_economicas,id',
        ]);*/
        $path = $this->pathDocumentos();
        if ($request->balance_general_rutaarachivo) {
            move_uploaded_file($request->file('identi_rutaarchivo'), $path . 'balanceo_general.' . $request->balance_general_rutaarachivo->getClientOriginalExtension());
            auth()->user()->empresa->balance_general_rutaarachivo = $path . 'balanceo_general.' . $request->balance_general_rutaarachivo->getClientOriginalExtension();
            auth()->user()->empresa->save();
        }
        auth()->user()->empresa->update([
            'sector_empresa_id' => $request->sector_empresa_id,
            'actividad_economica_1_id' => $request->actividad_economica_1_id,
            'actividad_economica_2_id' => $request->actividad_economica_2_id,
            'prome_ventas_anuales' => $request->prome_ventas_anuales,
            'total_activos' => $request->total_activos,
            'num_sedes' => $request->num_sedes,
            'ciudades_otras_sedes' => $request->ciudades_otras_sedes,
            'tipo_operacion_id' => $request->tipo_operacion_id,
        ]);
        auth()->user()->empresa->actividadEconomicaOtros()->sync($request->actividad_economica_otros);

        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarImagenCorporativayWeb(Request $request)
    {
       /* $request->validate([
            'pagina_web' => 'string|max:255',
            'link_portafolio' => 'string|max:255',
            'logo_rutararchivo' => 'file|mimes:png,jpg',
            'link_facebook' => 'string|max:255',
            'link_instagram' => 'string|max:255',
            'link_linkedin' => 'string|max:255',
            'link_twitter' => 'string|max:255',
            'tiene_canales_virtuales_atencion' => 'required|string|max:255',
            'realiza_ventas_linea' => 'required|string|max:255',
        ]);*/
        $path = $this->pathDocumentos();
        if ($request->logo_rutararchivo) {
            move_uploaded_file($request->file('logo_rutararchivo'), $path . 'logo.' . $request->logo_rutararchivo->getClientOriginalExtension());
            auth()->user()->empresa->logo_rutararchivo = $path . 'logo.' . $request->logo_rutararchivo->getClientOriginalExtension();
            auth()->user()->empresa->save();
        }
        auth()->user()->empresa->update([
            'pagina_web' => $request->pagina_web,
            'link_portafolio' => $request->link_portafolio,
            'link_facebook' => $request->link_facebook,
            'link_instagram' => $request->link_instagram,
            'link_linkedin' => $request->link_linkedin,
            'link_twitter' => $request->link_twitter,
            'tiene_canales_virtuales_atencion' => $request->tiene_canales_virtuales_atencion,
            'realiza_ventas_linea' => $request->realiza_ventas_linea,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarCompetitividad(Request $request)
    {
        /*$request->validate([
            'certifica_iso9001' => 'required|string|max:255',
            'certifica_iso14001' => 'required|string|max:255',
            'otra_certifica_internacional' => 'string|max:255',
            'uso_energias_alternativas' => 'required',
            'num_convenios_estra_alianza_estable' => 'required|string|max:255',
            'porce_presupues_info_personal' => 'required|string|max:255',
            'cuenta_software_crm' => 'required|string|max:255',
            'cuenta_software_erp' => 'required|string|max:255',
        ]);*/

        auth()->user()->empresa->update([
            'certifica_iso9001' => $request->certifica_iso9001,
            'certifica_iso14001' => $request->certifica_iso14001,
            'otra_certifica_internacional' => $request->otra_certifica_internacional,
            'uso_energias_alternativas' => $request->uso_energias_alternativas,
            'num_convenios_estra_alianza_estable' => $request->num_convenios_estra_alianza_estable,
            'porce_presupues_info_personal' => $request->porce_presupues_info_personal,
            'cuenta_software_crm' => $request->cuenta_software_crm,
            'cuenta_software_erp' => $request->cuenta_software_erp,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarCapitalHumano(Request $request)
    {
        /*$request->validate([
            'num_persona_dominio_segunda_lengua' => 'required',
            'num_mujeres_empleadas' => 'required',
            'num_empleados_con_postgrado' => 'required',
            'edad_promedio_empleados' => 'required',
            'num_madres_cabeza_hogar' => 'required',
        ]);*/

        auth()->user()->empresa->update([
            'num_persona_dominio_segunda_lengua' => $request->num_persona_dominio_segunda_lengua,
            'num_mujeres_empleadas' => $request->num_mujeres_empleadas,
            'num_empleados_con_postgrado' => $request->num_empleados_con_postgrado,
            'edad_promedio_empleados' => $request->edad_promedio_empleados,
            'num_madres_cabeza_hogar' => $request->num_madres_cabeza_hogar,
            'num_empleados_habilidades_ti' => $request->num_empleados_habilidades_ti,
            'num_empleados_region' => $request->num_empleados_region,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarExpectativas(Request $request)
    {
        /*$request->validate([
            'ingremento_ventas' => 'string|max:255',
            'llegar_nuevos_mercados' => 'string|max:255',
            'lanzamiento_nuevos_productos' => 'string|max:255',
            'mejoramiento_productividad' => 'required',
            'incremento_capacidad_productiva' => 'required|string|max:255',
            'desarrollo_nuevos_canales' => 'required|string|max:255',
            'implementacion_ti' => 'required|string|max:255',
            'infraestructura_fisica' => 'required|string|max:255',
            'compra_maquinaria_equipos' => 'required|string|max:255',
        ]);*/

        auth()->user()->empresa->update([
            'ingremento_ventas' => $request->ingremento_ventas,
            'llegar_nuevos_mercados' => $request->llegar_nuevos_mercados,
            'lanzamiento_nuevos_productos' => $request->lanzamiento_nuevos_productos,
            'mejoramiento_productividad' => $request->mejoramiento_productividad,
            'incremento_capacidad_productiva' => $request->incremento_capacidad_productiva,
            'desarrollo_nuevos_canales' => $request->desarrollo_nuevos_canales,
            'implementacion_ti' => $request->implementacion_ti,
            'infraestructura_fisica' => $request->infraestructura_fisica,
            'compra_maquinaria_equipos' => $request->compra_maquinaria_equipos,
        ]);
        return response(['message' => 'Informacion Actualizada con exito']);
    }

    public function actualizarEmpresasTI(Request $request)
    {
        /*$request->validate([
            'num_empleados_habilidades_ti' => 'required|string|max:255',
            'num_empleados_region' => 'required|string|max:255',
            'tipo_comercionalizacion_id' => 'required|exists:tipo_comercionalizacions,id',
            'sector_negocio' => 'required',
            'servicios_ofertados' => 'required',
        ]);*/

        auth()->user()->empresa->update([
            'num_empleados_habilidades_ti' => $request->num_empleados_habilidades_ti,
            'num_empleados_region' => $request->num_empleados_region,
            'tipo_comercionalizacion_id' => $request->tipo_comercionalizacion_id,
        ]);

        auth()->user()->empresa->servicios_ofertados()->sync($request->servicios_ofertados);
        auth()->user()->empresa->sector_negocios()->sync($request->sector_negocio);

        return response(['message' => 'Informacion Actualizada con exito',
            'data' => $request->all()]);
    }

    public function validarInputsEmpresa()
    {

        $empresa = auth()->user()->empresa;

        $contInfEmpresa = 0;
        $contInfRepresentante = 0;
        $contInfActividad = 0;
        $contInfWeb = 0;
        $contInfCompetitividad = 0;
        $contInfCapitalHumano = 0;

        $contRut = 0;
        $contCamaraComercio = 0;
        $contIdRepresentante = 0;
        $contBalance = 0;
        $contLogo = 0;

        $empresa->razonsocial == "" || $empresa->razonsocial == null ? $contInfEmpresa++ : 0;
        $empresa->nombrecomercial == "" || $empresa->nombrecomercial == null ? $contInfEmpresa++ : 0;
        $empresa->rut_rutaarachivo == "" || $empresa->rut_rutaarachivo == null ? $contInfEmpresa++ : 0;
        $empresa->rut_rutaarachivo == "" || $empresa->rut_rutaarachivo == null ? $contRut++ : 0;
        $empresa->camara_comercio_rutaarachivo == "" || $empresa->camara_comercio_rutaarachivo == null ? $contCamaraComercio++ : 0;
        $empresa->breve_descr_empresa == "" || $empresa->breve_descr_empresa == null ? $contInfEmpresa++ : 0;
        $empresa->breve_descr_empresa == "" || $empresa->razonsocial == null ? $contInfEmpresa++ : 0;
        $empresa->direccion_correspondencia == "" || $empresa->direccion_correspondencia == null ? $contInfEmpresa++ : 0;
        //$empresa->telefono == "" || $empresa->telefono == null ? $contInfEmpresa++ : 0;
        $empresa->anno_creacion_empresa == "" || $empresa->anno_creacion_empresa == null ? $contInfEmpresa++ : 0;
        $empresa->celular == "" || $empresa->celular == null ? $contInfEmpresa++ : 0;
        $empresa->num_empleados_directos == "" || $empresa->num_empleados_directos == null ? $contInfEmpresa++ : 0;
        $empresa->num_empleados_indirectos == "" || $empresa->num_empleados_indirectos == null ? $contInfEmpresa++ : 0;
        $empresa->city_id == "" || $empresa->city_id == null ? $contInfEmpresa++ : 0;


        $empresa->tipoid_represen_legal == "" || $empresa->tipoid_represen_legal == null ? $contInfRepresentante++ : 0;
        $empresa->identifica_representante_legal == "" || $empresa->identifica_representante_legal == null ? $contInfRepresentante++ : 0;
        $empresa->identifica_representante_legal == "" || $empresa->identifica_representante_legal == null ? $contIdRepresentante++ : 0;
        $empresa->nombte_representante_legal == "" || $empresa->nombte_representante_legal == null ? $contInfRepresentante++ : 0;
        $empresa->identi_rutaarchivo == "" || $empresa->identi_rutaarchivo == null ? $contInfRepresentante++ : 0;


        $empresa->sector_empresa_id == "" || $empresa->sector_empresa_id == null ? $contInfActividad++ : 0;
        $empresa->actividad_economica_1_id == "" || $empresa->actividad_economica_1_id == null ? $contInfActividad++ : 0;
        $empresa->actividad_economica_2_id == "" || $empresa->actividad_economica_2_id == null ? $contInfActividad++ : 0;
        $empresa->prome_ventas_anuales == "" || $empresa->prome_ventas_anuales == null ? $contInfActividad++ : 0;
        $empresa->balance_general_rutaarachivo == "" || $empresa->balance_general_rutaarachivo == null ? $contInfActividad++ : 0;
        $empresa->balance_general_rutaarachivo == "" || $empresa->balance_general_rutaarachivo == null ? $contBalance++ : 0;
        $empresa->total_activos == "" || $empresa->total_activos == null ? $contInfActividad++ : 0;
        $empresa->tipo_operacion_id == "" || $empresa->tipo_operacion_id == null ? $contInfActividad++ : 0;
        $empresa->num_sedes == "" || $empresa->num_sedes == null ? $contInfActividad++ : 0;
        $empresa->ciudades_otras_sedes == "" || $empresa->ciudades_otras_sedes == null ? $contInfActividad++ : 0;

        $empresa->pagina_web == "" || $empresa->pagina_web == null ? $contInfWeb++ : 0;
        $empresa->link_portafolio == "" || $empresa->link_portafolio == null ? $contInfWeb++ : 0;
        $empresa->tiene_canales_virtuales_atencion == "" || $empresa->tiene_canales_virtuales_atencion == null ? $contInfWeb++ : 0;
        $empresa->logo_rutararchivo == "" || $empresa->logo_rutararchivo == null ? $contInfWeb++ : 0;
        $empresa->logo_rutararchivo == "" || $empresa->logo_rutararchivo == null ? $contLogo++ : 0;
        $empresa->link_facebook == "" || $empresa->link_facebook == null ? $contInfWeb++ : 0;
        $empresa->link_instagram == "" || $empresa->link_instagram == null ? $contInfWeb++ : 0;
        $empresa->link_linkedin == "" || $empresa->link_linkedin == null ? $contInfWeb++ : 0;
        $empresa->link_twitter == "" || $empresa->link_twitter == null ? $contInfWeb++ : 0;
        $empresa->realiza_ventas_linea == "" || $empresa->realiza_ventas_linea == null ? $contInfWeb++ : 0;

        $empresa->certifica_iso9001 == "" || $empresa->certifica_iso9001 == null ? $contInfCompetitividad++ : 0;
        $empresa->certifica_iso14001 == "" || $empresa->certifica_iso14001 == null ? $contInfCompetitividad++ : 0;
        $empresa->otra_certifica_internacional == "" || $empresa->otra_certifica_internacional == null ? $contInfCompetitividad++ : 0;
        $empresa->uso_energias_alternativas == "" || $empresa->uso_energias_alternativas == null ? $contInfCompetitividad++ : 0;
        $empresa->num_convenios_estra_alianza_estable == "" || $empresa->num_convenios_estra_alianza_estable == null ? $contInfCompetitividad++ : 0;
        $empresa->porce_presupues_info_personal == "" || $empresa->porce_presupues_info_personal == null ? $contInfCompetitividad++ : 0;
        $empresa->cuenta_software_crm == "" || $empresa->cuenta_software_crm == null ? $contInfCompetitividad++ : 0;
        $empresa->cuenta_software_erp == "" || $empresa->cuenta_software_erp == null ? $contInfCompetitividad++ : 0;

        $empresa->num_persona_dominio_segunda_lengua == "" || $empresa->num_persona_dominio_segunda_lengua == null ? $contInfCapitalHumano++ : 0;
        $empresa->num_mujeres_empleadas == "" || $empresa->num_mujeres_empleadas == null ? $contInfCapitalHumano++ : 0;
        $empresa->num_empleados_con_postgrado == "" || $empresa->num_empleados_con_postgrado == null ? $contInfCapitalHumano++ : 0;
        $empresa->edad_promedio_empleados == "" || $empresa->edad_promedio_empleados == null ? $contInfCapitalHumano++ : 0;
        $empresa->num_madres_cabeza_hogar == "" || $empresa->num_madres_cabeza_hogar == null ? $contInfCapitalHumano++ : 0;


        $response = array(
            'empresa' => $contInfEmpresa,
            'rut' => $contRut,
            'camaraComercio' => $contCamaraComercio,
            'idRepresentante' => $contIdRepresentante,
            'contBalance' => $contBalance,
            'contLogo' => $contLogo,
            'representante' => $contInfRepresentante,
            'actividad-economica' => $contInfActividad,
            'web' => $contInfWeb,
            'competitividad' => $contInfCompetitividad,
            'capital-humano' => $contInfCapitalHumano,
            'estado-empresa' => $contInfEmpresa + $contInfRepresentante + $contIdRepresentante + $contInfActividad + $contLogo + $contInfRepresentante + $contInfActividad + $contInfWeb + $contInfCompetitividad + $contInfCapitalHumano,
            'empresa_data' => $empresa,

        );

        return response()->json($response);

    }

    public function agregarHabilidadEmpresa(Request $request)
    {
        $request->validate([
            'nivel_habilidad_id' => 'required|exists:nivel_habilidads,id',
        ]);
        $actualizar = false;

        $nivelHabilidad = NivelHabilidad::find($request->nivel_habilidad_id);
        foreach (auth()->user()->empresa->habilidades as $hability) {
            if ($hability->nivel_habilidad->habilidade_id == $nivelHabilidad->habilidad->id) {
                //actualizar
                $actualizar = true;
                $hability->nivel_habilidad_id = $request->nivel_habilidad_id;
                $nivel_habilidad = $hability;
                $hability->save();
                break;
            }
        }
        $habilidad = NivelHabilidad::find($request->nivel_habilidad_id);
        if (!$actualizar) {
            $nivel_habilidad = EmpresaHabilidades::create([
                'nivel_habilidad_id' => $request->nivel_habilidad_id,
                'empresa_id' => auth()->user()->empresa->id
            ]);
        }
        return response([
            'message' => 'Informacion Agregada con exito',
            'habilidad' => $nivel_habilidad,
            'titulo' => $habilidad->habilidad->descripcion,
            'nivel' => $habilidad->descripcion,
            'actualizar' => $actualizar,
        ]);

    }

    public function eliminarHabilidadEmpresa(Request $request)
    {
        $request->validate([
            'idHabilidad' => 'required|exists:empresa_habilidades,id',
        ]);
        $habilidad = EmpresaHabilidades::find($request->idHabilidad);
        $habilidad->delete();

        return response(['message' => 'Informacion eliminada con exito', 'habilidad' => $habilidad]);

    }

    public function actualizarPresupuesto(Request $request)
    {
        $request->validate([
            'especie' => 'required|numeric',
            'efectivo' => 'required|numeric',
            'anio' => 'required|numeric',
        ]);
        $presupuesto = Presupuesto::where('empresa_id', \auth()->user()->empresa->id)->where('anio', $request->anio)->first();
        $presupuesto->especie = $request->especie;
        $presupuesto->efectivo = $request->efectivo;
        $presupuesto->save();
        return response(['message' => 'Informacion actualizada con exito', 'presupuesto' => $presupuesto,'total'=>$request->especie+$request->efectivo]);
    }

    function pathDocumentos()
    {
        $path = "documentos/empresa/" . auth()->user()->empresa->id . "/";
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }
}
