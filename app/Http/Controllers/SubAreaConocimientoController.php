<?php

namespace App\Http\Controllers;

use App\Models\SubAreaConocimiento;
use Illuminate\Http\Request;

class SubAreaConocimientoController extends Controller
{
    //
    public function getSubAreaConocimiento(Request $request){
        $subAreas=SubAreaConocimiento::where('area_conocimiento_id',$request->area_id)->get();
        return response(['subAreas' => $subAreas]);
    }
}
