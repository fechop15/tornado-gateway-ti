<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Countries;
use App\Models\States;
use Illuminate\Http\Request;

class CountryStateCityController extends Controller
{
    //
    public function getCountry()
    {
        $data['countries'] = Countries::get(["name","id"]);
        return response()->json($data);
    }
    public function getState(Request $request)
    {
        $data['states'] = States::where("country_id",$request->country_id)
            ->get(["name","id"]);
        return response()->json($data);
    }
    public function getCity(Request $request)
    {
        $data['cities'] = Cities::where("state_id",$request->state_id)
            ->get(["name","id"]);
        return response()->json($data);
    }
}
