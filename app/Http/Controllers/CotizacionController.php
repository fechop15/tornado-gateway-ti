<?php

namespace App\Http\Controllers;

use App\Models\Cotizacion;
use App\Models\Proyecto;
use Illuminate\Http\Request;

class CotizacionController extends Controller
{
    //
    public function ver(Request $request)
    {
        $proyecto=Proyecto::find($request->proyecto_id);
        $response = array(
            'status' => 'success',
            'msg' => $proyecto==null?null:$proyecto->cotizaciones,
        );
        return response()->json($response);
    }

    public function registrar(Request $request)
    {
        $request->validate(
            [
                'nombreEmpresa' => 'required',
                'cotizacion_PDF' => 'required',
                'documento_PDF' => 'required',
                'camara_comercio_PDF' => 'required',
                'rut_PDF' => 'required',
                'representante_legal_PDF' => 'required',
                'proyecto_id' => 'required',
            ]
        );

        $cotizacion = Cotizacion::create([
            'nombreEmpresa' => $request->nombreEmpresa,
            'proyecto_id' => $request->proyecto_id,
        ]);
        $path = $this->pathDocumentos($cotizacion->proyecto, $cotizacion);
        if (isset($request->cotizacion_PDF)) {
            move_uploaded_file($request->file('cotizacion_PDF'), $path . 'cotizacion.' . $request->cotizacion_PDF->getClientOriginalExtension());
            $file = $path . 'cotizacion.' . $request->cotizacion_PDF->getClientOriginalExtension();
            $cotizacion->cotizacion_PDF = $file;
        }
        if (isset($request->documento_PDF)) {
            move_uploaded_file($request->file('documento_PDF'), $path . 'documento.' . $request->documento_PDF->getClientOriginalExtension());
            $file = $path . 'documento.' . $request->documento_PDF->getClientOriginalExtension();
            $cotizacion->documento_PDF = $file;
        }
        if (isset($request->camara_comercio_PDF)) {
            move_uploaded_file($request->file('camara_comercio_PDF'), $path . 'camara_comercio.' . $request->camara_comercio_PDF->getClientOriginalExtension());
            $file = $path . 'camara_comercio.' . $request->camara_comercio_PDF->getClientOriginalExtension();
            $cotizacion->camara_comercio_PDF = $file;
        }
        if (isset($request->rut_PDF)) {
            move_uploaded_file($request->file('rut_PDF'), $path . 'rut.' . $request->rut_PDF->getClientOriginalExtension());
            $file = $path . 'rut.' . $request->rut_PDF->getClientOriginalExtension();
            $cotizacion->rut_PDF = $file;
        }
        if (isset($request->representante_legal_PDF)) {
            move_uploaded_file($request->file('representante_legal_PDF'), $path . 'representante_legal.' . $request->representante_legal_PDF->getClientOriginalExtension());
            $file = $path . 'representante_legal.' . $request->representante_legal_PDF->getClientOriginalExtension();
            $cotizacion->representante_legal_PDF = $file;
        }
        $cotizacion->save();
        return response(['message' => 'Cotizacion registrada', 'cotizacion' => $cotizacion]);
    }

    public function editar(Request $request)
    {
        $cotizacion = Cotizacion::find($request->id);
        $path = $this->pathDocumentos($cotizacion->proyecto, $cotizacion);

        if (isset($request->cotizacion_PDF)) {
            move_uploaded_file($request->file('cotizacion_PDF'), $path . 'cotizacion.' . $request->cotizacion_PDF->getClientOriginalExtension());
            $file = $path . 'cotizacion.' . $request->cotizacion_PDF->getClientOriginalExtension();
            $cotizacion->cotizacion_PDF = $file;
        }
        if (isset($request->documento_PDF)) {
            move_uploaded_file($request->file('documento_PDF'), $path . 'documento.' . $request->documento_PDF->getClientOriginalExtension());
            $file = $path . 'documento.' . $request->documento_PDF->getClientOriginalExtension();
            $cotizacion->documento_PDF = $file;
        }
        if (isset($request->camara_comercio_PDF)) {
            move_uploaded_file($request->file('camara_comercio_PDF'), $path . 'camara_comercio.' . $request->camara_comercio_PDF->getClientOriginalExtension());
            $file = $path . 'camara_comercio.' . $request->camara_comercio_PDF->getClientOriginalExtension();
            $cotizacion->camara_comercio_PDF = $file;
        }
        if (isset($request->rut_PDF)) {
            move_uploaded_file($request->file('rut_PDF'), $path . 'rut.' . $request->rut_PDF->getClientOriginalExtension());
            $file = $path . 'rut.' . $request->rut_PDF->getClientOriginalExtension();
            $cotizacion->rut_PDF = $file;
        }
        if (isset($request->representante_legal_PDF)) {
            move_uploaded_file($request->file('representante_legal_PDF'), $path . 'representante_legal.' . $request->representante_legal_PDF->getClientOriginalExtension());
            $file = $path . 'representante_legal.' . $request->representante_legal_PDF->getClientOriginalExtension();
            $cotizacion->representante_legal_PDF = $file;
        }
        $cotizacion->save();
        return response(['message' => 'Cotizacion guardada']);
    }

    public function eliminar(Request $request)
    {
        Cotizacion::destroy($request->id);
        return response(['message' => 'Proyecto Eliminado']);
    }

    function pathDocumentos($proyecto, $cotizacion)
    {
        $path = "documentos/empresa/" . $proyecto->empresa->id . "/proyecto/" . $proyecto->id . "/cotizacion/" . $cotizacion->id . "/";
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }
}
