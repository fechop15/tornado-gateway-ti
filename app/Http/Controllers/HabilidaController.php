<?php

namespace App\Http\Controllers;

use App\Models\NivelHabilidad;
use Illuminate\Http\Request;

class HabilidaController extends Controller
{
    //
    public function getHabilidades(){}

    public function getNivelesHabilidad(Request $request){
        $nivelHabilidad=NivelHabilidad::where('habilidade_id',$request->habilidad_id)->get();
        return response(['nivel' => $nivelHabilidad]);
    }
}
