<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use Illuminate\Http\Request;

class ProyectoController extends Controller
{
    //
    public function registrar(Request $request){
        $request->validate(
            [
                'nombre' => 'required',
            ],
            [
                'nombre.required' => 'Ingrese el nombre del proyecto',
            ]
        );

        $proyecto = Proyecto::create([
            'nombre' => strtoupper($request->nombre),
            'empresa_id'=>auth()->user()->empresa->id,

        ]);

        return response(['message' => 'Proyecto registrado', 'proyecto' => $proyecto]);
    }

    public function editar(Request $request){
        $proyecto=Proyecto::find($request->id);

        if(isset($request->nombre)){
            $proyecto->nombre= strtoupper($request->nombre);
        }
        if(isset($request->descripcion)){$proyecto->descripcion=$request->descripcion;}
        if(isset($request->obj_general)){$proyecto->obj_general=$request->obj_general;}
        if(isset($request->obj_especifico)){$proyecto->obj_especifico=$request->obj_especifico;}
        if(isset($request->area_tematica)){$proyecto->area_tematica=$request->area_tematica;}
        if(isset($request->beneficios)){$proyecto->beneficios=$request->beneficios;}
        if(isset($request->estado_desarrollo)){$proyecto->estado_desarrollo=$request->estado_desarrollo;}
        if(isset($request->propiedad_intelectual)){$proyecto->propiedad_intelectual=$request->propiedad_intelectual;}
        if(isset($request->diferencial)){$proyecto->diferencial=$request->diferencial;}
        if(isset($request->explicacion_PDF_PPT)){
            $path = $this->pathDocumentos($proyecto);
            move_uploaded_file($request->file('explicacion_PDF_PPT'), $path . 'explicacion.' . $request->explicacion_PDF_PPT->getClientOriginalExtension());
            $file = $path . 'explicacion.'. $request->explicacion_PDF_PPT->getClientOriginalExtension();
            $proyecto->explicacion_PDF_PPT=$file;
        }
        if(isset($request->url_video)){$proyecto->url_video=$request->url_video;}
        if(isset($request->presupuesto_EXEL)){
            $path = $this->pathDocumentos($proyecto);
            move_uploaded_file($request->file('presupuesto_EXEL'), $path . 'presupuesto.' . $request->presupuesto_EXEL->getClientOriginalExtension());
            $file = $path . 'presupuesto.'. $request->presupuesto_EXEL->getClientOriginalExtension();
            $proyecto->presupuesto_EXEL=$file;
        }
        $proyecto->save();
        return response(['message' => 'Proyecto actualizado','proyecto'=>$proyecto]);
    }

    public function eliminar(Request $request){
        $proyecto=Proyecto::find($request->id);
        foreach ($proyecto->cotizaciones as $cotizacion){
            $cotizacion->delete();
        }
        Proyecto::destroy($request->id);
        return response(['message' => 'Proyecto Eliminado']);
    }

    function pathDocumentos($proyecto)
    {
        $path = "documentos/empresa/".$proyecto->empresa->id."/proyecto/".$proyecto->id."/";
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }
}
