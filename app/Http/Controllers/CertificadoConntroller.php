<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Wompi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mpdf\Mpdf;

class CertificadoConntroller extends Controller
{
    //
    function certificados(Request $request){
        if ($request->tipo=="Pendientes"){
            $message="Certificados Pendientes";
            $certificados=Wompi::whereNull('fechaInicio')->where('estado',1)->get();
            foreach ($certificados as $certificado){
                if ($certificado->persona){
                    $user=$certificado->persona->user;
                }else if ($certificado->empresa){
                    $user=$certificado->empresa->user;
                }
            }
        }
        else if ($request->tipo=="Personas"){
            $message="Certificados Personas";
            $certificados=Wompi::whereNotNull('persona_id')->whereNotNull('fechaInicio')->where('estado',1)->get();
            foreach ($certificados as $certificado){
                $persona=$certificado->persona;
                $user=$certificado->persona->user;

            }
        }
        else if ($request->tipo=="Empresas"){
            $message="Certificados Empresas";
            $certificados=Wompi::whereNotNull('empresa_id')->whereNotNull('fechaInicio')->where('estado',1)->get();
            foreach ($certificados as $certificado){
                $empresa=$certificado->empresa;
                $user=$certificado->empresa->user;
            }
        }
        else{
            return response(['message' => 'Error','certificados'=> null]);
        }
        return response(['message' => $message,'certificados'=> $certificados]);

    }
    function descargarCertificado(Request $request){


        $certificado = Wompi::findOrFail($request->id);

        //validacion
        if (auth()->user()->tipo_usuario_id!=0){

            if (auth()->user()->persona && (auth()->user()->persona->id!=$certificado->persona_id ||
                    $certificado->estado==false ||
                    $certificado->fechaInicio==null)){
                return abort(404);
            }else if(auth()->user()->empresa && (auth()->user()->empresa->id!=$certificado->empresa_id ||
                    $certificado->estado==false ||
                    $certificado->fechaInicio==null)){
                return abort(404);
            }
        }


        $rutapdf = "plantillas/plantilla1.pdf";
        $filename = "Certificado_".$certificado->id.".pdf";

        //$mpdf = new mPDF(['utf-8', [210,310], 'Arial Bold', 3, 3, 3, 3, 9, 9, 'L']);
        $mpdf = new mPDF([
            'format' => [200, 280],
            'orientation' => 'L',
            'default_font_size' => '30',
            'margin_left' => '3',
            'margin_right' => '3',
            'margin_top' => '3',
            'margin_bottom' => '3',
            'margin_header' => '9',
            'margin_footer' => '9',
        ]);

        //$mpdf = new mPDF();

        //datos del certificado//
        //$anioCurso = date("Y", strtotime($certificado->curso->fecha_fin_curso));
        //$mesCurso = date("n", strtotime($certificado->curso->fecha_fin_curso));
        ////

        //$mpdf->SetImportUse();

        $pagecount = $mpdf->SetSourceFile($rutapdf);

        $tplId = $mpdf->ImportPage(1);
        $mpdf->UseTemplate($tplId);

        if ($certificado->persona){
            $mpdf->WriteFixedPosHTML("<div style='text-align: center;font-size: 28pt;'>".strtoupper($certificado->persona->user->nombres)." ".strtoupper($certificado->persona->user->apellidos)."</div>", 65, 103, 180, 90, 'auto');
            $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
                "Con número de identificación ".number_format($certificado->persona->identificacion, 0, ',', '.').
                "</div>", 65, 118, 180, 90, 'auto');
        }else{
            $mpdf->WriteFixedPosHTML("<div style='text-align: center;font-size: 28pt;'>".$certificado->empresa->nombrecomercial."</div>", 65, 100, 180, 90, 'auto');
            $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
                "NIT: ".number_format($certificado->empresa->nit, 0, ',', '.').
                "</div>", 65, 118, 180, 90, 'auto');
        }

        $mpdf->WriteFixedPosHTML("<div style='font-size: 11pt; font-family: Times, serif;'>".
            date("Y/m/d", strtotime($certificado->fechaInicio)).
            "</div>", 127, 152, 30, 90, 'auto');
        $mpdf->WriteFixedPosHTML("<div style='font-size: 11pt; font-family: Times, serif;'>".
            date("Y/m/d", strtotime($certificado->fechaFin)).
            "</div>", 203, 152, 30, 90, 'auto');

        $mpdf->Output($filename,'I');
    }

    function certificar(Request $request)
    {
        $pago = Wompi::findOrFail($request->id);
        $pago->fechaInicio=Carbon::now()->format('Y-m-d');
        $fechaFin=strtotime ( '+365 day' , strtotime ( Carbon::now()->format('Y-m-d') ) ) ;
        $pago->fechaFin=date('Y-m-d', $fechaFin);
        $pago->save();
        return response(['message' => 'Cuenta Certificada con exito','status' => 'success']);
    }

}
