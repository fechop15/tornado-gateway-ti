<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegistroMail;
use App\Models\Empresa;
use App\Models\Persona;
use App\Models\Tipo_usuario;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        return view('auth.register',[
            'layout' => 'login',
            'tipo_usuarios' => Tipo_usuario::all()
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombres' => 'required|string|max:255',
            //'apellidos' => 'string|max:255',
            'telefono' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
            'politicas' => 'required|accepted',
            'tipo_usuario' => 'required|exists:tipo_usuario,id',
            'recaptcha' => 'recaptcha',
        ],[
            'recaptcha.recaptcha' => 'Complete el capcha',
            'nombres.required' => 'El campo nombre es obligatorio'
        ]);
        $user = User::create([
            'nombres' => $request->nombres,
            //'apellidos' => $request->apellidos,
            'telefono' => $request->telefono,
            'tipo_usuario_id' => $request->tipo_usuario,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        /*Auth::login($user = User::create([
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'telefono' => $request->telefono,
            'tipo_usuario_id' => $request->tipo_usuario,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]));*/
        if ($request->tipo_usuario=="1"){
            //crear Persona
            Persona::create(['user_id'=>$user->id]);
        }else{
            Empresa::create(['user_id'=>$user->id]);
        }
        event(new Registered($user));
        Mail::to($user->email)->send(new RegistroMail());

        //return redirect(RouteServiceProvider::HOME);
        //return redirect('/');
        return response(['message' => 'Registrado con exito']);

    }
}
