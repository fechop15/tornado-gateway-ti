<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    //

    private $permission;

    function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function index()
    {
        //return $this->permission->get();
        $response = array(
            'status' => 'success',
            'msg' => $this->permission->get(),
        );

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $role = $this->permission->create([
            'name' => $request->name,
        ]);
        return response(['message' => 'Permiso Creado']);
    }

    public function update(Request $request, Permission $permission)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $permission->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        return response(['message' => 'Permiso Actualizado']);
    }

    public function destroy($id)
    {
        $this->permission->destroy($id);
        return response(['message' => 'Permiso Eliminado']);
    }

}
