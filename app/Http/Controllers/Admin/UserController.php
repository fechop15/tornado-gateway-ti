<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    function __construct()
    {
        //$this->middleware(['permission:ver.roles'])->only('index');
        //$this->middleware(['permission:crear.usuarios'])->only('store');
        //$this->middleware(['permission:editar.usuarios'])->only('update');
        //$this->middleware(['permission:eliminar.usuarios'])->only('destroy');
        //$this->middleware(['permission:eliminar.usuarios'])->only('verificarUsuario');
    }

    public function index(Request $request)
    {
        $users = User::where('tipo_usuario_id',0)->get();
        $array = array();

        foreach ($users as $usuario) {
            $datos = [
                'id' => $usuario->id,
                'nombres' => $usuario->nombres,
                'apellidos' => $usuario->apellidos,
                'email' => $usuario->email,
                'rol' => $usuario->roles,
                'telefono' => $usuario->telefono,
                'fecha_registro' => date("d/m/Y h:i A", strtotime($usuario->created_at)),
                'active' => $usuario->active,
            ];
            array_push($array, $datos);
        }

        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nombres' => 'required',
                'apellidos' => 'required',
                'telefono' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required',
                'rol' => 'required',
            ],
            [
                'email.required' => 'Este email ya se encuentra registrado',
            ]
        );

        $user = User::create([
            'nombres' => strtoupper($request->nombres),
            'apellidos' => strtoupper($request->apellidos),
            'telefono' => $request->telefono,
            'tipo_usuario_id' => 0,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if ($request->has('rol')) {
            $user->assignRole($request->rol);
        }
        if ($request->has('permissions')) {
            $user->givePermissionTo($request->permissions);
        }
        return response(['message' => 'Usuario Creado', 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'telefono' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'rol' => 'required',
        ]);
        $user->update([
            'nombres' => strtoupper($request->nombres),
            'apellidos' => strtoupper($request->apellidos),
            'telefono' => $request->telefono,
            'tipo_usuario_id' => 0,
            'email' => $request->email,
        ]);
        if ($request->has('rol')) {
            $user->syncRoles($request->rol);
        }
        if ($request->has('permissions')) {
            $user->syncPermissions(collect($request->permissions)->pluck('id')->toArray());
        }
        return response(['message' => 'Usuario Actualizado', 'user' => $user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //User::destroy($id);
        return response(['message' => 'Usuario Eliminado']);

    }

    /**
     * Actualizar contraseña del usuario.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function actualizarPass(Request $request)
    {

        if ($request->id) {
            $usuario = User::findOrFail($request->id);
            $this->validate($request, [
                'pass' => 'required|string|min:6',
            ]);

            if (auth()->user()->rol_id != 1) {
                $response = array(
                    'status' => 'Error',
                    'msg' => "Error, Falta de privilegios",
                );
                return response()->json($response);

            } else {
                $usuario->password = bcrypt($request->pass);
                $usuario->save();
                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );
                return response()->json($response);

            }

        } else {

            $usuario = User::findOrFail(auth()->user()->id);
            $this->validate($request, [
                'passOld' => 'required|string',
                'pass' => 'required|string|min:6',
            ]);

            if (Hash::check($request->passOld, $usuario->password)) {

                $usuario->password = bcrypt($request->pass);
                $usuario->save();

                $response = array(
                    'status' => 'success',
                    'msg' => 'la Contraseña se ha cambiado correctamente',
                );

            } else {
                $response = array(
                    'status' => 'Error',
                    'msg' => "la Contraseña actual no es correcta",
                );
            }

            return response()->json($response);

        }

    }

    /**
     * Actualizar foto de perfil.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update_profile(Request $request)
    {

        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);
        //mensaje la imagen no se puede subir
        $path = "images/perfil/";

        $filename = Auth::id() . '_' . time() . '.' . $request->avatar->getClientOriginalExtension();
        move_uploaded_file($request->file('avatar'), $path . $filename);

        $user = Auth::user();
        $user->imagen = $filename;
        $user->save();

        $response = array(
            'status' => 'success',
            'imagen' => $filename,
            'msg' => "La Imagen Se Cargo Correctamente",
        );
        return response()->json($response);

    }

    function verificarUsuario(Request $request)
    {

        $usuario = User::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {

            $usuario->active = !$usuario->active;
            $usuario->save();
            DB::commit();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => ($usuario),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }


}
