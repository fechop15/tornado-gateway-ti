<?php

namespace App\Http\Controllers;

use App\Models\Wompi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class WompiController extends Controller
{
    //
    function respuestaWompi(Request $request){

        $respuesta=$request->all();
        $wompi= Wompi::where('referencia',$respuesta['data']['transaction']['reference'])->first();
        $wompi->respuesta=$request->all();
        if ($respuesta['data']['transaction']['status']=="APPROVED"){
            $wompi->estado=1;
        }
        $wompi->save();
        return response([
            'message' => 'Pago Registrado',
            'respuesta' => $respuesta['data']['transaction'],
        ]);
    }

    public function getInfoPago(){
        //$publicKey='pub_test_b4uK4LSO0RAC06N0VM68CPs65zoAZZJw';//pruebas
        $publicKey='pub_prod_Br5eXa5fS7KX58D4yChBePZAw4WmwlUU';//produccion
        if (auth()->user()->tipo_usuario_id==1){
            //persona
            if(auth()->user()->persona->pagos->last()){
                if (auth()->user()->persona->pagos->last()->fechaFin && auth()->user()->persona->pagos->last()->fechaFin < \Carbon\Carbon::now()){
                    $referencia= $this->generarOrdenDePagoPersona(auth()->user()->persona->id);
                }
                $referencia=auth()->user()->persona->pagos->last()->referencia;
            }else{
                $referencia=$this->generarOrdenDePagoPersona(auth()->user()->persona->id);
            }
            return response([
                'message' => 'Referencia Generada',
                'precio' => 10000000,
                'referencia' => $referencia,
                'publicKey' => $publicKey,
            ]);
        }else if(auth()->user()->tipo_usuario_id==2){
                //empresa
            if(auth()->user()->empresa->pagos->last()){
                if (auth()->user()->empresa->pagos->last()->fechaFin && auth()->user()->empresa->pagos->last()->fechaFin < \Carbon\Carbon::now()){
                    $referencia=$this->generarOrdenDePagoEmpresa(auth()->user()->empresa->id);
                }
                $referencia=auth()->user()->empresa->pagos->last()->referencia;
            }else{
                $referencia=$this->generarOrdenDePagoEmpresa(auth()->user()->empresa->id);
            }
            return response([
                'message' => 'Referencia Generada',
                'precio' => 50000000,
                'referencia' => $referencia,
                'publicKey' => $publicKey,
            ]);
        }

        return response([
            'message' => 'ERROR',
        ]);
    }

    public function generarOrdenDePagoPersona($id){
        $wompi= Wompi::create();
        $wompi->referencia = Str::uuid()->toString();
        $wompi->persona_id = $id;
        $wompi->save();
        return $wompi->referencia;
    }

    public function generarOrdenDePagoEmpresa($id){
        $wompi= Wompi::create();
        $wompi->referencia = Str::uuid()->toString();
        $wompi->empresa_id = $id;
        $wompi->save();
        return $wompi->referencia;
    }
/*
    function descargarCertificado(Request $request){


        $inscripcion = Inscripcion::findOrFail($request->id);

        //validacion
        if (auth()->user()->rol_id==5){
            if (auth()->user()->persona->id!=$inscripcion->persona->id ||
                $inscripcion->estado==false ||
                $inscripcion->fecha_certificacion==null){
                return abort(404);
            }
        }


        $rutapdf = $inscripcion->certificado->detalle->pdf; //"plantillas/plantilla1.pdf";
        $filename = "Certificado_".$inscripcion->persona->nombres.".pdf";
        $filename = str_replace(" ", "_", $filename);

        $tempDir = 'temp/';
        $qrfileName = $inscripcion->id.'_codigoqr.png';
        $pngAbsoluteFilePath = $tempDir.$qrfileName;

        // generating
        $url=$request->root();
        if (!file_exists($pngAbsoluteFilePath)) {
            QrCode::generate($url.'/consulta_externa/'.Crypt::encryptString($inscripcion->id), $pngAbsoluteFilePath);
        }

        //$mpdf = new mPDF(['utf-8', [210,310], 'Arial Bold', 3, 3, 3, 3, 9, 9, 'L']);
        $mpdf = new mPDF([
            'format' => [210, 310],
            'orientation' => 'L',
            'default_font_size' => '30',
            'margin_left' => '3',
            'margin_right' => '3',
            'margin_top' => '3',
            'margin_bottom' => '3',
            'margin_header' => '9',
            'margin_footer' => '9',
        ]);

        //$mpdf = new mPDF();

        //datos del certificado//
        $anioCurso = date("Y", strtotime($inscripcion->curso->fecha_fin_curso));
        $mesCurso = date("n", strtotime($inscripcion->curso->fecha_fin_curso));
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $diasTranscurridos=$this->devuelveArrayFechasEntreOtrasDos($inscripcion->curso->fecha_inicio_curso,$inscripcion->curso->fecha_fin_curso);
        $mesesTranscurridos=$this->devuelveArrayMesesEntreOtrasDos($inscripcion->curso->fecha_inicio_curso,$inscripcion->curso->fecha_fin_curso);
        $mesesCurso=array();
        foreach ($mesesTranscurridos as $mes){
            array_push($mesesCurso,$meses[$mes-1]);
        }

        ////

        $mpdf->SetImportUse();

        $pagecount = $mpdf->SetSourceFile($rutapdf);

        $tplId = $mpdf->ImportPage(1);
        $mpdf->UseTemplate($tplId);

        $mpdf->WriteFixedPosHTML("<div style='text-align: center;'>".$inscripcion->persona->nombres." ".$inscripcion->persona->apellidos."</div>", 35, 95, 310, 90, 'auto');
        $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
            "Con número de identificación ".number_format($inscripcion->persona->documento, 0, ',', '.').
            "</div>", 30, (95+14), 310, 90, 'auto');
        $mpdf->Image($pngAbsoluteFilePath, 266, 167, 35, 35, 'png', '');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 20pt; font-family: Times, serif;'>".
            $inscripcion->curso->nivel.
            "</div>", 150, 125, 310, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='text-align: center; font-size: 12pt; font-family: Times, serif;'>".
            "Realizado en ".$inscripcion->curso->ciudad.", los días ".$inscripcion->curso->dias." de ".implode(" y ",$mesesCurso)." de $anioCurso, con una intensidad de ".$inscripcion->curso->intensidad." horas".
            "</div>", 30, 135, 310, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 13pt; font-family: Times, serif;'>".
            date("d/m/Y", strtotime($inscripcion->fecha_certificacion)).
            "</div>", 210, 192, 30, 90, 'auto');

        $mpdf->WriteFixedPosHTML("<div style='font-size: 18pt; font-family: Times, serif;'>".
            "Reg. Nº ".str_pad($inscripcion->certificado->id, 4, '0', STR_PAD_LEFT).
            "</div>", 260, 7, 60, 90, 'auto');

        $mpdf->Output($filename,'I');
    }

    function certificar(Request $request)
    {

        $inscripcion = Inscripcion::findOrFail($request->id);

        //'estado','pago','documentado', 'nota_teorica', 'nota_practica', 'fecha_certificacion',
        //'curso_id','persona_id','asistencia_validada'

        $error = null;
        DB::beginTransaction();
        try {

            $inscripcion->fecha_certificacion=Carbon::now()->format('Y-m-d');
            $detallePDF = Detalle_certificados::where('curso_id', '=', $inscripcion->curso->id)->first();

            //generar certificado en la tabla
            $certificado=Certificados::create([
                'inscripcion_id'=>$inscripcion->id,
                'detalle_certificado_id'=>$detallePDF->id,
                'user_id'=>auth()->user()->id
            ]);

            if($inscripcion->persona->empresa_id==null){
                $inscripcion->descargable=true;
            }else{
                $inscripcion->descargable=false;
            }

            $inscripcion->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $this->datos($inscripcion),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }
*/
}
