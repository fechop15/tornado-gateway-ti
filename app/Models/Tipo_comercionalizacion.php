<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipo_comercionalizacion extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'descripcion'
    ];
}
