<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonaIdioma extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'idioma_id',
        'persona_id',
        'escrito',
        'leido',
        'hablado',
        'escuchado',
    ];
    public function idioma()
    {
        return $this->belongsTo(Idioma::class,'idioma_id');
    }

    public function persona()
    {
        return $this->belongsTo(Persona::class,'persona_id');
    }
}
