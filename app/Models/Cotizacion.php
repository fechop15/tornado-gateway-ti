<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'nombreEmpresa',
        'cotizacion_PDF',
        'documento_PDF',
        'camara_comercio_PDF',
        'rut_PDF',
        'representante_legal_PDF',
        'proyecto_id',
    ];

    public function proyecto()
    {
        return $this->belongsTo(Proyecto::class,'proyecto_id');
    }
}
