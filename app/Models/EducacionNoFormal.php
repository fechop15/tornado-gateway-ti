<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EducacionNoFormal extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'num_horas',
        'tituloEstudio',
        'fecha_certificado',
        'nivel_formacion_no_formal_id',
        'persona_id',
    ];

    public function nivel()
    {
        return $this->belongsTo(NivelFormacionNoFormal::class,'nivel_formacion_no_formal_id');
    }

    public function persona()
    {
        return $this->belongsTo(Persona::class,'persona_id');
    }
}
