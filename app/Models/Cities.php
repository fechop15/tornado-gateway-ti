<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    //
    protected $fillable = [
        'name', 'state_id'
    ];

    public function departamento()
    {
        return $this->belongsTo(States::class,'state_id');
    }
}
