<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wompi extends Model
{
    use HasFactory;

    protected $table='pagos';
    protected $casts = [
        'respuesta' => 'array'
    ];

    protected $fillable = [
        'referencia','respuesta','estado','persona_id', 'empresa_id','fechaInicio','fechaFin'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    public function persona()
    {
        return $this->belongsTo(Persona::class, 'persona_id');
    }
}
