<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpresaHabilidades extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'empresa_id',
        'nivel_habilidad_id'
    ];

    public function nivel_habilidad()
    {
        return $this->belongsTo(NivelHabilidad::class,'nivel_habilidad_id');
    }

    public function empresa()
    {
        return $this->belongsTo(Persona::class,'empresa_id');
    }
}
