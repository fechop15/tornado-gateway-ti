<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NivelHabilidad extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'descripcion',
        'habilidade_id'
    ];

    public function habilidad()
    {
        return $this->belongsTo(Habilidades::class,'habilidade_id');
    }
}
