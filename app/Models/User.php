<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres','apellidos','telefono','tipo_usuario_id', 'email', 'password','photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    /**
//     * The attributes that appends to returned entities.
//     *
//     * @var array
//     */
//    protected $appends = ['photo'];
//
//    /**
//     * The getter that return accessible URL for user photo.
//     *
//     * @var array
//     */

//    public function getPhotoUrlAttribute()
//    {
//        if ($this->photo !== null) {
//            return url('media/user/' . $this->id . '/' . $this->photo);
//        } else {
//            return url('media-example/no-image.png');
//        }
//    }

    //Tipo_usuario
    public function tipo_usuario()
    {
        return $this->belongsTo(Tipo_usuario::class, 'tipo_usuario_id');
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class, 'user_id');
    }

    public function persona()
    {
        return $this->hasOne(Persona::class, 'user_id');
    }
}
