<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'razonsocial',
        'nombrecomercial',
        'nit',
        'rut_rutaarachivo',
        'breve_descr_empresa',
        'direccion_correspondencia',
        'telefono',
        'celular',
        'tipoid_represen_legal',
        'identi_rutaarchivo',
        'identifica_representante_legal',
        'nombte_representante_legal',
        'anno_creacion_empresa',
        'num_empleados_directos',
        'num_empleados_indirectos',
        'camaracomercio_rutaarachivo',
        'prome_ventas_anuales',
        'balance_general_rutaarachivo',
        'total_activos',
        'num_sedes',
        'ciudades_otras_sedes',
        'pagina_web',
        'link_portafolio',
        'logo_rutararchivo',
        'link_facebook',
        'link_instagram',
        'link_linkedin',
        'link_twitter',
        'tiene_canales_virtuales_atencion',
        'realiza_ventas_linea',
        'certifica_iso9001',
        'certifica_iso14001',
        'otra_certifica_internacional',
        'uso_energias_alternativas',
        'num_convenios_estra_alianza_estable',
        'porce_presupues_info_personal',
        'cuenta_software_crm',
        'cuenta_software_erp',
        'num_persona_dominio_segunda_lengua',
        'num_mujeres_empleadas',
        'num_empleados_con_postgrado',
        'edad_promedio_empleados',
        'num_madres_cabeza_hogar',
        'ingremento_ventas',
        'llegar_nuevos_mercados',
        'lanzamiento_nuevos_productos',
        'mejoramiento_productividad',
        'incremento_capacidad_productiva',
        'desarrollo_nuevos_canales',
        'implementacion_ti',
        'infraestructura_fisica',
        'compra_maquinaria_equipos',
        'num_empleados_habilidades_ti',
        'num_empleados_region',
        'user_id',
        'city_id',
        'sector_empresa_id',
        'ciiu_id',
        'actividad_economica_1_id',
        'actividad_economica_2_id',
        'tipo_operacion_id',
        'tipo_comercionalizacion_id',
        'clave_rut',
        'camara_comercio_rutaarachivo',
        'celular_representante_legal',
        'mercado',
        'recurso_humano',
    ];

    public function servicios_ofertados()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Servicio::class,
            'servicios_ofertados',
            'empresa_id',
            'servicio_id');
    }

    public function sector_negocios()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Sector::class,
            'sector_negocios',
            'empresa_id',
            'sector_id');
    }

    public function actividadEconomicaOtros()
    {
        return $this->belongsToMany(
            Actividad_economica::class,
            'actividad_economica_empresas',
            'empresa_id',
            'actividad_economica_id');
    }

    public function tipoEmpresas()
    {
        return $this->belongsToMany(
            Tipo_empresa::class,
            'empresa_tipo_empresas',
            'empresa_id',
            'tipo_empresa_id');
    }

    public function ciudad()
    {
        return $this->belongsTo(Cities::class,'city_id');
    }

    public function pagos()
    {
        return $this->hasMany(Wompi::class,'empresa_id');
    }

    public function habilidades()
    {
        return $this->hasMany(EmpresaHabilidades::class,'empresa_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function proyecto()
    {
        return $this->hasMany(Proyecto::class,'empresa_id');
    }

    public function presupuestos()
    {
        return $this->hasMany(Presupuesto::class,'empresa_id');
    }

    public function herramientas()
    {
        return $this->hasMany(Herramienta::class,'empresa_id');
    }
}
