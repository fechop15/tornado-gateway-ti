<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'identificacion',
        'fecha_nacimiento',
        'celular',
        'correo_electro_personal',
        'link_instagram',
        'link_linkedin',
        'link_twitter',
        'link_facebook',
        'num_personas_dependientes',
        'numero_contacto_emergencias',
        'nombre_contacto_emergencias',
        'direccion_recidencia',
        'nivel_ingles_certifica_rutaarachivo',
        'identificacion_rurataarchivo',
        'user_id',
        'genero_id',
        'tipoid',
        'ciudad_nacimiento_id',
        'ciudad_recidencia_id',
        'nivel_ingles_id',
        'rh_sanguineo_id',
        'estado_civil_id',
        'eps_id',
        'pensiones_id',
        'cesantias_id',
    ];

    public function estudiosFormales()
    {
        return $this->hasMany(EstudiosFormales::class,'persona_id');
    }

    public function educacionNoFormales()
    {
        return $this->hasMany(EducacionNoFormal::class,'persona_id');
    }

    public function experiencias()
    {
        return $this->hasMany(ExperienciaPersonal::class,'persona_id');
    }
    public function ciudad()
    {
        return $this->belongsTo(Cities::class,'ciudad_nacimiento_id');
    }

    public function habilidades()
    {
        return $this->hasMany(PersonaHabilidades::class,'persona_id');
    }

    public function idiomas()
    {
        return $this->hasMany(PersonaIdioma::class,'persona_id');
    }

    public function pagos()
    {
        return $this->hasMany(Wompi::class,'persona_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
