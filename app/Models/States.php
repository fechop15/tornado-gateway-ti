<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    //
    protected $fillable = [
        'name', 'country_id'
    ];

    public function ciudades()
    {
        return $this->hasMany(Cities::class,'state_id');
    }

    public function pais()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }
}
