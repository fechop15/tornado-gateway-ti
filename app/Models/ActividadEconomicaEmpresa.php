<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActividadEconomicaEmpresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'empresa_id','actividad_economica_id'
    ];
}
