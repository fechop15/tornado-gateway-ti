<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstudiosFormales extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'titulo_obtenido',
        'finalizado',
        'fecha_obtenido_titulo',
        'diploma_rutaarchivo',
        'persona_id',
        'nivel_formacion_id',
        'sub_area_conocimiento_id',
    ];

    public function nivelFormacion()
    {
        return $this->belongsTo(NivelFormacion::class,'nivel_formacion_id');
    }

    public function subAreaConocimiento()
    {
        return $this->belongsTo(SubAreaConocimiento::class,'sub_area_conocimiento_id');
    }

}
