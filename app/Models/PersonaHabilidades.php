<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonaHabilidades extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'persona_id',
        'nivel_habilidad_id'
    ];

    public function nivel_habilidad()
    {
        return $this->belongsTo(NivelHabilidad::class,'nivel_habilidad_id');
    }

    public function persona()
    {
        return $this->belongsTo(Persona::class,'persona_id');
    }
}
