<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actividad_economica extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'descripcion'
    ];

    public function empresas1()
    {
        return $this->belongsToMany(Empresa::class,'actividad_economica_1_id');
    }
    public function empresas2()
    {
        return $this->belongsToMany(Empresa::class,'actividad_economica_2_id');
    }
}
