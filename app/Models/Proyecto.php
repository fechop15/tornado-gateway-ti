<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'obj_general',
        'obj_especifico',
        'area_tematica',
        'beneficios',
        'estado_desarrollo',
        'propiedad_intelectual',
        'diferencial',
        'explicacion_PDF_PPT',
        'url_video',
        'presupuesto_EXEL',
        'empresa_id',
        'estado',
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class,'empresa_id');
    }

    public function cotizaciones()
    {
        return $this->hasMany(Cotizacion::class,'proyecto_id');
    }
}
