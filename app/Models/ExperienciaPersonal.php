<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExperienciaPersonal extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'nombre_empresa_empleadora',
        'actividad_economina_empleador',
        'principal_funciones_cargo',
        'fecha_inicio',
        'fecha_retiro',
        'persona_id',
    ];
}
