<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciiu extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'descripcion'
    ];
    public function empresas()
    {
        return $this->belongsToMany(Empresa::class,'ciiu_id');
    }
}
