<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaConocimiento extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'descripcion',
    ];

    public function subAreaConocimiento()
    {
        return $this->hasMany(SubAreaConocimiento::class,'area_conocimiento_id');
    }
}
