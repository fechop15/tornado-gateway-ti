<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicios_ofertado extends Model
{
    use HasFactory;
    protected $fillable = [
        'servicio_id', 'empresa_id'
    ];
}
