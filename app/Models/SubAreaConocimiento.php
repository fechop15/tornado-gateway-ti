<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubAreaConocimiento extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'descripcion',
        'area_conocimiento_id',
    ];

    public function areaConocimiento()
    {
        return $this->belongsTo(AreaConocimiento::class,'area_conocimiento_id');
    }
}
